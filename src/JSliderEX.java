import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSliderUI;

class JSliderEX extends JFrame {
	JSlider slider;

	public JSliderEX() {
		super("JSliderEX");
		setSize(300, 80);
		setLocation(400, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// MySliderUI ms = new MySliderUI(slider);
		// slider.setUI(ms);
		slider = new JSlider(0, 100, 0) {
//			public Point getToolTipLocation(MouseEvent event) {
//				Point p = event.getPoint();
//				p.setLocation(p.x, p.y - 20);
//				return p;
//			}
		};
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				slider.setToolTipText(Integer.toString(slider.getValue()));
				
			}
			
		});
		SliderToolTips.enableSliderToolTips(slider); 
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(5);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		getContentPane().add(slider);
	}

	public static void main(String args[]) {
		new JSliderEX().setVisible(true);
	}
}

/**
 * taken from
 * http://forum.java.sun.com/thread.jspa?threadID=568104&messageID=2806255
 */
class MySliderUI extends BasicSliderUI implements MouseMotionListener,
		MouseListener {
	final JPopupMenu pop = new JPopupMenu();
	JMenuItem item = new JMenuItem();

	public MySliderUI(JSlider slider) {
		super(slider);
		slider.addMouseMotionListener(this);
		slider.addMouseListener(this);
		pop.add(item);

		pop.setDoubleBuffered(true);
	}

	public void showToolTip(MouseEvent me) {
		item.setText(slider.getValue() + " fps");

		// limit the tooltip location relative to the slider
		Rectangle b = me.getComponent().getBounds();
		int x = me.getX();
		x = (x > (b.x + b.width / 2) ? (b.x + b.width / 2)
				: (x < (b.x - b.width / 2) ? (b.x - b.width / 2) : x));

		pop.show(me.getComponent(), x - 5, -30);
		// pop.setFocusable( false );
		// pop.setEnabled( false );

		item.setArmed(false);
		// item.setSelected(false);

		// item.setEnabled( false );
	}

	public void mouseDragged(MouseEvent me) {
		showToolTip(me);
	}

	public void mouseMoved(MouseEvent me) {
	}

	public void mousePressed(MouseEvent me) {
		showToolTip(me);
	}

	public void mouseClicked(MouseEvent me) {
	}

	public void mouseReleased(MouseEvent me) {
		pop.setVisible(false);
	}

	public void mouseEntered(MouseEvent me) {
	}

	public void mouseExited(MouseEvent me) {
	}
}