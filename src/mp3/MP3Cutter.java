package mp3;

import java.io.*;
import java.nio.charset.Charset;

public class MP3Cutter {

	private MP3Info info;
	private File outFile;
	private static final int[][] SIDE_INFO = { //
	{ 32, 32, 32, 17 }, // MPEG 1
			{ 17, 17, 17, 9 }, // MPEG 2
			{ 17, 17, 17, 9 }, // MPEG 2.5
	};
	private ProgressMonitor monitor;
	private int beginFrame;
	private int endFrame;
	private int frames;
	private int totalPercent;
	private int beginStreamPos = -1;
	private int endStreamPos = -1;
	private static final int FIND_FRAMES_PERCENTAGE = 10;

	public MP3Cutter(MP3Info mp3Info, File outFile, int beginFrame, int endFrame) {
		this(mp3Info, outFile, beginFrame, endFrame, null);
	}

	public MP3Cutter(MP3Info mp3Info, File outFile, int beginFrame,
			int endFrame, ProgressMonitor monitor) {
		
		this.info = mp3Info;
		this.outFile = outFile;
		this.monitor = monitor;
		this.beginFrame = beginFrame;
		this.endFrame = endFrame;
		this.frames = endFrame - beginFrame;
		if (this.beginFrame == 0 && mp3Info.hasAdditionalFrame)
			this.beginFrame = 1;

	}

	private void calculateStreamPositions() throws IOException {
		int toSkip = info.firstFramePos;
		// if (beginFrame == 0)
		// beginStreamPos = toSkip;

		int counter = 0;
		int leftBytes = (int) info.mp3File.length();
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				info.mp3File));

		if (info.fastSeeking) {
			int steps = beginFrame / info.seekStep;
			if (steps == info.seekPoints.length)
				steps--;
			counter = steps * info.seekStep;

			toSkip = info.seekPoints[steps];
		}
		if (beginFrame == counter)
			beginStreamPos = toSkip;

		int total = toSkip;
		leftBytes -= toSkip;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		// System.out.println(total);
		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = MP3Utils.getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		// if(beginFrame == 1)
		// beginStreamPos = total;

		leftBytes -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);
		// --------------------------------------
		int lastPercent = 0;

		while (in.read(buff) != -1) {
			if (beginFrame == counter)
				beginStreamPos = total;
			counter++;

			if (monitor != null) {
				float f = (float) FIND_FRAMES_PERCENTAGE * (float) counter
						/ endFrame;
				int per = Math.round(f);

				if (lastPercent != per) {
					// System.out.println("per = " + per);
					lastPercent = per;
					monitor.progressMade(per);
				}
			}

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!MP3Utils.compareHeaders(lastHeader, header)) {

				if (counter > 1000) {
					// jedyne co mi przyszlo do glowy na bledu typu:
					// Free space or unrecognized Tag after last frame 15791
					// (bytes 16488280-16490368).
					counter--;
					break;
				}

				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					counter--;
					break;
				} else if (str.startsWith("LYRI")) {
					// LYRICSBEGIN
					byte[] strBuff = new byte[7];
					in.read(strBuff);
					str = new String(strBuff, Charset.forName("ASCII"));
					if (str.equals("CSBEGIN")) {
						counter--;
						break;
					}
				}

				// System.out.println("total = " + total);
				throw new IOException("NAGLOWKI NIE SA ROWNE:\n["
						+ (counter - 1) + "]:\n"
						+ MP3Utils.getHeaderLayout(lastHeader) + "\n["
						+ counter + "]:\n" + MP3Utils.getHeaderLayout(header));

			}
			lastHeader = header;

			toSkip = MP3Utils.getFrameLength(header) - 4;

			leftBytes -= (toSkip + 4);
			if (leftBytes < 0)
				toSkip = (toSkip + 4) + leftBytes;

			total += toSkip + 4;
			if (counter == endFrame) {
				endStreamPos = total;
				break;
			}

			while (toSkip > 0)
				toSkip -= in.skip(toSkip);
		}

		in.close();
	}

	public void write() throws IOException {

		calculateStreamPositions();
		if (monitor != null) {
			totalPercent = FIND_FRAMES_PERCENTAGE;
			monitor.progressMade(totalPercent);
		}

		int toSkip = beginStreamPos;

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				info.mp3File));
		while (toSkip != 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[1024];
		BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(outFile));
		int readBytes = 0;
		boolean firstRead = true;
		int bytesToWrite = endStreamPos - beginStreamPos;
		// System.out.println("MB TO WRITE = " + (bytesToWrite/(1024*1024)));
		int pos = beginStreamPos;
		int bytesWrote = 0;
		int lastPercent = totalPercent;
		while ((readBytes = in.read(buff, 0, buff.length)) != -1) {
			pos += readBytes;
			bytesWrote += readBytes;
			if (monitor != null) {
				float f = (float) (100 - totalPercent) * (float) bytesWrote
						/ bytesToWrite;
				int per = totalPercent + Math.round(f);

				if (lastPercent != per) {
					lastPercent = per;
					monitor.progressMade(per);
				}
			}

			if (firstRead) {
				firstRead = false;
				int h = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
						| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
				MP3Header mp3Header = null;
				try {
					mp3Header = new MP3Header(h);
				} catch (IncorrectMP3HeaderException e) {
					throw new IOException("ZLY NAGLOWEK: "
							+ MP3Utils.getHeaderLayout(h));
				}
				byte[] additionalFrame = createVBRFrame(mp3Header);
				out.write(additionalFrame);
			}

			if (pos > endStreamPos) {
				// int temp = readBytes - (pos - endStreamPos);
				// System.out.printf("pos = %d, endStreamPos = %d, readBytes = %d, temp = %d, buff.length = %d\n",
				// pos, endStreamPos, readBytes, temp, buff.length);
				out.write(buff, 0, readBytes - (pos - endStreamPos));
				break;
			} else
				out.write(buff, 0, readBytes);
		}

		in.close();
		out.close();
		// System.out.println("MP3Cutter: Final File length = " +
		// outFile.length());
	}

	public static byte[] intToByte(int i) {
		byte[] arr = new byte[4];
		arr[0] = (byte) (i >> 24);
		arr[1] = (byte) ((i << 8) >> 24);
		arr[2] = (byte) ((i << 16) >> 24);
		arr[3] = (byte) ((i << 24) >> 24);
		return arr;
	}

	private byte[] createVBRFrame(MP3Header header) throws IOException {
		String frameStr = "Info";
		if (info.hasXingFrame || info.hasVBRIFrame)
			frameStr = "Xing";
		int length = endStreamPos - beginStreamPos;
		return createVBRFrame(header, frameStr, frames, length);

	}

	public static byte[] createVBRFrame(MP3Header header, String frameStr,
			int frames, int length) throws IOException {
		// tempHeader.printHeaderLayout(tempHeader.getHeaderBits());
		// System.out.println("int = " + tempHeader.getHeaderBits());

		if (header.hasCRC()) {
			int i = header.getHeaderBits();
			int temp = 1 << 16;
			i ^= temp;
			try {
				header = new MP3Header(i);
			} catch (IncorrectMP3HeaderException e) {
				System.out.println("NIGDY NIE WYSTAPI :P");
				e.printStackTrace();
			}
		}

		byte[] vbrFrame = new byte[header.getFrameLength()];
		// System.out.println(tempHeader.getFrameLength());
		byte[] h = intToByte(header.getHeaderBits());
		int index = 0;
		for (byte b : h)
			vbrFrame[index++] = b;

		if (header.hasCRC()) {
			// index += 2;
			System.out.println("HASCRC!!");
		}

		int v = header.getMpegVersion();
		int chm = header.getChannelMode();
		index += SIDE_INFO[v][chm];

		byte[] xing = frameStr.getBytes(Charset.forName("ASCII"));
		// System.out.println("xing.length = " + xing.length);
		for (byte b : xing)
			vbrFrame[index++] = b;

		index += 3;
		vbrFrame[index++] = (byte) (1 | 2 | 8); // frames & bytes

		byte[] fr = intToByte(frames);

		for (byte b : fr)
			vbrFrame[index++] = b;

		length += vbrFrame.length;
		byte[] bs = intToByte(length);
		for (byte b : bs)
			vbrFrame[index++] = b;

		// Quality indicator = 0 (4bytes * 0)

		// System.out.println("index = " + index);
		// for(byte b : vbrFrame)
		// printBinaryByte(b);
		return vbrFrame;
	}
}
