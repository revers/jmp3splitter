package mp3;


public class IncorrectMP3HeaderException extends Exception {
	public IncorrectMP3HeaderException(String s) {
		super(s);
	}
}
