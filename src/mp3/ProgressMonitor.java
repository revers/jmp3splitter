package mp3;

public interface ProgressMonitor {
	public void progressMade(int totalPercent);
}
