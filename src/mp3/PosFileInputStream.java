package mp3;

import java.io.*;

public class PosFileInputStream extends FileInputStream {

	private int bytesRead = 0;

	public PosFileInputStream(File file) throws FileNotFoundException {
		super(file);
	}

	public int getPos() {
		return bytesRead;
	}

	@Override
	public int read() throws IOException {
		int temp = super.read();
		if (temp != -1) {
			bytesRead++;
		}

		return temp;
	}

	@Override
	public int read(byte[] b) throws IOException {
		int temp = super.read(b);
		if (temp != -1) {
			bytesRead += temp;
		}
		return temp;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int temp = super.read(b, off, len);
		if (temp != -1) {
			bytesRead += temp;
		}
		return temp;
	}

	@Override
	public long skip(long n) throws IOException {
		long temp = super.skip(n);
		if (temp != -1L) {
			bytesRead += (int) temp;
		}

		return temp;
	}

	public static void main(String[] args) throws IOException {
		PosFileInputStream pos = new PosFileInputStream(
				new File(
						"D:\\Muzyka\\Breakbeat\\11\\Pink - Get The Party Started (Malpractice Rmx) (2008).mp3"));
		BufferedInputStream in = new BufferedInputStream(pos);
		
		byte[] buff = new byte[4];
		System.out.println("pos1 = " + pos.getPos());
		in.read(buff);
		System.out.println("pos2 = " + pos.getPos());
		in.close();
	}
}
