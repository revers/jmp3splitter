package mp3;

import java.io.*;

public class EarlyEndException extends IOException {
	private int frames;
	private MP3Header header;
	public EarlyEndException(String msg, MP3Header header, int frames) {
		super(msg);
		this.frames = frames;
		this.header = header;
	}
	
	public int getFrames() {
		return frames;
	}

	public MP3Header getMP3Header() {
		return header;
	}
}
