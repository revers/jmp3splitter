package mp3;

import java.io.*;
import java.nio.charset.Charset;

public class MP3Info {

	private static final int[][] SIDE_INFO = { //
	{ 32, 32, 32, 17 }, // MPEG 1
			{ 17, 17, 17, 9 }, // MPEG 2
			{ 17, 17, 17, 9 }, // MPEG 2.5
	};
	private static final int POINTS_LENGTH = 100;

	public File mp3File;
	public int frames;
	public float msPerFrame;
	public int framesPer5s;
	public int framesPer1min;
	public boolean hasXingFrame;
	public boolean hasInfoFrame;
	public boolean hasVBRIFrame;
	public boolean hasAdditionalFrame;
	public int firstFramePos;
	public float totalMs;

	public boolean fastSeeking;
	public int[] seekPoints;
	public int seekStep;
	private ProgressMonitor monitor;
	private int percent;
	private int truePercent;
	private int lastPercent;

	private MP3Info(File mp3File, ProgressMonitor monitor) {
		this.mp3File = mp3File;
		this.monitor = monitor;
	}

	public static MP3Info scan(File mp3File) throws IOException {
		return scan(mp3File, null);
	}

	public static MP3Info scan(File mp3File, ProgressMonitor monitor)
			throws EarlyEndException, IOException {
		MP3Info m = new MP3Info(mp3File, monitor);

		if (monitor != null) {
			m.percent = 4;
			m.truePercent = (100 - m.percent) / 2;
			monitor.progressMade(m.percent);
		}

		m.firstFramePos = MP3Utils.findFirstFramePos(mp3File);

		byte[] frameBytes = getFrameBytes(mp3File, m.firstFramePos);
		int header = (((frameBytes[0] & 0xff) << 24)
				| ((frameBytes[1] & 0xff) << 16)
				| ((frameBytes[2] & 0xff) << 8) | (frameBytes[3] & 0xff));

		MP3Header mp3Header = null;
		try {
			mp3Header = new MP3Header(header);
		} catch (IncorrectMP3HeaderException e) {
			throw new IOException("ZLY NAGLOWEK: "
					+ MP3Utils.getHeaderLayout(header));
		}

		if (m.hasXingFrame = hasXingHeader(frameBytes, mp3Header))
			;
		else if (m.hasInfoFrame = hasInfoHeader(frameBytes, mp3Header))
			;
		else if (m.hasVBRIFrame = hasVBRIHeader(frameBytes, mp3Header))
			;

		m.hasAdditionalFrame = m.hasXingFrame || m.hasInfoFrame
				|| m.hasVBRIFrame;

		m.msPerFrame = mp3Header
				.getMsPerFrame(m.hasXingFrame || m.hasVBRIFrame);
		m.framesPer5s = Math.round((5000.0f / m.msPerFrame));
		m.framesPer1min = Math.round((60000.0f / m.msPerFrame));

		m.frames = m.getFramesCount(mp3File, m.firstFramePos);

		int tmp = m.hasAdditionalFrame ? m.frames - 1 : m.frames;
		m.totalMs = m.msPerFrame * (float) tmp;

		if (m.frames > m.framesPer1min) {
			if (monitor != null) {
				m.percent += m.truePercent;
				monitor.progressMade(m.percent);
			}

			m.fastSeeking = true;
			m.createSeekPoints();
		}

		return m;
	}

	private int getPercent(int percent, int truePercent) {
		float f = (float) truePercent * ((float) percent / 100.0f);
		return this.percent + Math.round(f);
	}

	private int getFramesCount(File mp3File, int toSkip)
			throws EarlyEndException, IOException {

		int total = toSkip;
		int length = (int) mp3File.length();
		int counter = 0;
		int left = length - toSkip;
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = MP3Utils.getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		left -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);
		// --------------------------------------

		while (in.read(buff) != -1) {
			counter++;

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!MP3Utils.compareHeaders(lastHeader, header)) {

				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					System.out.println("ID3v1 TAG :P");
					counter--;
					break;
				} 
				// NIE OBSLUGUJE LYRICS JEDNAK ;)
//				else if (str.startsWith("LYRI")) {
//					//System.out.println("Lyrics3 Tag :P");
//				
//					// LYRICSBEGIN
//					byte[] strBuff = new byte[7];
//					in.read(strBuff);
//					str = new String(strBuff, Charset.forName("ASCII"));
//					if (str.equals("CSBEGIN")) {
//						System.out.println("Lyrics3 Tag :P");
//						counter--;
//						break;
//					}
//				}
				in.close();
				// System.out.println("total = " + total);
				// System.out.println("counter = " + counter);

				String msg = "NAGLOWKI NIE SA ROWNE:\n[" + (counter - 1)
						+ "]:\n" + MP3Utils.getHeaderLayout(lastHeader) + "\n["
						+ counter + "]:\n" + MP3Utils.getHeaderLayout(header);
				MP3Header lastMp3Header = null;
				try {
					lastMp3Header = new MP3Header(lastHeader);
				} catch (IncorrectMP3HeaderException e) {
					e.printStackTrace();
				}
				lastMp3Header.setStreamPos(total
						- lastMp3Header.getFrameLength());
				throw new EarlyEndException(msg, lastMp3Header, counter);

			}
			lastHeader = header;

			toSkip = MP3Utils.getFrameLength(header) - 4;

			left -= (toSkip + 4);
			if (left < 0)
				toSkip = (toSkip + 4) + left;

			total += toSkip + 4;
			if (monitor != null) {
				long prc = (total * 100L) / length;
				// System.out.println("prc = " + prc);
				int p = getPercent((int) prc, truePercent);
				if (p != lastPercent) {
					lastPercent = p;
					monitor.progressMade(p);
				}
			}

			// if (toSkip > left)
			// toSkip = left;

			while (toSkip != 0)
				toSkip -= in.skip(toSkip);
		}

		in.close();

		// System.out.println("LENGTH = " + mp3File.length());
		return counter;
	}

	private void createSeekPoints() throws IOException {

		seekStep = frames / POINTS_LENGTH;
		seekPoints = new int[POINTS_LENGTH];

		int toSkip = firstFramePos;
		int total = toSkip;

		int index = 0;
		seekPoints[index++] = total;

		int counter = 0;
		int length = (int) mp3File.length();
		int left = length;
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = MP3Utils.getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		left -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		if (seekStep * index == counter)
			seekPoints[index++] = total;
		// --------------------------------------

		while (in.read(buff) != -1) {
			counter++;

			if (index < POINTS_LENGTH && seekStep * index == counter)
				// System.out.println(index++);
				seekPoints[index++] = total;

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!MP3Utils.compareHeaders(lastHeader, header)) {

				if (counter > 1000) {
					// jedyne co mi przyszlo do glowy na bledu typu:
					// Free space or unrecognized Tag after last frame 15791
					// (bytes 16488280-16490368).
					counter--;
					break;
				}

				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					counter--;
					break;
				} else if (str.startsWith("LYRI")) {
					// LYRICSBEGIN
					byte[] strBuff = new byte[7];
					in.read(strBuff);
					str = new String(strBuff, Charset.forName("ASCII"));
					if (str.equals("CSBEGIN")) {
						counter--;
						break;
					}
				}

				// System.out.println("total = " + total);
				throw new IOException("NAGLOWKI NIE SA ROWNE:\n["
						+ (counter - 1) + "]:\n"
						+ MP3Utils.getHeaderLayout(lastHeader) + "\n["
						+ counter + "]:\n" + MP3Utils.getHeaderLayout(header));

			}
			lastHeader = header;

			toSkip = MP3Utils.getFrameLength(header) - 4;

			left -= (toSkip + 4);
			if (left < 0)
				toSkip = (toSkip + 4) + left;

			total += toSkip + 4;
			if (monitor != null) {
				long prc = (total * 100L) / length;
				// System.out.println("prc = " + prc);
				int p = getPercent((int) prc, truePercent);
				if (p != lastPercent) {
					lastPercent = p;
					monitor.progressMade(p);
				}
			}

			while (toSkip > 0)
				toSkip -= in.skip(toSkip);
		}

		in.close();

	}

	public static boolean hasXingHeader(byte[] frameBytes, MP3Header header) {
		int v = header.getMpegVersion();
		int chm = header.getChannelMode();
		int sideInfo = SIDE_INFO[v][chm];

		int pos = 4;
		if (header.hasCRC()) {
			pos += 2;
		}

		for (int i = 0; i < sideInfo; i++, pos++) {
			if (frameBytes[pos] != 0)
				return false;
		}

		byte[] bytes = new byte[4];
		System.arraycopy(frameBytes, pos, bytes, 0, bytes.length);

		String s = new String(bytes, Charset.forName("ASCII"));

		if (s.equals("Xing"))
			return true;

		return false;
	}

	public static boolean hasInfoHeader(byte[] frameBytes, MP3Header header) {
		int v = header.getMpegVersion();
		int chm = header.getChannelMode();
		int sideInfo = SIDE_INFO[v][chm];

		int pos = 4;
		if (header.hasCRC()) {
			pos += 2;
		}

		for (int i = 0; i < sideInfo; i++, pos++) {
			if (frameBytes[pos] != 0)
				return false;
		}

		byte[] bytes = new byte[4];
		System.arraycopy(frameBytes, pos, bytes, 0, bytes.length);

		String s = new String(bytes, Charset.forName("ASCII"));

		if (s.equals("Info"))
			return true;

		return false;
	}

	public static boolean hasVBRIHeader(byte[] frameBytes, MP3Header header) {
		int pos = 4;
		int toSkip = 32;
		if (header.hasCRC()) {
			pos += 2;
			toSkip -= 2;
		}

		for (int i = 0; i < toSkip; i++, pos++) {
			if (frameBytes[pos] != 0)
				return false;
		}

		byte[] bytes = new byte[4];
		System.arraycopy(frameBytes, pos, bytes, 0, bytes.length);

		String s = new String(bytes, Charset.forName("ASCII"));

		if (s.equals("VBRI"))
			return true;

		return false;
	}

	public static byte[] getFrameBytes(File mp3File, int toSkip)
			throws IOException {
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[4];

		in.read(buff);
		int header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

		if (!MP3Utils.isHeaderCorrect(header)) {
			in.close();
			throw new IOException("ZLY NAGLOWEK: "
					+ MP3Utils.getHeaderLayout(header));
		}

		int size = MP3Utils.getFrameLength(header);
		byte[] result = new byte[size];
		System.arraycopy(buff, 0, result, 0, buff.length);

		int total = buff.length;
		int readBytes = 0;
		while (((readBytes = in.read(result, total, result.length - total)) != -1)
				&& total < result.length) {
			total += readBytes;
		}
		in.close();

		return result;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("FILE = " + mp3File.getAbsolutePath());
		sb.append("\nframes\t\t\t = " + frames);
		sb.append("\nmsPerFrame\t\t = " + msPerFrame);
		sb.append("\nframesPer5s\t\t = " + framesPer5s);
		sb.append("\nframesPer1min\t\t = " + framesPer1min);
		sb.append("\nhasAdditionalFrame\t = " + hasAdditionalFrame);
		sb.append("\nhasXingFrame\t\t = " + hasXingFrame);
		sb.append("\nhasInfoFrame\t\t = " + hasInfoFrame);
		sb.append("\nhasVBRIFrame\t\t = " + hasVBRIFrame);
		sb.append("\ntotalMs\t\t\t = " + totalMs);

		int min = (int) totalMs / (1000 * 60);
		int sec = Math.round((totalMs - (float) (1000 * 60 * min)) / 1000.0f);

		sb.append("\nTIME\t\t\t = " + min + ":" + sec);
		sb.append("\nfastSeeking\t\t = " + fastSeeking);
		sb.append("\nFILE_LENGTH\t\t = " + mp3File.length());
		// sb.append("\nPOINTS\t\t\t = \n" + Arrays.toString(seekPoints));

		return sb.toString();
	}

	public static void main(String[] args) throws IOException {
		long before = System.currentTimeMillis();
		File f = new File(
				"H:\\Grand.Theft.Auto.IV-Radio.Station.Rips-AiTB\\Electro-Choc\\01 Padded Cell - Signal Failure.mp3");
		// "D:\\[MUZYKA]\\techno\\02-hot_noizes-saturn www.4clubbers.pl.mp3");
		MP3Info m = MP3Info.scan(f, new ProgressMonitor() {

			@Override
			public void progressMade(int totalPercent) {
				//System.out.println(Integer.toString(totalPercent) + "%");

			}

		});
		System.out.println("SPEED TIME = "
				+ (System.currentTimeMillis() - before));
		System.out.println(m.toString());

	}

}
