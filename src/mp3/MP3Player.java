package mp3;

import javazoom.jl.decoder.*;
import javazoom.jl.player.advanced.*;
import java.io.*;

import utils.*;

public class MP3Player {

	public MP3Info info;
	private PlaybackListener listener;
	private int beginFrame;
	private int endFrame;

	private AdvancedPlayer player;
	private PosFileInputStream stream;
	private Thread runner;

	private int currFrame;

	private int timeStepFrames;
	private int timeStepFrame;

	private float currMsTime;
	private int beginFramePos;
	private boolean playInterrupted = true;
	private boolean paused = true;
	private boolean playing; // for seeking
	private static final int PRE_PLAY_FRAMES = 10;
	private int framesToSkip;

	private ExceptionHandler handler;
	private boolean highPrecisionTimer = false;

	public MP3Player(MP3Info info, PlaybackListener listener)
			throws IOException, JavaLayerException {
		this.info = info;
		this.listener = listener;
		if (info.hasAdditionalFrame)
			beginFrame = 1;
		else
			beginFrame = 0;

		endFrame = info.frames;

		init();
	}

	public MP3Player(MP3Info info, PlaybackListener listener, int beginFrame,
			int endFrame) throws IOException, JavaLayerException {
		this.info = info;
		this.listener = listener;
		this.beginFrame = beginFrame;
		this.endFrame = endFrame;

		init();
	}

	private void init() throws IOException, JavaLayerException {
		timeStepFrames = Math.round(100.0f / info.msPerFrame);

		int skipFrame = beginFrame - PRE_PLAY_FRAMES;
		if (skipFrame < 0) {
			skipFrame = 0;
			framesToSkip = beginFrame;
		} else
			framesToSkip = PRE_PLAY_FRAMES;

		if (info.fastSeeking) {
			int steps = skipFrame / info.seekStep;
			if (steps == info.seekPoints.length)
				steps--;

			int skipBytes = info.seekPoints[steps];
			skipFrame -= steps * info.seekStep;
			beginFramePos = MP3Utils.getFramePos(info.mp3File, skipFrame,
					skipBytes);
		} else {
			beginFramePos = MP3Utils.getFramePos(info.mp3File, skipFrame,
					info.firstFramePos);
		}
		// System.out.printf("skipFrame = %d\nbeginFramePos = %d\n" +
		// "firstFramePos = %d\n", skipFrame, beginFramePos,
		// info.firstFramePos);
		stream = new PosFileInputStream(info.mp3File);
		int toSkip = beginFramePos;
		while (toSkip != 0)
			toSkip -= (int) stream.skip(toSkip);

		player = new AdvancedPlayer(stream);
		for (int i = 0; i < framesToSkip; i++) {
			player.skipFrame();
		}
		currFrame = beginFrame;
		calcCurrMsTime();
	}

	private void reinit() throws IOException, JavaLayerException {
		player.close();

		stream = new PosFileInputStream(info.mp3File);
		int toSkip = beginFramePos;
		while (toSkip != 0)
			toSkip -= (int) stream.skip(toSkip);

		player = new AdvancedPlayer(stream);
		for (int i = 0; i < framesToSkip; i++) {
			player.skipFrame();
		}
		currFrame = beginFrame;
		calcCurrMsTime();
		if (listener != null)
			listener.playbackTimeChanged(this);
	}

	private final void calcCurrMsTime() {
		if (info.hasAdditionalFrame)
			currMsTime = info.msPerFrame * (currFrame - 1);
		else
			currMsTime = info.msPerFrame * currFrame;
	}

	public boolean isHighPrecisionTimer() {
		return highPrecisionTimer;
	}

	public void setHighPrecisionTimer(boolean maxPrecisionTimer) {
		this.highPrecisionTimer = maxPrecisionTimer;
	}

	private class MyRunner extends Thread {
		@Override
		public void run() {
			while (runner != null) {

				if (paused) {
					synchronized (this) {
						try {
							playInterrupted = true;
							wait();
						} catch (InterruptedException e) {
							runner = null;
							e.printStackTrace();
						}
					}
					playInterrupted = false;
				}

				try {
					player.decodeFrame();
				} catch (JavaLayerException ex) {
					ex.printStackTrace();
					if (handler != null)
						handler.handleException("B��d odtwarzania!", ex);
				}

				currFrame++;
				if (highPrecisionTimer) {
					calcCurrMsTime();
					if (listener != null)
						listener.playbackTimeChanged(MP3Player.this);
				} else {
					int diff = currFrame - timeStepFrame;
					if (diff < 0)
						diff = -diff;

					if (diff > timeStepFrames) {
						calcCurrMsTime();
						if (listener != null)
							listener.playbackTimeChanged(MP3Player.this);
						timeStepFrame = currFrame;
					}
				}

				if (currFrame >= endFrame) {
					// System.out.printf("(PLAYER)currFrame = %d; endFrame = %d\n",currFrame,
					// endFrame);
					currFrame = endFrame; // dla > TODO: Potrzebne?
					// if (playbackListener != null)
					runner = null;
					playInterrupted = true;
					playing = false;
					calcCurrMsTime();
					if (listener != null)
						listener.playbackTimeChanged(MP3Player.this);
					if (listener != null)
						listener.playbackFinished(MP3Player.this);
					break;
				}
			}
		}
	}

	public int getCurrMillis() {
		return Math.round(currMsTime);
	}

	public int getCurrFrame() {
		return currFrame;
	}

	public synchronized void play() {
		playing = true;
		if (runner == null) {
			runner = new MyRunner();
			runner.start();
		}
		paused = false;
		synchronized (runner) {
			runner.notify();
		}
		if (listener != null)
			listener.playbackStarted(this);
	}

	public synchronized void pause() {

		playing = false;
		paused = true;

		//while (!playInterrupted)
			Thread.yield();
		if (listener != null)
			listener.playbackPaused(MP3Player.this);

	}

	public synchronized void stop() throws IOException, JavaLayerException {
		playing = false;
		paused = true;
		while (!playInterrupted)
			Thread.yield();
		reinit();
		if (listener != null)
			listener.playbackStoped(this);
	}

	public synchronized void close() {
		paused = true;
		runner = null;
		player.close();
	}

	public synchronized void seek(int frame) throws JavaLayerException,
			IOException {
		// System.out.printf("currFrame = %d; frame = %d\n", currFrame, frame);

		if (playing) {
			paused = true;
			while (!playInterrupted)
				Thread.yield();
		}

		if (frame > currFrame) {

			int diff = frame - currFrame;
			// /*
			// TODO: zostawic to - PRE_PLAY_FRAMES?
			if (diff < info.framesPer1min - PRE_PLAY_FRAMES
					|| !info.fastSeeking) {

				// */
				// System.out.println("seeking, diff = " + diff);
				for (; diff > 0; diff--)
					player.skipFrame();
				// /*
			} else {
				player.notFullClose();
				int tmp = frame - PRE_PLAY_FRAMES;
				tmp = tmp / info.seekStep;
				int frSkip = frame - tmp * info.seekStep;

				// TODO: Sprawdzic czy potrzebne:
				if (tmp == info.seekPoints.length)
					tmp--;
				// -------------------------------

				tmp = info.seekPoints[tmp];
				tmp -= stream.getPos();
				while (tmp != 0)
					tmp -= stream.skip(tmp);

				player = new AdvancedPlayer(stream);
				for (int i = 0; i < frSkip; i++) {
					player.skipFrame();
				}
			}
			// */
		} else if (info.fastSeeking) {
			player.close();
			stream = new PosFileInputStream(info.mp3File);

			int skipFrame = frame - PRE_PLAY_FRAMES;

			if (skipFrame < 0) {
				skipFrame = 0;
			}

			skipFrame = skipFrame / info.seekStep;
			if (skipFrame == info.seekPoints.length)
				skipFrame--;

			int frSkip = frame - skipFrame * info.seekStep;

			skipFrame = info.seekPoints[skipFrame];
			while (skipFrame != 0)
				skipFrame -= stream.skip(skipFrame);

			player = new AdvancedPlayer(stream);
			for (int i = 0; i < frSkip; i++) {
				player.skipFrame();
			}

		} else {
			player.close();

			stream = new PosFileInputStream(info.mp3File);
			int toSkip = beginFramePos;
			while (toSkip != 0)
				toSkip -= (int) stream.skip(toSkip);

			player = new AdvancedPlayer(stream);
			int frSkip = framesToSkip + (frame - beginFrame);
			for (int i = 0; i < frSkip; i++) {
				player.skipFrame();
			}
		}

		currFrame = frame;
		calcCurrMsTime();

		if (playing) {
			paused = false;
			synchronized (runner) {
				runner.notify();
			}
		}
		if (listener != null)
			listener.playbackTimeChanged(this);

	}

	public synchronized void seekAhead5s() throws JavaLayerException,
			IOException {
		int frame = currFrame + info.framesPer5s;
		if (frame > endFrame)
			frame = endFrame;
		seek(frame);
	}

	public synchronized void seekBack5s() throws JavaLayerException,
			IOException {
		int frame = currFrame - info.framesPer5s;
		if (frame < beginFrame) {
			frame = beginFrame;
		}
		seek(frame);
	}

	public synchronized void seekAhead1min() throws JavaLayerException,
			IOException {
		int frame = currFrame + info.framesPer1min;
		if (frame > endFrame)
			frame = endFrame;
		seek(frame);
	}

	public synchronized void seekBack1min() throws JavaLayerException,
			IOException {
		int frame = currFrame - info.framesPer1min;
		if (frame < beginFrame) {
			frame = beginFrame;
		}
		seek(frame);
	}

	public PlaybackListener getListener() {
		return listener;
	}

	public void setListener(PlaybackListener listener) {
		this.listener = listener;
	}

	public int getBeginFrame() {
		return beginFrame;
	}

	public synchronized void setBeginFrame(int beginFrame) throws IOException,
			JavaLayerException {
		this.beginFrame = beginFrame;
		if (playing) {
			paused = true;
			while (!playInterrupted)
				Thread.yield();
		}
		init();
		if (playing) {
			paused = false;
			synchronized (runner) {
				runner.notify();
			}
		}

	}

	public int getEndFrame() {
		return endFrame;
	}

	public synchronized void setEndFrame(int endFrame) {
		this.endFrame = endFrame;
	}

	public MP3Info getInfo() {
		return info;
	}

	public ExceptionHandler getHandler() {
		return handler;
	}

	public void setHandler(ExceptionHandler handler) {
		this.handler = handler;
	}

	public synchronized boolean isPlaying() {
		return playing;
	}

	public synchronized int getStreamPosition() {
		return stream.getPos();
	}

}
