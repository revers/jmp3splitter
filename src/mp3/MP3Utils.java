package mp3;

import java.io.*;
import java.nio.charset.Charset;

public class MP3Utils {
	private static final int SAME_FRAMES = 5;
	private static final int BUFFER_SIZE = (433 * 4 + 8) * SAME_FRAMES;

	// Special Values:
	private static final int FREE = 0;
	private static final int BAD = -1;
	private static final int RESERVED = Integer.MIN_VALUE;

	private static final int[][][] BITRATES = {
			{
					{ FREE, 32000, 64000, 96000, 128000, 160000, 192000,
							224000, 256000, 288000, 320000, 352000, 384000,
							416000, 448000, BAD }, // V1_L1
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 160000, 192000, 224000, 256000, 320000,
							384000, BAD }, // V1_L2
					{ FREE, 32000, 40000, 48000, 56000, 64000, 80000, 96000,
							112000, 128000, 160000, 192000, 224000, 256000,
							320000, BAD }, // V1_L3
			},
			{
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 144000, 160000, 176000, 192000, 224000,
							256000, BAD }, // V2_L1
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD }, // V2_L2
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD } // V2_L3
			},
			{
					{ FREE, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
							128000, 144000, 160000, 176000, 192000, 224000,
							256000, BAD }, // V2.5_L1
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD }, // V2.5_L2
					{ FREE, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
							64000, 80000, 96000, 112000, 128000, 144000,
							160000, BAD } // V2.5_L3
			} };

	private static final int[][] SAMPLERATES = {
			{ 44100, 48000, 32000, RESERVED }, // MPEG1
			{ 22050, 24000, 16000, RESERVED }, // MPEG2
			{ 11025, 12000, 8000, RESERVED } // MPEG2.5
	};

	private static final int[][] SAMPLES_PER_FRAME = { //
	{ 384, 1152, 1152 }, // MPEG-1
			{ 384, 1152, 576 }, // MPEG-2
			{ 384, 1152, 576 }, // MPEG-2.5
	};

	// 1111 1111 1110 0000 0000 0000 0000 0000
	private static final int SYNC_BITS = 0xFFE00000;

	// MPEG Versions:
	private static final int MPEG1 = 0;
	private static final int MPEG2 = 1;
	private static final int MPEG2_5 = 2;

	// Layer Version:
	private static final int LAYER_I = 0;
	private static final int LAYER_II = 1;
	private static final int LAYER_III = 2;

	// Channel Modes:
	private static final int STEREO = 0;
	private static final int JOINT_STEREO = 1;
	private static final int DUAL_CHANNEL = 2;
	private static final int MONO = 3;

	// AAAAAAAAAAA BB CC D EEEE FF G H II JJ K L MM
	// 11111111111 11 11 0 0000 11 0 0 00 00 0 0 11
	private static final int COMPARE_MASK = 0xFFFE0C03;

	public static boolean isHeaderCorrect(int header) {
		if ((header & SYNC_BITS) != SYNC_BITS)
			return false;

		int ver = (header >> 19) & 3;
		int lay = (header >> 17) & 3;
		// (MPEGVersion == RESERVED) || (MPEGLayer == RESERVED)
		if ((ver == 1) || (lay == 0))
			return false;

		// channel_mode != JOINT_STEREO && mod_extensions != 0
		if (((header >> 6) & 3) != JOINT_STEREO && ((header >> 4) & 3) != 0)
			return false;

		switch (ver) {
		case 0:
			ver = MPEG2_5;
			break;
		case 2:
			ver = MPEG2;
			break;
		case 3:
			ver = MPEG1;
			break;
		}

		int srIndex = (header >> 10) & 3;
		if (SAMPLERATES[ver][srIndex] == RESERVED)
			return false;

		switch (lay) {
		case 1:
			lay = LAYER_III;
			break;
		case 2:
			lay = LAYER_II;
			break;
		case 3:
			lay = LAYER_I;
			break;
		}

		int brIndex = (header >> 12) & 15;
		if (BITRATES[ver][lay][brIndex] == BAD)
			return false;

		return true;
	}

	public static boolean compareHeaders(int header1, int header2) {
		if ((header1 & COMPARE_MASK) != (header2 & COMPARE_MASK))
			return false;

		// from mono to stereo never possible
		if ((((header1 >> 6) & 3) == MONO) != (((header2 >> 6) & 3) == MONO))
			return false;

		return true;
	}

	private static final String insertSpace(String s, int pos) {
		return s.substring(0, pos) + " " + s.substring(pos);
	}

	public static void printHeaderLayout(int frameHeader) {
		System.out.println(getHeaderLayout(frameHeader));
	}

	public static String getHeaderLayout(int frameHeader) {
		String s = asBinaryString(frameHeader);

		s = insertSpace(s, 11);
		s = insertSpace(s, 14);
		s = insertSpace(s, 17);
		s = insertSpace(s, 19);
		s = insertSpace(s, 24);
		s = insertSpace(s, 27);
		s = insertSpace(s, 29);
		s = insertSpace(s, 31);
		s = insertSpace(s, 34);
		s = insertSpace(s, 37);
		s = insertSpace(s, 39);
		s = insertSpace(s, 41);

		return "AAAAAAAAAAA BB CC D EEEE FF G H II JJ K L MM\n" + s;

	}

	private static String asBinaryString(int i) {
		StringBuffer buf = new StringBuffer();
		int mask = 1;
		for (int j = 31; j >= 0; j--) {
			if (((mask << j) & i) != 0)
				buf.append("1");
			else
				buf.append("0");
		}

		return buf.toString();
	}

	public static int getFrameLength(int header) {
		int ver = (header >> 19) & 3;
		int lay = (header >> 17) & 3;

		switch (ver) {
		case 0:
			ver = MPEG2_5;
			break;
		case 2:
			ver = MPEG2;
			break;
		case 3:
			ver = MPEG1;
			break;
		}

		switch (lay) {
		case 1:
			lay = LAYER_III;
			break;
		case 2:
			lay = LAYER_II;
			break;
		case 3:
			lay = LAYER_I;
			break;
		}

		int srIndex = (header >> 10) & 3;
		int sr = SAMPLERATES[ver][srIndex];

		int brIndex = (header >> 12) & 15;
		int br = BITRATES[ver][lay][brIndex];

		int padding = 0;
		if (((header >> 17) & 3) == LAYER_I) {
			if (((header >> 9) & 1) == 1)
				padding = 4;
			return (12 * br / sr + padding) * 4;
		} else {
			// System.out.printf("br = %d, sr = %d, padding = %d\n", br, sr,
			// padding);
			if (((header >> 9) & 1) == 1)
				padding = 1;
			return 144 * br / sr + padding;
		}
	}

	private static int findFirstFramePos(File mp3File, int toSkip)
			throws IOException {

		// System.out.println("toSkip = " + toSkip);
		int startPos = toSkip;
		int fileLength = (int) mp3File.length();
		if (toSkip >= fileLength)
			return -1;

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		while (toSkip != 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[BUFFER_SIZE];
		int readBytes = 0;
		int total = 0;
		while (((readBytes = in.read(buff, total, buff.length - total)) != -1)
				&& total < buff.length) {
			total += readBytes;
		}
		in.close();

		int pos = 0;
		int sameFrames = 0;
		int header = 0;
		int returnPos = -1;
		int lastHeader = -1;

		while (total >= pos + 4) {
			// System.out.println("pos = " + pos);
			header = (((buff[pos] & 0xff) << 24)
					| ((buff[pos + 1] & 0xff) << 16)
					| ((buff[pos + 2] & 0xff) << 8) | (buff[pos + 3] & 0xff));

			if (!isHeaderCorrect(header)
					|| (lastHeader != -1 && !compareHeaders(header, lastHeader))) {
				if (returnPos != -1) {
					pos = returnPos;
					returnPos = -1;
					lastHeader = -1;
				} else {
					pos++;

				}
				sameFrames = 0;
				continue;
			}

			sameFrames++;
			// System.out.println("sameFrames = " + sameFrames);
			if (sameFrames == SAME_FRAMES)
				return startPos + returnPos - 1;

			if (lastHeader == -1)
				lastHeader = header;

			if (returnPos == -1)
				returnPos = pos + 1;

			int frameLength = getFrameLength(header);
			// System.out.println("frameLength = " + frameLength + "\n");
			if (frameLength == 0
					|| (startPos + pos + frameLength + 4 > fileLength)) {
				pos = returnPos;
				returnPos = -1;
				sameFrames = 0;
				lastHeader = -1;
				continue;
			} else if (pos + frameLength + 4 > total) {
				buff = null; // help to gc?
				return findFirstFramePos(mp3File, startPos + returnPos - 1);
			}
			pos += frameLength;

		}

		if (startPos + pos + 4 <= fileLength) {
			buff = null; // help to gc?
			return findFirstFramePos(mp3File, startPos + pos);
		}

		return -1;
	}

	public static int findFirstFramePos(File mp3File) throws IOException {
		return findFirstFramePos(mp3File, 0);
	}

	public static MP3Header findFirstHeader(File mp3File) throws IOException {
		return findFirstHeader(mp3File, 0);
	}

	public static MP3Header findFirstHeader(File mp3File, int toSkip)
			throws IOException {

		// System.out.println("toSkip = " + toSkip);
		int startPos = toSkip;
		int fileLength = (int) mp3File.length();
		if (toSkip >= fileLength)
			return null;// -1;

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		while (toSkip != 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[BUFFER_SIZE];
		int readBytes = 0;
		int total = 0;
		while (((readBytes = in.read(buff, total, buff.length - total)) != -1)
				&& total < buff.length) {
			total += readBytes;
		}
		in.close();

		int pos = 0;
		int sameFrames = 0;
		int header = 0;
		int returnPos = -1;
		int lastHeader = -1;

		int firstHeader = -1;

		while (total >= pos + 4) {
			// System.out.println("pos = " + pos);
			header = (((buff[pos] & 0xff) << 24)
					| ((buff[pos + 1] & 0xff) << 16)
					| ((buff[pos + 2] & 0xff) << 8) | (buff[pos + 3] & 0xff));

			if (!isHeaderCorrect(header)
					|| (lastHeader != -1 && !compareHeaders(header, lastHeader))) {
				if (returnPos != -1) {
					pos = returnPos;
					returnPos = -1;
					lastHeader = -1;
				} else {
					pos++;
				}
				sameFrames = 0;
				continue;
			}

			if (sameFrames == 0)
				firstHeader = header;

			sameFrames++;
			// System.out.println("sameFrames = " + sameFrames);
			if (sameFrames == SAME_FRAMES) {
				MP3Header mp3Header = null;
				try {
					mp3Header = new MP3Header(firstHeader);
					mp3Header.setStreamPos(startPos + returnPos - 1);
				} catch (IncorrectMP3HeaderException e) {
					e.printStackTrace();
				}

				return mp3Header;
			}

			if (lastHeader == -1)
				lastHeader = header;

			if (returnPos == -1)
				returnPos = pos + 1;

			int frameLength = getFrameLength(header);
			// System.out.println("frameLength = " + frameLength + "\n");
			if (frameLength == 0
					|| (startPos + pos + frameLength + 4 > fileLength)) {
				pos = returnPos;
				returnPos = -1;
				sameFrames = 0;
				lastHeader = -1;
				continue;
			} else if (pos + frameLength + 4 > total) {
				buff = null; // help to gc?
				return findFirstHeader(mp3File, startPos + returnPos - 1);
			}
			pos += frameLength;

		}

		if (startPos + pos + 4 <= fileLength) {
			buff = null; // help to gc?
			return findFirstHeader(mp3File, startPos + pos);
		}

		return null;
	}

	public static int getFramesCount(File mp3File, int toSkip)
			throws EarlyEndException, IOException {

		int counter = 0;
		int left = (int) mp3File.length();
		BufferedInputStream in = new BufferedInputStream(
				new FileInputStream(mp3File));

		left -= toSkip;
		int total = toSkip;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		left -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);
		// --------------------------------------

		while (in.read(buff) != -1) {
			counter++;

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!compareHeaders(lastHeader, header)) {

				counter--;

				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					break;
				} else if (str.startsWith("LYRI")) {
					// LYRICSBEGIN
					byte[] strBuff = new byte[7];
					in.read(strBuff);
					str = new String(strBuff, Charset.forName("ASCII"));
					if (str.equals("CSBEGIN")) {
						break;
					}
				}

				in.close();
				// System.out.println("total = " + total);
				String msg = "NAGLOWKI NIE SA ROWNE:\n[" + (counter - 1)
						+ "]:\n" + getHeaderLayout(lastHeader) + "\n["
						+ counter + "]:\n" + getHeaderLayout(header);
				MP3Header lastMp3Header = null;
				try {
					lastMp3Header = new MP3Header(lastHeader);
				} catch (IncorrectMP3HeaderException e) {
					e.printStackTrace();
				}
				lastMp3Header.setStreamPos(total - lastMp3Header.getFrameLength());
				throw new EarlyEndException(msg, lastMp3Header, counter);

			}
			lastHeader = header;

			toSkip = getFrameLength(header) - 4;

			left -= (toSkip + 4);
			if (left < 0)
				toSkip = (toSkip + 4) + left;

			total += toSkip + 4;

			while (toSkip > 0)
				toSkip -= in.skip(toSkip);
		}

		in.close();

		//System.out.println("LENGTH = " + mp3File.length());
		return counter;
	}

	public static int getFramesCount(File mp3File) throws IOException {
		int toSkip = findFirstFramePos(mp3File);
		return getFramesCount(mp3File, toSkip);
	}

	public static int findFramePos(MP3Info info, int ms) throws IOException {
		int frame = Math.round(((float) ms / info.msPerFrame));
		if (info.hasAdditionalFrame)
			frame++;
		return getFramePos(info.mp3File, frame);
	}

	// -------------------------------------------------------------------
	public static int getFramePos(File mp3File, int frame) throws IOException {
		int toSkip = findFirstFramePos(mp3File);
		return getFramePos(mp3File, frame, toSkip);
	}

	public static int getFramePos(File mp3File, int frame, int toSkip)
			throws IOException {
		if (frame == 0)
			return toSkip;

		int total = toSkip;
		int counter = 0;
		int left = (int) mp3File.length();
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		left -= toSkip;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		// System.out.println(total);
		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		if (frame == 1)
			return total;

		left -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);
		// --------------------------------------

		while (in.read(buff) != -1) {
			counter++;

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!compareHeaders(lastHeader, header)) {
				counter--;
				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					// counter--;
					break;
				} else if (str.startsWith("LYRI")) {
					// LYRICSBEGIN
					byte[] strBuff = new byte[7];
					in.read(strBuff);
					str = new String(strBuff, Charset.forName("ASCII"));
					if (str.equals("CSBEGIN")) {
						// counter--;
						break;
					}
				}

				in.close();
				// System.out.println("total = " + total);
				throw new IOException("NAGLOWKI NIE SA ROWNE:\n["
						+ (counter - 1) + "]:\n" + getHeaderLayout(lastHeader)
						+ "\n[" + counter + "]:\n" + getHeaderLayout(header));

			}
			lastHeader = header;

			toSkip = MP3Utils.getFrameLength(header) - 4;

			left -= (toSkip + 4);
			if (left < 0)
				toSkip = (toSkip + 4) + left;

			total += toSkip + 4;
			if (counter == frame)
				break;

			while (toSkip > 0)
				toSkip -= in.skip(toSkip);
		}

		in.close();

		// System.out.println("LENGTH = " + mp3File.length());
		return total;
	}

	private static void testFixMp3(File f) {
		int frames = 0;
		try {
			frames = getFramesCount(f);
		} catch (EarlyEndException e) {
			int pos = -1;
			try {
				MP3Header header = findFirstHeader(f, e.getMP3Header().getStreamPos());// getFramePos(f,
				// e.getFrames()));
				pos = header.getStreamPos();
				System.out
						.println("COMPARE HEADERS: "
								+ e.getMP3Header().equals(header));
								//compareHeaders(e.getLastHeader(), header
									//	.getHeaderBits()));
			} catch (IOException e1) {
			}
			System.out.println(pos);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		System.out.println("START");
		File f = new File(
				"D:\\Revers\\[ECLIPSE]\\Carl_Cox_-_Mix_on_Kiss_100_(Recorded_Live_at_Amnesia_Ibiza)_-_26-June-2009_mixing.dj.mp3");
		testFixMp3(f);
		System.out.println("END");
	}

}
