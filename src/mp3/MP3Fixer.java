package mp3;

import java.io.*;
import java.nio.charset.Charset;

public class MP3Fixer {

	private class FramesCount {
		private int frames;
		private int streamEndPos;

		public FramesCount(int frames, int streamEndPos) {
			this.frames = frames;
			this.streamEndPos = streamEndPos;
		}
	}

	private static final int BUFFER_SIZE = 1024;
	private File inFile;
	private File outFile;
	private ProgressMonitor monitor;

	public MP3Fixer(File inFile, File outFile) {
		this(inFile, outFile, null);
	}

	public MP3Fixer(File inFile, File outFile, ProgressMonitor monitor) {
		this.inFile = inFile;
		this.outFile = outFile;
		this.monitor = monitor;
	}

	public void fix() throws IOException {

		MP3Header firstHeader = MP3Utils.findFirstHeader(inFile);
		byte[] frameBytes = MP3Info.getFrameBytes(inFile, firstHeader
				.getStreamPos());
		int toSkip = firstHeader.getStreamPos();
		boolean isVBR = false;
		if (MP3Info.hasXingHeader(frameBytes, firstHeader)
				|| MP3Info.hasVBRIHeader(frameBytes, firstHeader)) {
			toSkip += firstHeader.getFrameLength();
			isVBR = true;
		} else if (MP3Info.hasInfoHeader(frameBytes, firstHeader)) {
			toSkip += firstHeader.getFrameLength();
		}

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				inFile));
		BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(outFile));

		int pos = toSkip;
		while (toSkip != 0)
			toSkip -= in.skip(toSkip);

		int frames = 0;
		boolean continueLoop = true;

		while (continueLoop) {
			int toWrite = 0;
			try {
				// System.out.println("early pos = " + pos);
				FramesCount fc = getFramesCount(inFile, pos);
				frames += fc.frames;
				toWrite = fc.streamEndPos - pos;
				// System.out.println("FramesCounter.streamEndPos = "
				// + fc.streamEndPos);
				toSkip = 0;
				continueLoop = false;
				pos = fc.streamEndPos;//toWrite;
				
			} catch (EarlyEndException e) {
				// System.out
				// .printf(
				// "e.getMP3Header().getStreamPos() = %d\ne.getMP3Header().getFrameLength() = %d\npos = %d\n",
				// e.getMP3Header().getStreamPos(), e
				// .getMP3Header().getFrameLength(), pos);
				toWrite = e.getMP3Header().getStreamPos()
						+ e.getMP3Header().getFrameLength() - pos;
				frames += e.getFrames();
				// System.out.println("frames = " + frames);

				MP3Header header = MP3Utils.findFirstHeader(inFile, e
						.getMP3Header().getStreamPos());

				if (header == null) {
					continueLoop = false;
					toSkip = 0;
					pos += toWrite;
				} else if (!e.getMP3Header().equals(header)) {
					continueLoop = false;
					toSkip = 0;
					pos = header.getStreamPos();
				} else {
					toSkip = header.getStreamPos() - pos - toWrite;
					pos = header.getStreamPos();
				}
			}
			write(in, out, toWrite, toSkip);
			if (monitor != null) {
				int prc = Math.round((float) pos / (float) inFile.length() * 100.0f);
				if(prc > 98)
					prc = 98;
				
				monitor.progressMade(prc);
			}
		}

		byte[] buff = new byte[3]; // [11]
		int readBytes = 0;
		int toRead = buff.length;
		while (toRead > 0
				&& (readBytes = in.read(buff, buff.length - toRead, toRead)) != -1)
			toRead -= readBytes;
		String str = new String(buff, Charset.forName("ASCII"));
		if (str.startsWith("TAG")) { // || str.equals("LYRICSBEGIN")) {
			out.write(buff, 0, buff.length - toRead);
			write(in, out,
					(int) inFile.length() - pos - (buff.length - toRead), 0);
		}
		in.close();
		out.close();
		//System.out.println("frames = " + frames);
		//System.out.println(outFile.length());

		String frameStr = "Info";
		if (isVBR)
			frameStr = "Xing";
		frameBytes = MP3Cutter.createVBRFrame(firstHeader, frameStr, frames,
				(int) outFile.length());
		in = new BufferedInputStream(new FileInputStream(outFile));
		File tempFile = new File(outFile.getAbsolutePath() + ".temp");
		out = new BufferedOutputStream(new FileOutputStream(tempFile));
		out.write(frameBytes);
		write(in, out, (int) outFile.length(), 0);
		in.close();
		out.close();

		if (!outFile.delete())
			throw new IOException("Nie moge usunac pliku:\n"
					+ outFile.getAbsolutePath());
		if (!tempFile.renameTo(outFile))
			throw new IOException("Nie moge zmienic nazwy z:\n"
					+ tempFile.getAbsolutePath() + "\nna\n"
					+ outFile.getAbsolutePath());
		if(monitor != null)
			monitor.progressMade(100);
	}

	private void write(InputStream in, OutputStream out, int toWrite, int toSkip)
			throws IOException {
		byte[] buff = new byte[BUFFER_SIZE];
		int toRead = toWrite <= buff.length ? toWrite : buff.length;
		int readBytes = 0;
		while (toWrite > 0 && (readBytes = in.read(buff, 0, toRead)) != -1) {
			toWrite -= readBytes;
			toRead = toWrite <= buff.length ? toWrite : buff.length;
			out.write(buff, 0, readBytes);
		}

		while (toSkip != 0)
			toSkip -= in.skip(toSkip);
	}

	private FramesCount getFramesCount(File mp3File, int toSkip)
			throws EarlyEndException, IOException {

		int counter = 0;
		int left = (int) mp3File.length();

		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				mp3File));

		int total = toSkip;
		left -= toSkip;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);

		byte[] buff = new byte[4];
		int header = 0;

		// --------------------------------------
		in.read(buff);
		int lastHeader = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		toSkip = MP3Utils.getFrameLength(lastHeader) - 4;
		total += toSkip + 4;
		left -= (toSkip + 4);
		counter++;
		while (toSkip > 0)
			toSkip -= in.skip(toSkip);
		// --------------------------------------

		while (in.read(buff) != -1) {
			counter++;

			header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
					| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));

			if (!MP3Utils.compareHeaders(lastHeader, header)) {

				counter--;

				String str = new String(buff, Charset.forName("ASCII"));
				if (str.startsWith("TAG")) {
					break;
				} else if (str.startsWith("LYRI")) {
					// LYRICSBEGIN
					byte[] strBuff = new byte[7];
					in.read(strBuff);
					str = new String(strBuff, Charset.forName("ASCII"));
					if (str.equals("CSBEGIN")) {
						break;
					}
				}

				in.close();
				// System.out.println("total = " + total);
				String msg = "NAGLOWKI NIE SA ROWNE:\n[" + (counter - 1)
						+ "]:\n" + MP3Utils.getHeaderLayout(lastHeader) + "\n["
						+ counter + "]:\n" + MP3Utils.getHeaderLayout(header);
				MP3Header lastMp3Header = null;
				try {
					lastMp3Header = new MP3Header(lastHeader);
				} catch (IncorrectMP3HeaderException e) {
					e.printStackTrace();
				}
				lastMp3Header.setStreamPos(total
						- lastMp3Header.getFrameLength());
				throw new EarlyEndException(msg, lastMp3Header, counter);

			}
			lastHeader = header;

			toSkip = MP3Utils.getFrameLength(header) - 4;

			left -= (toSkip + 4);
			if (left < 0)
				toSkip = (toSkip + 4) + left;

			total += toSkip + 4;

			while (toSkip > 0)
				toSkip -= in.skip(toSkip);
			
			if (monitor != null) {
				int prc = Math.round((float) total / (float) inFile.length() * 100.0f);
				if(prc > 98)
					prc = 98;
				monitor.progressMade(prc);
			}
		}

		in.close();

		// System.out.println("LENGTH = " + mp3File.length());
		return new FramesCount(counter, total);
	}

//	private void test() throws IOException {
//		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
//				inFile));
//		int toSkip = 73468;
//		while (toSkip != 0)
//			toSkip -= in.skip(toSkip);
//
//		BufferedOutputStream out = new BufferedOutputStream(
//				new FileOutputStream(outFile));
//		byte[] buff = new byte[BUFFER_SIZE];
//		int readBytes;
//		while ((readBytes = in.read(buff)) != -1)
//			out.write(buff, 0, readBytes);
//
//		in.close();
//		out.close();
//	}
//
//	public static void main(String[] args) throws IOException {
//		System.out.println("START");
//		File inFile = new File(
//				"D:\\Revers\\[ECLIPSE]\\Carl_Cox_-_Mix_on_Kiss_100_(Recorded_Live_at_Amnesia_Ibiza)_-_26-June-2009_mixing.dj.mp3");
//		File outFile = new File(
//				"D:\\Revers\\[ECLIPSE]\\Carl_Cox_-_Mix_on_Kiss_100_(Recorded_Live_at_Amnesia_Ibiza)_-_26-June-2009_mixing.dj (FIXED).mp3");
//		// int frame = 139;
//		// System.out
//		// .println("frame pos = " + MP3Utils.getFramePos(inFile, frame));
//
//		MP3Fixer fixer = new MP3Fixer(inFile, outFile);
//		fixer.fix();
//		// fixer.test();
//		System.out.println("END");
//	}

}
