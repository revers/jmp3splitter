package mp3;

public interface PlaybackListener {
	public void playbackStarted(MP3Player player);
	public void playbackPaused(MP3Player player);
	public void playbackStoped(MP3Player player);
	public void playbackFinished(MP3Player player);
	public void playbackTimeChanged(MP3Player player);
}
