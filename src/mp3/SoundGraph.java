package mp3;

import javazoom.jl.decoder.*;

import java.io.*;
import utils.*;

public class SoundGraph {
	//private static final float MAX_AMPLITUDE = 20000;

	//private MP3Player player;
	private MP3Info info;
	private int frames;
	private int beginFrame;
	private float greatestValue;

	public SoundGraph(MP3Info info, int beginFrame, int frames) {
		this.info = info;
		this.frames = frames;
		this.beginFrame = beginFrame;
	}

	public float[] getValues() throws IOException,
			JavaLayerException {
		float[] result = new float[frames];
		int toSkip = 0;
		
		if (info.fastSeeking) {
			int steps = beginFrame / info.seekStep;
			if (steps == info.seekPoints.length)
				steps--;

			int skipBytes = info.seekPoints[steps];
			int skipFrame = beginFrame - steps * info.seekStep;
			toSkip = MP3Utils.getFramePos(info.mp3File, skipFrame,
					skipBytes);
		} else {
			toSkip = MP3Utils.getFramePos(info.mp3File, beginFrame,
					info.firstFramePos);
		}

		FileInputStream stream = new FileInputStream(info.mp3File);
		while (toSkip != 0)
			toSkip -= stream.skip(toSkip);
		
//		System.out.println("player pos = " + player.getCurrFrame()
//				+ "; frames = " + frames + "; totalFrames = "
//				+ player.info.frames + "; curr + frames = "
//				+ (player.getCurrFrame() + frames) + "; stream pos = " + stream.getPos());

		Bitstream bitstream = new Bitstream(stream);
		Decoder decoder = new Decoder();

		for (int i = 0; i < frames; i++) {

			Header h = bitstream.readFrame();
			if (h == null) {
				throw new IOException("Nie ma ramki O_o ;)");
//				[ ramka = "
//						+ (player.getCurrFrame() + i) + "]");
			}

			SampleBuffer output = (SampleBuffer) decoder.decodeFrame(h,
					bitstream);

			bitstream.closeFrame();
			float avg = averageAplitude(output.getBuffer());
			
			//if(avg != 0)
			//throw new Error("output.getBuffer().length = " + output.getBuffer().length);
			
			result[i] = avg;
			
			if(result[i] > greatestValue) {
				greatestValue = result[i];
				//System.out.println(result[i]);
			}
//			if (avg > MAX_AMPLITUDE) {
//				throw new IOException("A jednak sa wieksze od " + MAX_AMPLITUDE
//						+ ": " + avg);
//			}
//			avg = avg / MAX_AMPLITUDE;
//			result[i] = (int) (avg * (float) amplitude);
		}

		return result;
	}

	public float getGreatestValue() {
		return greatestValue;
	}

	private static float averageAplitude(short[] arr) {
		int avg = 0;
		for (short sh : arr) {
			if (sh < 0)
				avg += -(int) sh;
			else
				avg += (int) sh;
		}
		return (float) avg / (float) arr.length;
	}
}
