import javazoom.jl.decoder.*;

import java.io.*;

import mp3.*;

public class SoundGraph {

	MP3Info info;

	public SoundGraph(MP3Info info) {
		this.info = info;
	}

	private static final float MAX_AMPLITUDE = 20000;

	public int[] getValues(int millis, int amplitude) throws IOException,
			JavaLayerException {
		int frames = Math.round((float) millis / info.msPerFrame);
		int[] result = new int[frames];
		System.out.println("frames = " + frames);

		FileInputStream stream = new FileInputStream(info.mp3File);
		int toSkip = info.firstFramePos;
		while (toSkip != 0)
			toSkip -= stream.skip(toSkip);

		Bitstream bitstream = new Bitstream(stream);
		Decoder decoder = new Decoder();

		for (int i = 0; i < result.length; i++) {
			Header h = bitstream.readFrame();
			if (h == null) {
				throw new IOException("Nie ma ramki O_o ;)");
			}

			SampleBuffer output = (SampleBuffer) decoder.decodeFrame(h,
					bitstream);

			bitstream.closeFrame();
			float avg = averageAplitude(output.getBuffer());
			if (avg > MAX_AMPLITUDE) {
				throw new IOException("A jednak sa wieksze od " + MAX_AMPLITUDE
						+ ": " + avg);
			}
			avg = avg / MAX_AMPLITUDE;
			result[i] = (int) (avg * (float) amplitude);
		}

		return result;
	}

	private static float averageAplitude(short[] arr) {
		int avg = 0;
		for (short sh : arr) {
			if (sh < 0)
				avg += -(int)sh;
			else
				avg += (int)sh;
		}
		return (float) avg / (float) arr.length;
	}

	public static void main(String[] args) {

	}

}
