package gui;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.*;

import javax.swing.*;
import javax.swing.border.*;
import utils.*;

public class TimeSpinnerModel extends AbstractSpinnerModel {
	private TimeData timeData;
	private TimeSpinnerEditor tsEditor;

	public TimeSpinnerModel(TimeData td, TimeSpinnerEditor tse) {
		this.timeData = td;
		this.tsEditor = tse;
	}

	@Override
	public Object getNextValue() {
		boolean valueChanged = false;
		switch (tsEditor.getSelectedField()) {
		case TimeData.HOUR:
			valueChanged = timeData.increaseHours();
			break;
		case TimeData.MINUTE:
			valueChanged = timeData.increaseMinutes();
			break;
		case TimeData.SECOND:
			valueChanged = timeData.increaseSeconds();
			break;
		case TimeData.HUNDRED_MILLIS:
			valueChanged = timeData.addHudredMillis();
			break;
		}

		if (valueChanged) {
			//fireStateChanged();
			tsEditor.updateValue();
		}
		return timeData;
	}

	@Override
	public Object getPreviousValue() {
		boolean valueChanged = false;
		switch (tsEditor.getSelectedField()) {
		case TimeData.HOUR:
			valueChanged = timeData.decreaseHours();
			break;
		case TimeData.MINUTE:
			valueChanged = timeData.decreaseMinutes();
			break;
		case TimeData.SECOND:
			valueChanged = timeData.decreaseSeconds();
			break;
		case TimeData.HUNDRED_MILLIS:
			valueChanged = timeData.subHudredMillis();
			break;
		}

		if (valueChanged) {
			//fireStateChanged();
			tsEditor.updateValue();
		}
		return timeData;
	}

	@Override
	public Object getValue() {
		return timeData;
	}

	@Override
	public void setValue(Object o) {
		if(o instanceof TimeData) {
			timeData = (TimeData)o;
			tsEditor.setTimeData(timeData);
			fireStateChanged();
		}
	}
}
