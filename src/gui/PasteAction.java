package gui;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * 
 * Action class to provide an action to Paste into a component.
 * 
 * @author steve.webb
 */
class PasteAction extends AbstractAction {
	// static final private ImageIcon icon =
	// new
	// ImageIcon(ClassLoader.getSystemResource("toolbarButtonGraphics/general/Paste16.gif"));

	/**
	 * The component the action is associated with.
	 */
	JTextComponent comp;

	/**
	 * Default constructor.
	 * 
	 * @param comp
	 *            The component the action is associated with.
	 */
	public PasteAction(JTextComponent comp, String txt) {
		super(txt /* ,icon */);
		this.comp = comp;
	}

	/**
	 * Action has been performed on the component.
	 * 
	 * @param e
	 *            ignored
	 */
	public void actionPerformed(ActionEvent e) {
		comp.paste();
	}

	/**
	 * Checks if the action can be performed.
	 * 
	 * @return True if the action is allowed
	 */
	public boolean isEnabled() {
		if (comp.isEditable() && comp.isEnabled()) {
			Transferable contents = Toolkit.getDefaultToolkit()
					.getSystemClipboard().getContents(this);
			return contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		} else
			return false;
	}
}
