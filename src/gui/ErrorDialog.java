package gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import utils.OptionValues;

import java.io.*;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ErrorDialog extends JDialog {

	private Exception exc;
	private ExtendedTextArea textArea;
	private JLabel errorLabel;
	private JButton okButton;
	private ResourceBundle resourceBundle;
	
	public ErrorDialog(Dialog owner, Exception exc, OptionValues options) {
		super(owner, true);
		this.exc = exc;
		
		
		if (options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("ErrorDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("ErrorDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}
		setTitle(getString("error") + exc.getClass().getName()); //$NON-NLS-1$
		setup(options);
		
		Dimension d = new Dimension(432, 264);
		setSize(d);
		setMinimumSize(d);
		Point p = owner.getLocation();
    	Dimension dim = owner.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);
	}

	public ErrorDialog(Frame owner, Exception exc, OptionValues options) {
		super(owner, true);
		this.exc = exc;
		
		if (options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("ErrorDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("ErrorDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}
		
		setTitle(getString("error") + exc.getClass().getName()); //$NON-NLS-1$
		setup(options);
		
		Dimension d = new Dimension(432, 264);
		setSize(d);
		setMinimumSize(d);
		Point p = owner.getLocation();
    	Dimension dim = owner.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);
	}
	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private void setup(OptionValues options) {
		Image icon = new ImageIcon(ClassLoader.getSystemResource("error.png")).getImage();//Toolkit.getDefaultToolkit().getImage("./nutka.png"); //$NON-NLS-1$
		setIconImage(icon);

		//this.exc = exc;
		textArea = new ExtendedTextArea();
		if (options.getLang().equals("pl")) {
			textArea.setCopyStr("Kopiuj");
			textArea.setCutStr("Wytnij");
			textArea.setPasteStr("Wklej");
			textArea.setSelectAllStr("Zaznacz wszystko");
			textArea.setDeleteStr("Usu�");
		}
		errorLabel = new JLabel(exc.getMessage());
		okButton = new JButton(getString("closeWindow")); //$NON-NLS-1$
		
		initComponents();

		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
			// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			System.err.println(getString("cantChangLaF")); //$NON-NLS-1$
		}
		SwingUtilities.updateComponentTreeUI(this);
		
		Toolkit.getDefaultToolkit().beep();
	}

	private void initComponents() {

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setLayout(new BorderLayout());
		errorLabel.setBorder(new CompoundBorder(errorLabel.getBorder(),
				new EmptyBorder(0, 0, 10, 0)));
		// textArea.setBorder(new EmptyBorder(20, 5, 5, 5));
		// okButton.setBorder(new CompoundBorder(okButton.getBorder(),
		// new EmptyBorder(20, 5, 5, 5)));
		JScrollPane scrollPane = new JScrollPane(textArea,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());

		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());

		buttonPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		buttonPanel.add(okButton, BorderLayout.CENTER);

		mainPanel.add(errorLabel, BorderLayout.NORTH);
		mainPanel.add(scrollPane, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);

		add(mainPanel, BorderLayout.CENTER);

		textArea.setEditable(false);
		textArea.setFont(new Font("Times", Font.PLAIN, 12)); //$NON-NLS-1$
		textArea.setText(getErrorMessage());

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println(getSize());
				dispose();

			}

		});
	}

	private String getErrorMessage() {

		StringWriter writer = new StringWriter();
		PrintWriter out = new PrintWriter(writer);
		exc.printStackTrace(out);
		// exc.printStackTrace();
		out.close();
		return writer.getBuffer().toString();
	}
//
//	public static void main(String[] args) {
//		java.awt.EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					throw new Exception("adfafaere"); //$NON-NLS-1$
//				} catch (Exception e) {
//					JFrame frame = new JFrame();
//					frame.setLocation(500,500);
//					new ErrorDialog(frame, e, null).setVisible(true);
//					System.exit(0);
//				}
//			}
//		});

//	}

}
