package gui;
import javax.swing.JSpinner;
import utils.*;

public class TimeSpinner extends JSpinner {
	private TimeData timeData;
	private TimeSpinnerEditor tsEditor;
	private TimeSpinnerModel tsModel;
	
	public TimeSpinner(TimeData td) {
		this.timeData = td;
		this.tsEditor = new TimeSpinnerEditor(td, this);
		this.tsModel = new TimeSpinnerModel(timeData, tsEditor);
		setEditor(tsEditor);
		setModel(tsModel);
	}

	public TimeData getTimeData() {
		return timeData;
	}

	public void setTimeData(TimeData timeData) {
		this.timeData = timeData;
		//tsEditor.setTimeData(timeData);
		tsModel.setValue(timeData);
	}
	
	public TimeSpinnerEditor getTimeSpinnerEditor() {
		return tsEditor;
	}
	
	public TimeSpinnerModel getTimeSpinnerModel() {
		return tsModel;
	}
}
