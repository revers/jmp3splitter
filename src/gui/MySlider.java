package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

import de.muntjak.tinylookandfeel.TinySliderUI;

public class MySlider extends JSlider implements MouseListener,
		MouseMotionListener {

	private Popup popup;
	private JToolTip toolTip;
	private String toolTipText = "0";

	public MySlider() {
		super();
		init();
	}

	public MySlider(BoundedRangeModel brm) {
		super(brm);
		init();
	}

	public MySlider(int orientation) {
		super(orientation);
		init();
	}

	public MySlider(int min, int max) {
		super(min, max);
		init();
	}

	public MySlider(int min, int max, int value) {
		super(min, max, value);
		init();
	}

	public MySlider(int orientation, int min, int max, int value) {
		super(orientation, min, max, value);
		init();
	}

	private void init() {
		addMouseListener(this);
		addMouseMotionListener(this);
		toolTip = createToolTip();
		setUI(new TinySliderUI() {
		    protected void scrollDueToClickInTrack(int direction) {
		        // this is the default behaviour, let's comment that out
		        //scrollByBlock(direction);

		        int value = slider.getValue(); 

		        if (slider.getOrientation() == JSlider.HORIZONTAL) {
		            value = this.valueForXPosition(slider.getMousePosition().x);
		        } else if (slider.getOrientation() == JSlider.VERTICAL) {
		            value = this.valueForYPosition(slider.getMousePosition().y);
		        }
		        slider.setValue(value);
		    }
		});
		
		//setToolTipText("kiss my ass");
	}

	public void setToolTipText(String text) {
		this.toolTipText = text;
		toolTip.setTipText(toolTipText);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (!isEnabled())
			return;

		showToolTip(e);

		int value = getValue();

		if (getOrientation() == JSlider.HORIZONTAL) {
			value = ((TinySliderUI) getUI()).valueForXPosition((e
					.getLocationOnScreen().x - getLocationOnScreen().x));
		} else if (getOrientation() == JSlider.VERTICAL) {
			value = ((TinySliderUI) getUI())
					.valueForYPosition((e
							.getLocationOnScreen().y - getLocationOnScreen().y));
		}
		setValue(value);

	}

	public String getToolTipText() {
		return toolTipText;
	}

	protected void showToolTip(MouseEvent event) {
		// Determine tooltip text and location

		Point toolTipLocation = getToolTipLocation(event);

		// Trick is to hide a previous popup before showing a new one

		if (popup != null)
			popup.hide();

		PopupFactory factory = PopupFactory.getSharedInstance();
		popup = factory.getPopup(this, toolTip, toolTipLocation.x,
				toolTipLocation.y);
		popup.show();
	}

	public Point getToolTipLocation(MouseEvent event) {
		Point p = event.getLocationOnScreen();
		Rectangle sliderBounds = getBounds();
		Point sliderLoc = getLocationOnScreen();
		int y = sliderLoc.y + (sliderBounds.height / 2) + 12;
		int x = p.x;

		Dimension toolTipDim = toolTip.getSize();
		int maxXLocation = sliderLoc.x + sliderBounds.width - toolTipDim.width;
		int minXLocation = sliderLoc.x;

		if (x > maxXLocation)
			x = maxXLocation;
		else if (x < minXLocation)
			x = minXLocation;

		return new Point(x, y);
	}

	//
	// Implement MouseListener Interface
	//
	@Override
	public void mousePressed(MouseEvent e) {
		if (isEnabled())
			showToolTip(e);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (isEnabled())
			popup.hide();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
