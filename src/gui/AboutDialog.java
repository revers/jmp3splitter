package gui;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import utils.OptionValues;

import java.awt.event.*;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static java.awt.GridBagConstraints.*;

public class AboutDialog extends JDialog {
	private String version;
	private JButton okButton;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	//private JMP3SplitterFrame parent;
	private ResourceBundle resourceBundle;
	
	public AboutDialog(JMP3SplitterFrame parent, String version) {
		super(parent, true);
//		this.parent = parent;
		this.version = version;
		setSize(190, 217);
		setResizable(false);
		
		if (parent.options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("AboutDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("AboutDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}

		Point p = parent.getLocation();
		Dimension dim = parent.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);

		initComponents();

		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");//UIManager.getSystemLookAndFeelClassName());// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, parent.options).setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);
	}
	
	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	private void initComponents() {
		setTitle(getString("About")); //$NON-NLS-1$
		jLabel1 = new JLabel("JMP3Splitter"); //$NON-NLS-1$
		jLabel2 = new JLabel("ver. " + version); //$NON-NLS-1$
		jLabel3 = new JLabel(getString("sugestions")); //$NON-NLS-1$
		jLabel4 = new JLabel(getString("sendTo")); //$NON-NLS-1$
		jLabel5 = new JLabel("revers666@o2.pl"); //$NON-NLS-1$
		jLabel6 = new JLabel("(c) 2009 Kamil Kołaczyński"); //$NON-NLS-1$
		okButton = new JButton("OK"); //$NON-NLS-1$

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); //$NON-NLS-1$
		jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); //$NON-NLS-1$

		setLayout(new BorderLayout());

		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(0, 0, 5, 0);
		c.fill = NONE;
		c.anchor = CENTER;
		//c.weightx = 1.0;
		
		mainPanel.add(jLabel1, c);
		
		c.gridy = 1;
		c.insets = new Insets(0, 0, 15, 0);
		mainPanel.add(jLabel2, c);
	
		c.insets = new Insets(0, 0, 5, 0);
		c.gridy = 2;
		mainPanel.add(jLabel3, c);
		
		c.gridy = 3;
		mainPanel.add(jLabel4, c);
		
		c.gridy = 5;
		c.insets = new Insets(0, 0, 10, 0);
		mainPanel.add(jLabel5, c);
		
		c.gridy = 6;
		c.anchor = WEST;
		//c.insets = new Insets(0, 0, 5, 0);
		mainPanel.add(jLabel6, c);
		
		c.gridy = 7;
		c.anchor = EAST;
		c.insets = new Insets(0, 0, 0, 0);
		mainPanel.add(okButton, c);
		

		add(mainPanel, BorderLayout.CENTER);
		
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println(getSize());
				dispose();
			}
		});
	}

//	public static void main(String args[]) {
//		java.awt.EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				JFrame frame = new JFrame();
//				frame.setLocation(500, 500);
//				AboutDialog dialog = new AboutDialog(frame, "1.0 beta", null);
//				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
//					public void windowClosing(java.awt.event.WindowEvent e) {
//						System.exit(0);
//					}
//				});
//				dialog.setVisible(true);
//			}
//		});
//	}
}
