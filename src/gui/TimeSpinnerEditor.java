package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.border.*;
import javax.swing.text.Caret;
import javax.swing.*;

import utils.*;

public class TimeSpinnerEditor extends JTextField implements MouseListener,
		KeyListener {

	private TimeSpinner timeSpinner;
	private TimeData timeData;
	private int selectedField = TimeData.HUNDRED_MILLIS;
	private int selectionStart;
	private int selectionEnd;

	public TimeSpinnerEditor(TimeData td, TimeSpinner ts) {
		this.timeData = td;
		this.timeSpinner = ts;

		setColumns(td.toStringShorter().split("[:|.]").length * 2 - 1);
		setHorizontalAlignment(JTextField.RIGHT);

		String s = td.toStringShorter();
		setText(s);
		selectionStart = s.length() - 1;
		selectionEnd = s.length();
		setEditable(false);
		setBackground(Color.WHITE);
		setBorder(new EmptyBorder(0, 0, 0, 0));
		addMouseListener(this);
		// addFocusListener(this);
		addKeyListener(this);
	}

	public void updateValue() {
		setText(timeData.toStringShorter());
		setSelectionStart(selectionStart);
		setSelectionEnd(selectionEnd);
		getCaret().setSelectionVisible(true);
	}

	public TimeData getTimeData() {
		return timeData;
	}

	public void setTimeData(TimeData timeData) {
		this.timeData = timeData;
		updateValue();
	}

	public int getSelectedField() {
		return selectedField;
	}

	private void selectProperField() {
		int length = getText().length();
		int pos = getCaretPosition();

		if (pos >= length - 2) {
			selectedField = TimeData.HUNDRED_MILLIS;
			selectionStart = length - 1;
			selectionEnd = length;
		} else if (pos < length - 2 && pos >= length - 5) {
			selectedField = TimeData.SECOND;
			selectionStart = length - 4;
			selectionEnd = length - 2;
		} else if (length > 4) {
			if (pos < length - 5 && pos >= length - 8) {
				selectedField = TimeData.MINUTE;
				selectionStart = length - 7;
				selectionEnd = length - 5;
			} else {
				selectedField = TimeData.HOUR;
				selectionStart = length - 10;
				selectionEnd = length - 8;
			}
		}

		setSelectionStart(selectionStart);
		setSelectionEnd(selectionEnd);
		getCaret().setSelectionVisible(true);
	}

	public void mouseReleased(MouseEvent arg0) {
		selectProperField();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int caretPos = 0;

		if (e.getKeyCode() == 39) {
			caretPos = selectionStart + 3;
			if (caretPos >= getText().length() - 1)
				caretPos = getText().length() - 1;
		} else if (e.getKeyCode() == 37) {
			caretPos = selectionStart - 3;
			if (caretPos < 0)
				caretPos = 0;
		} else
			return;

		setCaretPosition(caretPos);
		selectProperField();
	}

	private boolean firstDigit = true;
	private int firstValue = 0;

	private void typeDigit(char digit) {
		String text = getText();
		if (selectedField == TimeData.HUNDRED_MILLIS) {
			text = text.substring(0, text.length() - 1) + digit;
		} else if (firstDigit) {
			text = text.substring(0, selectionStart) + "  " + digit
					+ text.substring(selectionEnd);
			// getCaret().setVisible(true);
			firstDigit = false;
			firstValue = (int) digit - 48;
			// selectionEnd--;
		} else {
			int val = ((int) digit - 48) * 10 + firstValue;
			if (val > 59)
				Toolkit.getDefaultToolkit().beep();
			else {

				text = text.substring(0, selectionStart) + digit
						+ text.substring(selectionEnd);
				// getCaret().setVisible(false);
				firstDigit = true;
				// selectionEnd++;
			}

		}
		int millis = TimeData.shorterToMillis(text.replaceAll("  ", "0"));
		if (millis > timeData.getMaxMillis()) {
			TimeData temp = new TimeData(millis);
			TimeData temp2 = new TimeData(timeData.getMaxMillis());
			if (temp.toStringShorter().equals(temp2.toStringShorter())) {
				// System.out.pr
				millis = timeData.getMaxMillis();
				timeData.setCurrentMillis(millis);
				timeSpinner.getTimeSpinnerModel().setValue(timeData);
			} else {
				text = timeData.toStringShorter();
				Toolkit.getDefaultToolkit().beep();
				System.out.printf("millis = %d; maxMillis = %d\n", millis,
						timeData.getMaxMillis());
			}
		} else {
			timeData.setCurrentMillis(millis);
			timeSpinner.getTimeSpinnerModel().setValue(timeData);
		}
		setText(text);
		setSelectionStart(selectionStart);
		setSelectionEnd(selectionEnd);
		getCaret().setSelectionVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {

		char c = e.getKeyChar();
		if (c >= '0' && c <= '9') {
			typeDigit(c);
		} else {
			Toolkit.getDefaultToolkit().beep();
			firstDigit = false;
		}

	}

	public void showSelectedField() {
		// setCaretPosition(selectionEnd);
		System.out.printf("start = %d; end = %d; length = %d\n",
				selectionStart, selectionEnd, getText().length());
		setSelectionStart(selectionStart);
		setSelectionEnd(selectionEnd);

		// selectAll();
		Caret c = getCaret();

		// c.setVisible(true);
		c.setSelectionVisible(true);
	}
}