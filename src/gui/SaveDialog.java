package gui;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import java.awt.*;
import java.awt.event.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.prefs.Preferences;
import java.util.regex.*;

import static java.awt.GridBagConstraints.*;
import com.l2fprod.common.swing.JDirectoryChooser;
import utils.*;
import mp3.*;
import mp3.ProgressMonitor;

import org.blinkenlights.jid3.MP3File;
import org.blinkenlights.jid3.MediaFile;
import org.blinkenlights.jid3.v2.ID3V2_3_0Tag;
import org.cmc.music.myid3.*;
import org.cmc.music.metadata.*;

public class SaveDialog extends JDialog implements ComponentListener {
	private final static String SAVE_PREF = "save_dir"; //$NON-NLS-1$
	// private final static int TEXT_FIELD_HEIGHT = 22;

	private JCheckBox includeId3CB;
	private JCheckBox rememberCB;
	private JCheckBox autoNameCB;
	private JLabel dirJL;
	private JLabel fileJL;
	private JLabel extJL;
	private JLabel titleJL;
	private JLabel artistJL;
	private JLabel albumJL;
	private JLabel genreJL;
	private JLabel commentJL;
	private JLabel yearJL;
	private JButton browseJB;
	private JButton readElseJB;
	private JButton readOrigJB;
	private JButton openFileJB;
	private JButton okButton;
	private JButton cancelButton;
	private ExtendedFormattedTextField dirTF;
	private ExtendedFormattedTextField fileTF;
	private ExtendedFormattedTextField titleTF;
	private ExtendedFormattedTextField artistTF;
	private ExtendedFormattedTextField albumTF;
	private ExtendedFormattedTextField genreTF;
	private ExtendedFormattedTextField commentTF;
	private ExtendedFormattedTextField yearTF;

	private String defaultPath;
	private String fileName;

	private boolean id3Enabled;
	private int beginFrame;
	private int endFrame;
	private File inFile;
	private JMP3SplitterFrame owner;
	private TagData tagData;
	private Dimension baseDimension;
	private MP3Info mp3Info;
	private ResourceBundle resourceBundle;

	public SaveDialog(JMP3SplitterFrame parent, MP3Info mp3Info,
			int beginFrame, int endFrame, TagData td) {
		super(parent, true); //$NON-NLS-1$
		this.tagData = td;
		this.owner = parent;
		this.mp3Info = mp3Info;
		this.inFile = mp3Info.mp3File;
		this.endFrame = endFrame;
		this.beginFrame = beginFrame;
		baseDimension = new Dimension(400, 380);

		try {
			UIManager
					.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");// UIManager.getSystemLookAndFeelClassName());//
			// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			new ErrorDialog(this, e, owner.options).setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);
		// System.out.printf("SaveDialog: begin = %d; end = %d; file = %s\n",
		// beginFrame, endFrame, inFile.toString());

		if (parent.options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("SaveDialog",
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("SaveDialog",
					OptionValues.LOCALE_PL);
		}
		setTitle(getString("saveAsDots"));

		Preferences prefs = Preferences.userNodeForPackage(SaveDialog.this
				.getClass());

		String defaultValue = File.listRoots()[0].getPath();
		defaultPath = prefs.get(SAVE_PREF, defaultValue);

		addComponentListener(this);
		initComponents();

		if (tagData == null) {
			fileName = makeFileName();
			OptionValues values = OptionValues.getDefaults();
			prefs = Preferences.userNodeForPackage(values.getClass());
			boolean autofill = parent.options.isAutofillTag();
			// Boolean.parseBoolean(prefs.get(
			// OptionValues.AUTOFILL_PREF, Boolean.toString(values
			// .isAutofillTag())));
			if (autofill) {
				includeId3CB.setSelected(autofill);
				fileTF.setEditable(false);
				id3Enabled = autofill;
				setID3Enabled(autofill);
				String[] tokens = fileName.split("] - T"); //$NON-NLS-1$
				artistTF.setText(tokens[0].substring(1));
				titleTF.setText("T" + tokens[1]); //$NON-NLS-1$
				Pattern ptr = Pattern.compile("\\d\\d\\d\\d"); //$NON-NLS-1$
				Matcher mchr = ptr.matcher(tokens[0]);
				if (mchr.find())
					yearTF.setText(mchr.group());
			}
		} else if (tagData.rememberTag) {
			restoreTag();
		}
	}

	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	private void restoreTag() {
		fileName = tagData.fileName;
		rememberCB.setSelected(true);
		fileTF.setEditable(true);
		fileTF.setText(fileName);
		autoNameCB.setSelected(false);
		if (!(tagData.artist.equals("") && tagData.album.equals("") //$NON-NLS-1$ //$NON-NLS-2$
				&& tagData.year.equals("") && tagData.title.equals("") && tagData.comment //$NON-NLS-1$ //$NON-NLS-2$
				.equals(""))) { //$NON-NLS-1$
			id3Enabled = true;
			setID3Enabled(id3Enabled);
			includeId3CB.setSelected(id3Enabled);
			yearTF.setText(tagData.year);
			artistTF.setText(tagData.artist);
			albumTF.setText(tagData.album);
			commentTF.setText(tagData.comment);
			titleTF.setText(tagData.title);
			if (tagData.autoFileName) {
				autoNameCB.setSelected(true);
				fileTF.setEditable(false);
			}
		}

	}

	private String makeFileName() {
		String fName = inFile.getName();
		fName = fName.substring(0, fName.length() - 4); // cut
		// extension
		Pattern p = Pattern.compile(" - Track No\\. \\d+$"); //$NON-NLS-1$
		Matcher m = p.matcher(fName);
		if (m.find())
			fName = incrementCounter(fName);
		else
			fName = "[Cut from " + FileNameFormatter.format(fName) //$NON-NLS-1$
					+ "] - Track No. 1"; //$NON-NLS-1$

		File tempFile = createFileObject(defaultPath, fName);
		while (tempFile.exists()) {
			fName = incrementCounter(fName);
			tempFile = createFileObject(defaultPath, fName);
		}
		return fName;
	}

	private String incrementCounter(String name) {
		Pattern p = Pattern.compile(" - Track No\\. \\d+$"); //$NON-NLS-1$
		Matcher m = p.matcher(name);
		if (m.find()) {
			String temp = name.substring(m.start() + 13);
			int number = Integer.parseInt(temp);
			number++;
			name = name.substring(0, m.start() + 13) + number;
			// System.out.println(temp);
		} else {
			name += " No. 1"; //$NON-NLS-1$
		}
		name = name.replaceAll("\\s{2,}", " "); //$NON-NLS-1$ //$NON-NLS-2$
		return name;
	}

	private File createFileObject(String dir, String name) {
		String s = dir;
		if (!s.endsWith("/") && !s.endsWith("\\")) //$NON-NLS-1$ //$NON-NLS-2$
			s += File.separator;
		return new File(s + name + ".mp3"); //$NON-NLS-1$
	}

	private void setProperSize() {
		int width = baseDimension.width;
		int height = baseDimension.height;

		Graphics2D g = (Graphics2D) fileTF.getGraphics();
		FontRenderContext frc = g.getFontRenderContext();
		TextLayout tl = new TextLayout(fileTF.getText(), fileTF.getFont(), frc);
		int textWidth = (int) Math.round(tl.getBounds().getWidth());
		if (textWidth > 210) {
			width += textWidth - 210;
		}
		setSize(width, height);
		setLocationOnCenter();
		// System.out.println("in setProperSize(): ");
		// printSizes();
	}

	private void printSizes() {
		System.out.println("browseJB:");
		System.out.println(openFileJB.getSize());
		System.out.println(openFileJB.getPreferredSize());
		System.out.println("---------------------");
		System.out.println("artistTF:");
		System.out.println(artistTF.getSize());
		System.out.println(artistTF.getPreferredSize());
	}

	private void setLocationOnCenter() {
		Point p = owner.getLocation();
		Dimension dim = owner.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);
	}

	private void setPolishMenuItems(ExtendedFormattedTextField textField) {
		textField.setCopyStr("Kopiuj");
		textField.setCutStr("Wytnij");
		textField.setPasteStr("Wklej");
		textField.setSelectAllStr("Zaznacz wszystko");
		textField.setDeleteStr("Usu�");
	}

	private void setProperLabelSizes() {
		Dimension dim = commentJL.getPreferredSize();
		dim.height = readElseJB.getPreferredSize().height;
		artistJL.setMinimumSize(dim);
		albumJL.setMinimumSize(dim);
		titleJL.setMinimumSize(dim);
		commentJL.setMinimumSize(dim);
		genreJL.setMinimumSize(dim);

		artistJL.setPreferredSize(dim);
		albumJL.setPreferredSize(dim);
		titleJL.setPreferredSize(dim);
		commentJL.setPreferredSize(dim);
		genreJL.setPreferredSize(dim);

		artistJL.setSize(dim);
		albumJL.setSize(dim);
		titleJL.setSize(dim);
		commentJL.setSize(dim);
		genreJL.setSize(dim);

		artistJL.setHorizontalAlignment(JTextField.RIGHT);
		albumJL.setHorizontalAlignment(JTextField.RIGHT);
		titleJL.setHorizontalAlignment(JTextField.RIGHT);
		commentJL.setHorizontalAlignment(JTextField.RIGHT);
		genreJL.setHorizontalAlignment(JTextField.RIGHT);
		readElseJB.setMaximumSize(new Dimension(Integer.MAX_VALUE, 18));
	}

	private void initComponents() {
		extJL = new JLabel(".mp3"); //$NON-NLS-1$
		okButton = new JButton("OK"); //$NON-NLS-1$
		includeId3CB = new JCheckBox();
		dirJL = new JLabel();
		fileJL = new JLabel();
		titleJL = new JLabel();
		artistJL = new JLabel();
		albumJL = new JLabel();
		genreJL = new JLabel();
		commentJL = new JLabel();
		yearJL = new JLabel();
		browseJB = new JButton();
		readElseJB = new JButton();
		readOrigJB = new JButton();
		cancelButton = new JButton();
		openFileJB = new JButton("...");
		dirTF = new ExtendedFormattedTextField();
		fileTF = new ExtendedFormattedTextField();
		titleTF = new ExtendedFormattedTextField();
		artistTF = new ExtendedFormattedTextField();
		albumTF = new ExtendedFormattedTextField();
		genreTF = new ExtendedFormattedTextField();
		commentTF = new ExtendedFormattedTextField();
		yearTF = new ExtendedFormattedTextField();

		if (owner.options.getLang().equals("pl")) {
			setPolishMenuItems(dirTF);
			setPolishMenuItems(fileTF);
			setPolishMenuItems(titleTF);
			setPolishMenuItems(artistTF);
			setPolishMenuItems(albumTF);
			setPolishMenuItems(genreTF);
			setPolishMenuItems(commentTF);
			setPolishMenuItems(yearTF);
		}

		rememberCB = new JCheckBox();
		autoNameCB = new JCheckBox();

		includeId3CB.setText(getString("include")); //$NON-NLS-1$
		dirJL.setText(getString("dir")); //$NON-NLS-1$
		fileJL.setText(getString("file")); //$NON-NLS-1$
		titleJL.setText(getString("title")); //$NON-NLS-1$
		artistJL.setText(getString("artist")); //$NON-NLS-1$
		albumJL.setText(getString("album")); //$NON-NLS-1$
		genreJL.setText(getString("genre")); //$NON-NLS-1$
		commentJL.setText(getString("comment")); //$NON-NLS-1$
		yearJL.setText(getString("year")); //$NON-NLS-1$
		browseJB.setText(getString("browse")); //$NON-NLS-1$
		readElseJB.setText(getString("readTag")); //$NON-NLS-1$
		readOrigJB.setText(getString("copyTag")); //$NON-NLS-1$
		cancelButton.setText(getString("cancel")); //$NON-NLS-1$
		rememberCB.setText(getString("remeber")); //$NON-NLS-1$
		autoNameCB.setText(getString("autoName")); //$NON-NLS-1$

		yearTF.setColumns(4);
		autoNameCB.setSelected(true);

		setSize(baseDimension);
		// setMinimumSize(baseDimension);

		// setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setLocationOnCenter();

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());

		setProperLabelSizes();

		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
		rootPanel.setBorder(new EmptyBorder(6, 10, 10, 10));

		JPanel filePanel = new JPanel();

		filePanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("saveAs")), new EmptyBorder(0, //$NON-NLS-1$
				5, 5, 5)));
		filePanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();

		c1.anchor = EAST;
		c1.weightx = 0.0;
		filePanel.add(dirJL, c1);

		c1.insets = new Insets(5, 0, 0, 0);
		c1.gridy = 1;
		filePanel.add(fileJL, c1);

		c1.gridx = 1;
		c1.weightx = 1.0;
		c1.fill = BOTH;
		filePanel.add(fileTF, c1);
		c1.insets = new Insets(0, 0, 0, 0);

		c1.gridy = 0;
		filePanel.add(dirTF, c1);

		c1.anchor = WEST;
		c1.gridx = 2;
		c1.weightx = 0.0;
		c1.gridwidth = 2;
		c1.insets = new Insets(0, 5, 0, 0);
		filePanel.add(browseJB, c1);

		c1.gridy = 1;
		c1.insets = new Insets(5, 5, 0, 0);
		c1.gridwidth = 1;
		filePanel.add(extJL, c1);
		c1.gridx = 3;
		filePanel.add(openFileJB, c1);

		rootPanel.add(filePanel);

		JPanel id3Panel = new JPanel();
		id3Panel.setBorder(new CompoundBorder(new TitledBorder(
				getString("id3Tag")), //$NON-NLS-1$
				new EmptyBorder(0, 5, 0, 5)));
		id3Panel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();

		// c2.gridwidth = 4;
		c2.anchor = WEST;
		c2.fill = HORIZONTAL;
		c2.insets = new Insets(0, 0, 5, 0);
		id3Panel.add(includeId3CB, c2);

		c2.gridwidth = 3;
		c2.gridx = 1;
		c2.anchor = EAST;
		c2.fill = NONE;
		id3Panel.add(autoNameCB, c2);
		c2.gridx = 0;

		// c2.fill = NONE;
		c2.gridwidth = 1;
		// c2.anchor = EAST;
		// c2.weightx = 0.0;
		c2.gridy = 1;
		id3Panel.add(titleJL, c2);

		c2.gridy = 2;
		id3Panel.add(artistJL, c2);

		c2.gridy = 3;
		id3Panel.add(albumJL, c2);

		c2.gridy = 4;
		id3Panel.add(genreJL, c2);

		c2.gridy = 5;
		id3Panel.add(commentJL, c2);

		c2.gridy = 6;
		id3Panel.add(yearJL, c2);

		c2.fill = BOTH;// HORIZONTAL;
		c2.gridx = 1;
		id3Panel.add(yearTF, c2);

		c2.gridwidth = 3;
		c2.weightx = 1.0;
		c2.gridy = 1;
		id3Panel.add(titleTF, c2);

		c2.gridy = 2;
		id3Panel.add(artistTF, c2);

		c2.gridy = 3;
		id3Panel.add(albumTF, c2);

		c2.gridy = 4;
		id3Panel.add(genreTF, c2);

		c2.gridy = 5;
		id3Panel.add(commentTF, c2);

		c2.insets = new Insets(0, 5, 5, 0);
		c2.gridy = 6;
		c2.gridwidth = 1;

		c2.gridx = 2;
		id3Panel.add(readOrigJB, c2);

		c2.gridx = 3;
		id3Panel.add(readElseJB, c2);

		rootPanel.add(id3Panel);

		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
		bottomPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		bottomPanel.add(rememberCB);

		bottomPanel.add(Box.createHorizontalGlue());

		okButton.setSize(cancelButton.getSize());
		okButton.setPreferredSize(cancelButton.getPreferredSize());

		bottomPanel.add(okButton);
		bottomPanel.add(Box.createHorizontalStrut(5));
		bottomPanel.add(cancelButton);

		// rootPanel.add(Box.createVerticalGlue());
		rootPanel.add(bottomPanel);
		add(rootPanel, BorderLayout.CENTER);

		// dirTF.setEnabled(false);

		setID3Enabled(id3Enabled);
		includeId3CB.setSelected(id3Enabled);

		dirTF.setText(defaultPath);
		fileTF.setText(fileName);

		okButton.addActionListener(new OkButtonListener());

		browseJB.addActionListener(new OpenButtonListener());

		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// System.out.println(getSize());
				// tagData = null;
				SaveDialog.this.dispose();
			}
		});

		includeId3CB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				id3Enabled = !id3Enabled;
				setID3Enabled(id3Enabled);
				if (id3Enabled && autoNameCB.isSelected())
					fileTF.setEditable(false);
				else
					fileTF.setEditable(true);

			}

		});

		readElseJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				readElseJBAction();
			}
		});

		readOrigJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				readOrigJBAction();
			}
		});

		fileTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		titleTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		artistTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		albumTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		genreTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		commentTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		yearTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});
		autoNameCB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// System.out.println(autoNameCB.isSelected());
				fileTF.setEditable(!autoNameCB.isSelected());
				updateFileName();

			}

		});
		// titleTF.addKeyListener(new KeyAdapter() {
		// @Override
		// public void keyTyped(KeyEvent e) {
		// updateFileName();
		// }
		// });
		//
		// artistTF.addKeyListener(new KeyAdapter() {
		// @Override
		// public void keyTyped(KeyEvent e) {
		// updateFileName();
		// }
		// });
		artistTF.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent e) {
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				updateFileName();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				updateFileName();
			}
		});

		titleTF.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				updateFileName();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				updateFileName();
			}

		});

		openFileJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				openFileJBAction();
			}

		});
	}

	private void openFileJBAction() {
		UIManager.put("FileChooser.saveDialogTitleText", getString("saveDots")); //$NON-NLS-1$ //$NON-NLS-2$
		UIManager.put("FileChooser.cancelButtonText", getString("cancel")); //$NON-NLS-1$ //$NON-NLS-2$
		UIManager.put("FileChooser.saveButtonText", getString("save"));

		final JFileChooser fileChooser = new JFileChooser(new File(defaultPath));
		SwingUtilities.updateComponentTreeUI(fileChooser);
		fileChooser.removeChoosableFileFilter(fileChooser
				.getAcceptAllFileFilter());

		// fileChooser.setSelectedFile(getDefaultFixFile(file));
		fileChooser
				.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

					public boolean accept(File f) {
						return f.isDirectory()
								|| f.getName().toLowerCase().endsWith(".mp3"); //$NON-NLS-1$
					}

					public String getDescription() {
						return "*.mp3"; //$NON-NLS-1$
					}
				});

		int returnVal = fileChooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {

			File selectedFile = fileChooser.getSelectedFile();
			if (selectedFile != null) {
				String name = selectedFile.getName();

				name = name.substring(0, name.length() - 4);
				defaultPath = selectedFile.getParent();
				dirTF.setText(defaultPath);
				fileTF.setText(name);
				autoNameCB.setSelected(false);
				fileTF.setEditable(true);
			}
		}
	}

	private void updateFileName() {
		if (autoNameCB.isSelected()) {
			try {
				Document doc = artistTF.getDocument();
				String artist = doc.getText(0, doc.getLength());
				doc = titleTF.getDocument();
				fileTF.setText("[" + artist + "] - " //$NON-NLS-1$ //$NON-NLS-2$
						+ doc.getText(0, doc.getLength()));
			} catch (BadLocationException ex) {
				ex.printStackTrace();
			}

		}

	}

	private void setID3Enabled(boolean b) {

		id3Enabled = b;
		titleTF.setEnabled(b);
		artistTF.setEnabled(b);
		albumTF.setEnabled(b);
		genreTF.setEnabled(b);
		commentTF.setEnabled(b);
		yearTF.setEnabled(b);

		titleJL.setEnabled(b);
		artistJL.setEnabled(b);
		albumJL.setEnabled(b);
		genreJL.setEnabled(b);
		commentJL.setEnabled(b);
		yearJL.setEnabled(b);

		readElseJB.setEnabled(b);
		readOrigJB.setEnabled(b);

		autoNameCB.setEnabled(b);
	}

	//
	// private void setAllDisabled() {
	// dirJL.setEnabled(false);
	// dirTF.setEnabled(false);
	// fileJL.setEnabled(false);
	// fileTF.setEnabled(false);
	// openJB.setEnabled(false);
	// extJL.setEnabled(false);
	// cancelButton.setEnabled(false);
	// okButton.setEnabled(false);
	// includeId3CB.setEnabled(false);
	// rememberCB.setEnabled(false);
	//
	// setID3Enabled(false);
	//
	// }

	public TagData getTagData() {
		return tagData;
	}

	public void setTagData(TagData tagData) {
		this.tagData = tagData;
	}

	private class OpenButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JDirectoryChooser chooser = new JDirectoryChooser(defaultPath);
			int choice = chooser.showOpenDialog(SaveDialog.this);
			if (choice == 0) {
				try {
					defaultPath = chooser.getSelectedFile().getCanonicalPath();
					dirTF.setText(defaultPath);

				} catch (Exception e) {
					new ErrorDialog(SaveDialog.this, e, owner.options)
							.setVisible(true);
					e.printStackTrace();
				}
			}

		}

	}

	private void okAction() {
		new Thread() {
			public void run() {
				try {
					File dir = new File(dirTF.getText());
					if (!dir.isDirectory()) {
						JOptionPane
								.showMessageDialog(
										SaveDialog.this,
										getString("typedDir") + dir.getAbsolutePath() //$NON-NLS-1$
												+ getString("dontExcist"), getString("changeDir"), //$NON-NLS-1$ //$NON-NLS-2$
										JOptionPane.WARNING_MESSAGE);
						return;
					}
					defaultPath = dir.getCanonicalPath();
					Preferences prefs = Preferences
							.userNodeForPackage(SaveDialog.this.getClass());
					prefs.put(SAVE_PREF, defaultPath);

					// String s = dirTF.getText();
					// if (!s.endsWith("/") && !s.endsWith("\\"))
					// s += "/";
					// s += fileTF.getText() + ".mp3";
					File outputFile = createFileObject(dirTF.getText(), fileTF
							.getText());// new File(s);
					if (outputFile.exists()) {
						Object[] options = { getString("Yes"), getString("No") }; //$NON-NLS-1$ //$NON-NLS-2$
						int n = JOptionPane.showOptionDialog(
								SaveDialog.this,
								getString("overwrite") + "\n"//$NON-NLS-1$
										+ fileTF.getText() + ".mp3", //$NON-NLS-1$
								getString("fileExcists"), //$NON-NLS-1$
								JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE, null, options,
								options[0]);

						if (n == 1)
							return;

						if (!outputFile.delete()) {
							String msg = getString("cantOverwrite") //$NON-NLS-1$
									+ fileTF.getText() + ".mp3\n" //$NON-NLS-1$
									+ getString("typeOtherName"); //$NON-NLS-1$
							JOptionPane.showMessageDialog(SaveDialog.this, msg,
									getString("useOtherName"), //$NON-NLS-1$
									JOptionPane.ERROR_MESSAGE);
							return;
						}
					}

					TagData td = null;
					if (rememberCB.isSelected()) {

						td = new TagData(fileTF.getText());
						td.rememberTag = true;
						if (includeId3CB.isSelected()) {
							td.artist = artistTF.getText();
							td.album = albumTF.getText();
							td.title = titleTF.getText();
							td.year = yearTF.getText();
							td.comment = commentTF.getText();
							td.genre = genreTF.getText();
							if (autoNameCB.isSelected())
								td.autoFileName = true;
						}
					}
					setTagData(td);

					dispose();
					final ProgressDialog progress = new ProgressDialog(owner,
							outputFile.getName());
					new Thread() {
						public void run() {
							progress.setVisible(true);
						}
					}.start();

					// System.out
					// .println("SaveDialog: Saving file: " + outputFile);
					MP3Cutter cut = new MP3Cutter(mp3Info, outputFile,
							beginFrame, endFrame, new ProgressMonitor() {

								@Override
								public void progressMade(int totalPercent) {
									// int percent = (int) (99.0f * ((float)
									// totalPercent / 100.0f));
									saveProgressMade(progress, totalPercent);
									// System.out.println("Procent: " +
									// totalPercent + "%");

								}

							});
					cut.write();
					if (includeId3CB.isSelected()
							&& !(titleTF.getText().equals("") //$NON-NLS-1$
									&& artistTF.getText().equals("") //$NON-NLS-1$
									&& albumTF.getText().equals("") //$NON-NLS-1$
									&& commentTF.getText().equals("") //$NON-NLS-1$
									&& yearTF.getText().equals("") && genreTF //$NON-NLS-1$
									.getText().equals(""))) { //$NON-NLS-1$

						MediaFile oMediaFile = new MP3File(outputFile);

						ID3V2_3_0Tag id3 = new ID3V2_3_0Tag();
						id3.setAlbum(albumTF.getText()); // sets TALB frame
						id3.setArtist(artistTF.getText()); // sets TPE1 frame
						id3.setComment(commentTF.getText()); // sets COMM frame
						// with language
						// "eng" and no
						// description
						id3.setGenre(genreTF.getText()); // sets TCON frame
						id3.setTitle(titleTF.getText()); // sets TIT2 frame
						try {
							id3.setYear(Integer.parseInt(yearTF.getText())); // sets
							// TYER
							// frame
						} catch (NumberFormatException exc) {
						}

						// set this v2.3.0 tag in the media file object
						oMediaFile.setID3Tag(id3);

						// update the actual file to reflect the current state
						// of our object
						oMediaFile.sync();
					}
				} catch (Exception e) {
					new ErrorDialog(SaveDialog.this, e, owner.options)
							.setVisible(true);
					e.printStackTrace();
				}
			}
		}.start();
	}

	private void saveProgressMade(ProgressDialog dialog, int percent) {
		dialog.setProgressBarValue(percent);
	}

	private class OkButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			okAction();
		}

	}

	@SuppressWarnings("unchecked")
	private void readTag(File f) {
		String oldFileName = fileTF.getText();
		try {
			// System.out.println("FILE: " + f.getAbsolutePath());
			MusicMetadataSet src_set = new MyID3().read(f);

			if (src_set == null) {
				System.out.println(getString("file2") + f.getAbsolutePath() //$NON-NLS-1$
						+ getString("noTags")); //$NON-NLS-1$
			} else {

				String artist = src_set.merged.getArtist();
				String title = src_set.merged.getSongTitle();
				Number y = src_set.merged.getYear();
				String year = y == null ? "" : y.toString(); //$NON-NLS-1$
				if (artist == null)
					artist = ""; //$NON-NLS-1$
				if (title == null)
					title = ""; //$NON-NLS-1$
				String genre = src_set.merged.getGenreName();
				if (genre == null)
					genre = ""; //$NON-NLS-1$
				java.util.List<String> list = src_set.merged.getComments();
				String comments = ""; //$NON-NLS-1$
				for (String s : list)
					comments += s + "\n"; //$NON-NLS-1$

				String album = src_set.merged.getAlbum();
				if (album == null)
					album = ""; //$NON-NLS-1$

				// System.out.printf(
				// "artsit = %s; title = %s; year = %s; genre = %s\n",
				// artist, title, year, genre);
				titleTF.setText(title);
				artistTF.setText(artist);
				commentTF.setText(comments);
				genreTF.setText(genre);
				albumTF.setText(album);
				yearTF.setText(year);
				if (title.equals("") && artist.equals("")) {
					fileTF.setText(oldFileName);
					autoNameCB.setSelected(false);
					fileTF.setEditable(true);
				}

			}
			// --------------------------------------
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(),
					getString("error"), //$NON-NLS-1$
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

	}

	private void readOrigJBAction() {
		readTag(inFile);
	}

	private void readElseJBAction() {
		final JFileChooser fileChooser = new JFileChooser(new File(inFile
				.getParent()));
		fileChooser.removeChoosableFileFilter(fileChooser
				.getAcceptAllFileFilter());

		// UIManager.put("FileChooser.openDialogTitleText", "Otw�rz...");
		// UIManager.put("FileChooser.cancelButtonText", "Anuluj");
		// UIManager.put("FileChooser.openButtonText", "Otw�rz");

		fileChooser
				.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

					public boolean accept(File f) {
						return f.isDirectory()
								|| f.getName().toLowerCase().endsWith(".mp3"); //$NON-NLS-1$
					}

					public String getDescription() {
						return "*.mp3"; //$NON-NLS-1$
					}
				});
		// fileChooser.set
		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			// if (file != null) {
			readTag(file);
			// }
		}

	}

	// public static void main(String[] args) {
	// java.awt.EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// MP3Info info = null;
	// try {
	// info = MP3Info
	// .scan(new File(
	// "D:\\Pink Floyd - Comfortably Numb [DZ vs Rastatronics RMX] 4clubbers.pl[by Ov].mp3"));
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// if (info == null) {
	// System.exit(-1);
	// }
	//
	// JFrame frame = new JFrame();
	// frame.setLocation(500, 500);
	// SaveDialog sd = new SaveDialog(frame, info, 2675, 4862, null);
	// sd.setVisible(true);
	// System.exit(0);
	// }
	// });
	// }

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {

	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		browseJB.requestFocusInWindow();
		setProperSize();
		// System.out.println("afadfa");

	}
}
