package gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.Format;
import javax.swing.JFormattedTextField;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Extended JFormattedTextField which provides cut/copy/paste/delete/selectAll
 * actions via a popup menu. This provides similar operations to the windows
 * system.
 * 
 * @author steve.webb
 */
public class ExtendedFormattedTextField extends JFormattedTextField {
	
	private String copyStr = "Copy";
	private String pasteStr = "Paste";
	private String cutStr = "Cut";
	private String selectAllStr = "Select All";
	private String deleteStr = "Delete";
	
	
	/**
	 * Creates a ExtendedFormattedTextField with no AbstractFormatterFactory.
	 * Use setMask or setFormatterFactory to configure the
	 * ExtendedFormattedTextField to edit a particular type of value.
	 */
	public ExtendedFormattedTextField() {
		super();
		initialize();
	}

	/**
	 * Creates a ExtendedFormattedTextField. format is wrapped in an appropriate
	 * AbstractFormatter which is then wrapped in an AbstractFormatterFactory.
	 * 
	 * @param format
	 *            Format used to look up an AbstractFormatter
	 */
	public ExtendedFormattedTextField(Format format) {
		super(format);
		initialize();
	}

	/**
	 * Creates a ExtendedFormattedTextField with the specified
	 * AbstractFormatter. The AbstractFormatter is placed in an
	 * AbstractFormatterFactory.
	 * 
	 * @param formatter
	 *            AbstractFormatter to use for formatting.
	 */
	public ExtendedFormattedTextField(
			JFormattedTextField.AbstractFormatter formatter) {
		super(formatter);
		initialize();
	}

	/**
	 * Creates a ExtendedFormattedTextField with the specified
	 * AbstractFormatterFactory.
	 * 
	 * @param factory
	 *            AbstractFormatterFactory used for formatting.
	 */
	public ExtendedFormattedTextField(
			JFormattedTextField.AbstractFormatterFactory factory) {
		super(factory);
		initialize();
	}

	/**
	 * Creates a ExtendedFormattedTextField with the specified
	 * AbstractFormatterFactory and initial value.
	 * 
	 * @param factory
	 *            AbstractFormatterFactory used for formatting.
	 * @param currentValue
	 *            Initial value to use
	 */
	public ExtendedFormattedTextField(
			JFormattedTextField.AbstractFormatterFactory factory,
			Object currentValue) {
		super(factory, currentValue);
		initialize();
	}

	/**
	 * Creates a ExtendedFormattedTextField with the specified value. This will
	 * create an AbstractFormatterFactory based on the type of value.
	 * 
	 * @param value
	 *            Initial value for the ExtendedFormattedTextField
	 */
	public ExtendedFormattedTextField(Object value) {
		super(value);
		initialize();
	}

	/**
	 * Initialize's specific behaviour for this class.
	 */
	protected void initialize() {
		
		// Add focus listener so that we can place the cursor correctly
		this.addFocusListener(new P4uFormattedTextFocusListener());

		// Add the mouse listener
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				requestFocus();
				dealWithMousePress(evt);
			}
		});
	}

	/**
	 * The mouse has been pressed over this text field. Popup the cut/paste
	 * menu.
	 * 
	 * @param evt
	 *            mouse event
	 */
	protected void dealWithMousePress(MouseEvent evt) {
		// Only interested in the right button
		if (SwingUtilities.isRightMouseButton(evt)) {
			// if(MenuSelectionManager.defaultManager().getSelectedPath().length>0)
			// return;

			JPopupMenu menu = new JPopupMenu();
			menu.add(new CutAction(this, cutStr));
			menu.add(new PasteAction(this, pasteStr));
			menu.add(new CopyAction(this, copyStr));
			menu.add(new DeleteAction(this, deleteStr));
			menu.addSeparator();
			menu.add(new SelectAllAction(this, selectAllStr));

			// Display the menu
			Point pt = SwingUtilities.convertPoint(evt.getComponent(), evt
					.getPoint(), this);
			menu.show(this, pt.x, pt.y);
		}
	}
	
	public String getCopyStr() {
		return copyStr;
	}

	public void setCopyStr(String copyStr) {
		this.copyStr = copyStr;
	}

	public String getPasteStr() {
		return pasteStr;
	}

	public void setPasteStr(String pasteStr) {
		this.pasteStr = pasteStr;
	}

	public String getCutStr() {
		return cutStr;
	}

	public void setCutStr(String cutStr) {
		this.cutStr = cutStr;
	}

	public String getSelectAllStr() {
		return selectAllStr;
	}

	public void setSelectAllStr(String selectAllStr) {
		this.selectAllStr = selectAllStr;
	}
	
	public String getDeleteStr() {
		return deleteStr;
	}

	public void setDeleteStr(String deleteStr) {
		this.deleteStr = deleteStr;
	}

	protected static class P4uFormattedTextFocusListener extends FocusAdapter {
		public void focusGained(FocusEvent e) {
			/*
			 * After a formatted text field gains focus, it replaces its text
			 * with its current value, formatted appropriately of course. It
			 * does this after any focus listeners are notified. We want to make
			 * sure that the caret is placed in the correct position rather than
			 * the dumb default that is before the 1st character !
			 */
			final JTextField field = (JTextField) e.getSource();
			final int dot = field.getCaret().getDot();
			final int mark = field.getCaret().getMark();
			if (field.isEnabled() && field.isEditable()) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						// Only set the caret if the textfield hasn't got a
						// selection on it
						if (dot == mark) {
							field.getCaret().setDot(dot);
						}
					}
				});
			}
		}
	}

}
