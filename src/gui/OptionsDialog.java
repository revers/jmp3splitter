package gui;

import java.awt.*;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.prefs.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import java.awt.event.*;

import static java.awt.GridBagConstraints.*;
import utils.*;

/*
 * TODO: 
 * 1. czas przewijania dac w tooltip'ie
 * 2. Po kliknieciu myszka w pasku postepu utwor przeskakuje do tego miejsca gdzie
 *    sie kliknelo
 * 3. Zawsze dzialajace klawisze (nie tylko kiedy focus)
 * 4. Poprawic reakcje po zmianie czasu w oknie edit gdy czasy zblizaja sie do siebie
 * 5. Dac rozne wyswietlanie czasu odtwarzania w oknie edit
 */

public class OptionsDialog extends JDialog implements ComponentListener {

	private static final Dimension SIZE_ENGLISH = new Dimension(408, 322);
	private static final Dimension SIZE_POLISH = new Dimension(448, 322);

	private JComboBox secsCB;
	private JComboBox langCB;
	private JComboBox rollCB;
	private JLabel langJL;
	private JLabel secsJL;
	private JButton okButton;
	private JButton cancelButton;
	private JCheckBox autofillTagCB;
	private JCheckBox jmpToEndCB;
	private JLabel fiveSAheadJL;
	private JLabel fiveSBackJL;
	private JLabel oneMinAheadJL;
	private JLabel oneMinBackJL;
	private JLabel markBeginJL;
	private JLabel markEndJL;
	private JLabel playPauseJL;
	private JLabel stopJL;
	private JLabel rollJL;
	private JTextField fiveSAheadTF;
	private JTextField fiveSBackTF;
	private JTextField oneMinAheadTF;
	private JTextField oneMinBackTF;
	private JTextField markBeginTF;
	private JTextField markEndTF;
	private JTextField playPauseTF;
	private JTextField stopTF;

	private String shortcutText = ""; //$NON-NLS-1$
	private JMP3SplitterFrame parent;
	private ResourceBundle resourceBundle;
	private boolean okAction = false;
	private JPanel shortPanel;
	private JPanel miscPanel;
	private int lastLangIndex = 0;

	private OptionsDialog(JMP3SplitterFrame parent) {
		super(parent, true);
		this.parent = parent;

		if (parent.options.getLang().equals("en")) { //$NON-NLS-1$ 
			// langCB.setSelectedIndex(0);
			resourceBundle = ResourceBundle.getBundle("OptionsDialog",
					Locale.ENGLISH);
			setSize(SIZE_ENGLISH);
		} else {

			resourceBundle = ResourceBundle.getBundle("OptionsDialog",
					OptionValues.LOCALE_PL);
			// langCB.setSelectedIndex(1);
			setSize(SIZE_POLISH);
		}
		setResizable(false);
		
		SwingUtils.setLocationOnCenter(parent, this);

		initComponents();
		initValues();

		addComponentListener(this);

		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
			// "com.jgoodies.looks.plastic.Plastic3DLookAndFeel"
			// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, parent.options).setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);
	}

	public static boolean showOptionsDialog(JMP3SplitterFrame parent) {
		OptionsDialog od = new OptionsDialog(parent);
		od.setVisible(true);
		return od.okAction;
	}

	private void initValues() {
		if (parent.options.getLang().equals("en")) {//$NON-NLS-1$  
			langCB.setSelectedIndex(0);
			lastLangIndex = 0;
		} else {
			langCB.setSelectedIndex(1);
			lastLangIndex = 1;
		}

		jmpToEndCB.setSelected(parent.options.isJmpToEnd());
		autofillTagCB.setSelected(parent.options.isAutofillTag());
		secsCB.getModel().setSelectedItem(
				Integer.toString(parent.options.getSeconds()));

		if (parent.options.getRoll().equals("s")) //$NON-NLS-1$
			rollCB.setSelectedIndex(0);
		else
			rollCB.setSelectedIndex(1);

		ShortcutsEnum se = ShortcutsEnum.FIVE_S_AHEAD;
		fiveSAheadTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.FIVE_S_BACK;
		fiveSBackTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.ONE_MIN_AHEAD;
		oneMinAheadTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.ONE_MIN_BACK;
		oneMinBackTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.MARK_BEGIN;
		markBeginTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.MARK_END;
		markEndTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.PLAY_PAUSE;
		playPauseTF.setText(getKeysName(se.getKey(), se.getMod()));
		se = ShortcutsEnum.STOP;
		stopTF.setText(getKeysName(se.getKey(), se.getMod()));
	}

	private boolean isAnyShortcutDuplicated() {
		for (ShortcutsEnum se1 : ShortcutsEnum.values()) {
			for (ShortcutsEnum se2 : ShortcutsEnum.values()) {
				if (se1 == se2)
					continue;
				if (se1.getKey() == se2.getKey()
						&& se1.getMod() == se2.getMod())
					return true;
			}
		}
		return false;
	}

	private void saveValues() {
		OptionValues values = OptionValues.getDefaults();
		Preferences prefs = Preferences.userNodeForPackage(values.getClass());
		prefs.put(OptionValues.JMP_PREF, Boolean.toString(jmpToEndCB
				.isSelected()));
		prefs.put(OptionValues.AUTOFILL_PREF, Boolean.toString(autofillTagCB
				.isSelected()));
		prefs.put(OptionValues.SECONDS_PREF, secsCB.getModel()
				.getSelectedItem().toString());
		parent.options.setSeconds(secsCB.getSelectedIndex() + 1);
		parent.options.setJmpToEnd(jmpToEndCB.isSelected());
		parent.options.setAutofillTag(autofillTagCB.isSelected());

		String val = ShortcutsEnum.FIVE_S_AHEAD.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.FIVE_S_AHEAD.getMod();
		prefs.put(OptionValues.FIVE_S_AHEAD_PREF, val);

		val = ShortcutsEnum.FIVE_S_BACK.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.FIVE_S_BACK.getMod();
		prefs.put(OptionValues.FIVE_S_BACK_PREF, val);

		val = ShortcutsEnum.ONE_MIN_AHEAD.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.ONE_MIN_AHEAD.getMod();
		prefs.put(OptionValues.ONE_MIN_AHEAD_PREF, val);

		val = ShortcutsEnum.ONE_MIN_BACK.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.ONE_MIN_BACK.getMod();
		prefs.put(OptionValues.ONE_MIN_BACK_PREF, val);

		val = ShortcutsEnum.MARK_BEGIN.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.MARK_BEGIN.getMod();
		prefs.put(OptionValues.MARK_BEGIN_PREF, val);

		val = ShortcutsEnum.MARK_END.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.MARK_END.getMod();
		prefs.put(OptionValues.MARK_END_PREF, val);

		val = ShortcutsEnum.PLAY_PAUSE.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.PLAY_PAUSE.getMod();
		prefs.put(OptionValues.PLAY_PAUSE_PREF, val);

		val = ShortcutsEnum.STOP.getKey() + ";" //$NON-NLS-1$
				+ ShortcutsEnum.STOP.getMod();
		prefs.put(OptionValues.STOP_PREF, val);

		if (rollCB.getSelectedIndex() == 0) {
			prefs.put(OptionValues.ROLL_PREF, "s"); //$NON-NLS-1$
			parent.options.setRoll("s");
		} else {
			prefs.put(OptionValues.ROLL_PREF, "m"); //$NON-NLS-1$
			parent.options.setRoll("m");
		}
		if (langCB.getSelectedIndex() == 0) {
			prefs.put(OptionValues.LANG_PREF, "en"); //$NON-NLS-1$
			parent.options.setLang("en");
		} else {
			prefs.put(OptionValues.LANG_PREF, "pl"); //$NON-NLS-1$
			parent.options.setLang("pl");
		}
	}

	private void dealTFs() {
		fiveSAheadTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.FIVE_S_AHEAD.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						fiveSAheadTF.setText(shortcutText);
					}
				}.start();
			}
		});
		fiveSBackTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.FIVE_S_BACK.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						fiveSBackTF.setText(shortcutText);
					}
				}.start();
			}
		});
		oneMinAheadTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.ONE_MIN_AHEAD.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						oneMinAheadTF.setText(shortcutText);
					}
				}.start();
			}
		});
		oneMinBackTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.ONE_MIN_BACK.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						oneMinBackTF.setText(shortcutText);
					}
				}.start();
			}
		});
		markBeginTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.MARK_BEGIN.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						markBeginTF.setText(shortcutText);
					}
				}.start();
			}
		});
		markEndTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.MARK_END.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						markEndTF.setText(shortcutText);
					}
				}.start();
			}
		});
		playPauseTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.PLAY_PAUSE.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						playPauseTF.setText(shortcutText);
					}
				}.start();
			}
		});
		stopTF.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int mod = e.getModifiers();

				shortcutText = getKeysName(key, mod);
				ShortcutsEnum.STOP.setKeys(key, mod);

				new Thread() {
					public void run() {
						Thread.yield();
						stopTF.setText(shortcutText);
					}
				}.start();
			}
		});

	}

	private String localizeShortcut(String s) {
		if (langCB.getSelectedIndex() == 0)
			return s.replace("G�ra", "Up").replace("D�", "Down").replace(
					"Prawo", "Right").replace("Lewo", "Left").replace("Spacja",
					"Space");
		else
			return s.replace("Up", "G�ra").replace("Down", "D�").replace(
					"Right", "Prawo").replace("Left", "Lewo").replace("Space",
					"Spacja");
	}

	private String getKeysName(int key, int mod) {
		String keyStr = KeyEvent.getKeyText(key);
		String modStr = KeyEvent.getKeyModifiersText(mod);
		
		return keyStr + "#" + modStr;

//		String[] keys = modStr.split("\\+"); //$NON-NLS-1$
//		for (String s : keys)
//			if (s.equals(keyStr)) {
//				keyStr = ""; //$NON-NLS-1$
//				break;
//			}
//		// String combine = "";
//		//		if (modStr.equals("")) //$NON-NLS-1$
//		// ;
//		if (!modStr.equals("") && keyStr.equals("")) //$NON-NLS-1$
//			keyStr = modStr;
//		else if (!modStr.equals(""))
//			keyStr = modStr + "+" + keyStr; //$NON-NLS-1$
//
//		return localizeShortcut(keyStr);
		
	}

	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	private void setLocalizedStrings() {
		setTitle(getString("titleOptions")); //$NON-NLS-1$
		langJL.setText(getString("langJL")); //$NON-NLS-1$
		fiveSAheadJL.setText(getString("5sAheadJL")); //$NON-NLS-1$
		fiveSBackJL.setText(getString("5sBackJL")); //$NON-NLS-1$
		oneMinAheadJL.setText(getString("1minAheadJL")); //$NON-NLS-1$
		oneMinBackJL.setText(getString("1minBackJL")); //$NON-NLS-1$
		markBeginJL.setText(getString("markBeginJL")); //$NON-NLS-1$
		markEndJL.setText(getString("markEndJL")); //$NON-NLS-1$
		playPauseJL.setText(getString("playPauseJL")); //$NON-NLS-1$
		stopJL.setText(getString("stopJL")); //$NON-NLS-1$
		rollJL.setText(getString("rollJL")); //$NON-NLS-1$
		jmpToEndCB.setText(getString("jmpJL")); //$NON-NLS-1$
		secsJL.setText(getString("secsJL")); //$NON-NLS-1$
		autofillTagCB.setText(getString("tagJL")); //$NON-NLS-1$
		cancelButton.setText(getString("cancelJB")); //$NON-NLS-1$

		langCB.setModel(new DefaultComboBoxModel(new String[] {
				getString("langEN"), getString("langPL") })); //$NON-NLS-1$ //$NON-NLS-2$
		langCB.setSelectedIndex(lastLangIndex);
		rollCB.setModel(new DefaultComboBoxModel(new String[] {
				getString("5sAheadBack"), getString("oneMinAheadBack") })); //$NON-NLS-1$ //$NON-NLS-2$

		miscPanel.setBorder(new TitledBorder(getString("titleMisc"))); //$NON-NLS-1$
		shortPanel.setBorder(new TitledBorder(getString("titleShortc"))); //$NON-NLS-1$

	}

	private void initComponents() {
		secsCB = new JComboBox();
		langCB = new JComboBox();
		rollCB = new JComboBox();

		jmpToEndCB = new JCheckBox();
		secsJL = new JLabel();
		autofillTagCB = new JCheckBox();
		okButton = new JButton();
		cancelButton = new JButton();
		langJL = new JLabel();
		fiveSAheadJL = new JLabel();
		fiveSBackJL = new JLabel();
		oneMinAheadJL = new JLabel();
		oneMinBackJL = new JLabel();
		markBeginJL = new JLabel();
		markEndJL = new JLabel();
		playPauseJL = new JLabel();
		stopJL = new JLabel();
		rollJL = new JLabel();

		fiveSAheadTF = new JTextField();
		fiveSBackTF = new JTextField();
		oneMinAheadTF = new JTextField();
		oneMinBackTF = new JTextField();
		markBeginTF = new JTextField();
		markEndTF = new JTextField();
		playPauseTF = new JTextField();
		stopTF = new JTextField();
		miscPanel = new JPanel();
		shortPanel = new JPanel();

		Dimension dim = new Dimension(110, 20);
		langCB.setMinimumSize(dim);
		langCB.setPreferredSize(dim);

		rollCB.setMinimumSize(dim);
		rollCB.setPreferredSize(dim);

		fiveSAheadTF.setMinimumSize(dim);
		// fiveSAheadTF.setMaximumSize(dim);
		fiveSAheadTF.setPreferredSize(dim);
		fiveSBackTF.setMinimumSize(dim);
		// fiveSBackTF.setMaximumSize(dim);
		fiveSBackTF.setPreferredSize(dim);
		oneMinAheadTF.setMinimumSize(dim);
		// oneMinAheadTF.setMaximumSize(dim);
		oneMinAheadTF.setPreferredSize(dim);
		oneMinBackTF.setMinimumSize(dim);
		// oneMinBackTF.setMaximumSize(dim);
		oneMinBackTF.setPreferredSize(dim);
		markBeginTF.setMinimumSize(dim);
		// markBeginTF.setMaximumSize(dim);
		markBeginTF.setPreferredSize(dim);
		markEndTF.setMinimumSize(dim);
		// markEndTF.setMaximumSize(dim);
		markEndTF.setPreferredSize(dim);
		playPauseTF.setMinimumSize(dim);
		// playPauseTF.setMaximumSize(dim);
		playPauseTF.setPreferredSize(dim);
		stopTF.setMinimumSize(dim);
		// stopTF.setMaximumSize(dim);
		stopTF.setPreferredSize(dim);

		dim = new Dimension(37, 18);
		secsCB.setMinimumSize(dim);

		setLocalizedStrings();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		// setResizable(false);

		// langCB.setSelectedIndex(1);

		secsCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
				"1", "2", "3", "4", "5", "6", "7", "8", "9", "10" })); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$

		okButton.setText("OK"); //$NON-NLS-1$

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setBorder(new EmptyBorder(new Insets(6, 10, 10, 10)));

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = 2;
		gbc.fill = HORIZONTAL;

		setLayout(new BorderLayout());

		miscPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		JPanel secsPanel = new JPanel();
		secsPanel.setLayout(new BoxLayout(secsPanel, BoxLayout.X_AXIS));
		secsPanel.add(secsCB);
		secsPanel.add(Box.createHorizontalStrut(7));
		secsPanel.add(secsJL);
		// c.insets = new Insets(0, 0, 5, 0);

		c.anchor = WEST;
		c.insets = new Insets(0, 5, 5, 0);
		c.weightx = 1.0;
		c.gridwidth = 3;
		c.fill = NONE;
		miscPanel.add(secsPanel, c);

		c.fill = HORIZONTAL;
		c.gridy = 1;
		miscPanel.add(jmpToEndCB, c);

		c.gridy = 2;
		c.gridwidth = 1;
		miscPanel.add(autofillTagCB, c);

		c.weightx = 0.0;
		c.insets = new Insets(0, 5, 0, 0);
		c.anchor = EAST;
		c.fill = NONE;
		c.gridx = 1;
		miscPanel.add(langJL, c);

		c.gridx = 2;
		c.insets = new Insets(0, 5, 0, 5);
		miscPanel.add(langCB, c);

		dealTFs();
		// gbc.fill = HORIZONTAL;

		mainPanel.add(miscPanel, gbc);

		// ----------------------------------------

		shortPanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = NONE;
		c1.anchor = WEST;
		c1.weightx = 0.0;
		c1.insets = new Insets(0, 5, 0, 5);
		shortPanel.add(fiveSAheadJL, c1);
		c1.insets = new Insets(5, 5, 0, 5);
		c1.gridy = 1;
		shortPanel.add(fiveSBackJL, c1);
		c1.gridy = 2;
		shortPanel.add(oneMinAheadJL, c1);
		c1.gridy = 3;
		shortPanel.add(oneMinBackJL, c1);
		// /c1.gridy = 4;
		// shortPanel.add(rollJL, c1);

		c1.gridx = 1;
		c1.weightx = 0.5;
		c1.anchor = EAST;
		// c1.fill = HORIZONTAL;
		c1.insets = new Insets(5, 0, 0, 10);
		// shortPanel.add(rollCB, c1);
		c1.gridy = 3;
		shortPanel.add(oneMinBackTF, c1);
		c1.gridy = 2;
		shortPanel.add(oneMinAheadTF, c1);
		c1.gridy = 1;
		shortPanel.add(fiveSBackTF, c1);
		c1.gridy = 0;
		c1.insets = new Insets(0, 0, 0, 10);
		shortPanel.add(fiveSAheadTF, c1);

		c1.gridx = 2;
		c1.weightx = 0.0;
		c1.fill = NONE;
		c1.anchor = WEST;
		c1.insets = new Insets(0, 5, 0, 5);
		shortPanel.add(markBeginJL, c1);
		c1.insets = new Insets(5, 5, 0, 5);
		c1.gridy = 1;
		shortPanel.add(markEndJL, c1);
		c1.gridy = 2;
		shortPanel.add(playPauseJL, c1);
		c1.gridy = 3;
		shortPanel.add(stopJL, c1);

		c1.gridx = 3;
		c1.weightx = 0.5;
		// c1.fill = HORIZONTAL;
		c1.anchor = EAST;
		c1.insets = new Insets(5, 0, 0, 5);
		shortPanel.add(stopTF, c1);
		c1.gridy = 2;
		shortPanel.add(playPauseTF, c1);

		c1.gridy = 1;
		shortPanel.add(markEndTF, c1);

		c1.insets = new Insets(0, 0, 0, 5);
		c1.gridy = 0;
		shortPanel.add(markBeginTF, c1);

		c1.insets = new Insets(5, 5, 5, 0);
		c1.fill = NONE;
		c1.anchor = WEST;
		c1.weightx = 0.0;
		c1.gridx = 0;
		c1.gridy = 4;
		shortPanel.add(rollJL, c1);

		c1.anchor = EAST;
		// c1.fill = NONE;
		c1.insets = new Insets(5, 0, 5, 10);
		// c1.gridwidth = 3;
		c1.weightx = 1.0;
		c1.gridx = 1;
		shortPanel.add(rollCB, c1);

		// ----------------------------------------
		gbc.gridy = 1;
		mainPanel.add(shortPanel, gbc);

		gbc.insets = new Insets(8, 0, 0, 5);
		gbc.gridwidth = 1;
		gbc.weightx = 0.5;
		gbc.gridy = 2;
		mainPanel.add(okButton, gbc);
		//		
		gbc.insets = new Insets(8, 0, 0, 0);
		gbc.gridx = 1;
		gbc.weightx = 0.5;

		mainPanel.add(cancelButton, gbc);

		// Dimension dim = secsCB.getMinimumSize();
		// secsCB.setPreferredSize(dim);

		add(mainPanel, BorderLayout.CENTER);

		okButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okButtonActionPerformed(evt);
			}
		});

		cancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				System.out.println(OptionsDialog.this.getSize());
				// cancelButtonActionPerformed(evt);
				OptionsDialog.this.dispose();
			}
		});
		langCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (langCB.getSelectedIndex() != lastLangIndex) {
					lastLangIndex = langCB.getSelectedIndex();
					if (lastLangIndex == 0) { //$NON-NLS-1$ 
						// langCB.setSelectedIndex(0);
						resourceBundle = ResourceBundle.getBundle(
								"OptionsDialog", Locale.ENGLISH);
						setSize(SIZE_ENGLISH);
						SwingUtils.setLocationOnCenter(parent,
								OptionsDialog.this);
					} else {
						resourceBundle = ResourceBundle.getBundle(
								"OptionsDialog", OptionValues.LOCALE_PL);
						// langCB.setSelectedIndex(1);
						setSize(SIZE_POLISH);
						SwingUtils.setLocationOnCenter(parent,
								OptionsDialog.this);
					}
					setLocalizedStrings();

					fiveSAheadTF.setText(localizeShortcut(fiveSAheadTF
							.getText()));
					fiveSBackTF
							.setText(localizeShortcut(fiveSBackTF.getText()));
					oneMinAheadTF.setText(localizeShortcut(oneMinAheadTF
							.getText()));
					oneMinBackTF.setText(localizeShortcut(oneMinBackTF
							.getText()));
					markBeginTF
							.setText(localizeShortcut(markBeginTF.getText()));
					markEndTF.setText(localizeShortcut(markEndTF.getText()));
					playPauseTF
							.setText(localizeShortcut(playPauseTF.getText()));
					stopTF.setText(localizeShortcut(stopTF.getText()));
				}

			}

		});
		// pack();
	}

	private void okButtonActionPerformed(ActionEvent evt) {
		if (isAnyShortcutDuplicated()) {
			JOptionPane.showMessageDialog(this, getString("warnUniq"), //$NON-NLS-1$
					getString("warnDuplicated"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
			return;
		}
		saveValues();
		dispose();
		okAction = true;
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		// System.out.println("RESIZED");
		// System.out.printf("prefered = %s\n minimum = %s\n size = %s\n"
		// , fiveSAheadTF.getPreferredSize().toString(), fiveSAheadTF
		// .getMinimumSize().toString(), fiveSAheadTF.getSize().toString());
		// rollCB.setPreferredSize(fiveSAheadTF.getSize());
		// //rollCB.setMinimumSize(fiveSAheadTF.getSize());
		// //System.out.println(rollCB.getSize());
		// rollCB.setSize(fiveSAheadTF.getSize());
		// System.out.println(rollCB.getSize());
		// rollCB.repaint();
		// rollCB.setSize(fiveSAheadTF.getSize());
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		cancelButton.requestFocusInWindow();
		// rollCB.setSize(fiveSAheadTF.getSize());

	}

	// public static void main(String[] args) {
	// SwingUtilities.invokeLater(new Runnable() {
	// @Override
	// public void run() {
	// JFrame f = new JFrame();
	// f.setLocation(400, 400);
	// new OptionsDialog(f).setVisible(true);
	//
	// }
	// });
	// }
}
