package gui;
import static java.awt.GridBagConstraints.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.*;
import utils.*;

public class TimeDialog extends JDialog {
	private JMP3SplitterFrame jmp3SplitterFrame;
	private JLabel spinnerLabel;
	private TimeSpinner timeSpinner;
	private JButton okButton;
	private JButton cancelButton;
	private TimeData timeData;
	private WhichTime whichTime;
	private ResourceBundle resourceBundle;
	private boolean ok = false;

	public static enum WhichTime {
		BEGIN, END
	}
	
	private TimeDialog(JMP3SplitterFrame owner, WhichTime whichTime,
			String labelText) {
		super(owner, true);
		this.whichTime = whichTime;
		jmp3SplitterFrame = owner;
		
		if (owner.options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("TimeDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("TimeDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}

		spinnerLabel = new JLabel(labelText);
		initTime();

		// setSize(300, 300);

		initComponents();

		Point p = owner.getLocation();
		Dimension dim = owner.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);
		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");//UIManager.getSystemLookAndFeelClassName());// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, jmp3SplitterFrame.options).setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);
	}

	public static boolean showTimeDialog(JMP3SplitterFrame owner, WhichTime whichTime,
			String labelText) {
		TimeDialog td = new TimeDialog(owner, whichTime, labelText);
		td.setVisible(true);
		return td.ok;
	}
	
	
	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private void initComponents() {
		okButton = new JButton("OK"); //$NON-NLS-1$
		cancelButton = new JButton(getString("cancel")); //$NON-NLS-1$
		timeSpinner = new TimeSpinner(timeData);
		setResizable(false);

		setTitle(getString("time")); //$NON-NLS-1$
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.fill = HORIZONTAL;
		c.anchor = WEST;
		c.weightx = 0.5;
		c.insets = new Insets(8, 8, 8, 0);
		add(spinnerLabel, c);

		c.gridx = 1;
		c.anchor = EAST;
		c.insets = new Insets(8, 5, 8, 8);
		add(timeSpinner, c);

		c.gridy = 1;
		c.anchor = CENTER;

		c.insets = new Insets(0, 5, 8, 8);

		add(cancelButton, c);

		c.gridx = 0;
		c.insets = new Insets(0, 8, 8, 0);
		add(okButton, c);

		// c1.anchor = EAST;
		// c1.weightx = 0.0;
		// c1.gridy = 1;
		// c1.gridx = 1;
		// c1.fill = HORIZONTAL;
		// c1.insets = new Insets(0, 5, 0, 0);
		Dimension d = new Dimension(73, 23);
		// okButton.setSize(d);
		okButton.setPreferredSize(d);
		// cancelButton.setSize(d);
		cancelButton.setPreferredSize(d);

		pack();

		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				okButtonAction();
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// System.out.println(getSize());
				dispose();
			}
		});
	}

	private void initTime() {
		if (whichTime == WhichTime.BEGIN
				&& jmp3SplitterFrame.getSelBeginTime() != null) {
			TimeData temp = jmp3SplitterFrame.getSelBeginTime();
			timeData = new TimeData(temp.getCurrentMillis(), temp.getMaxMillis());
		}
		
		else if (jmp3SplitterFrame.getSelEndTime() != null) {
			TimeData temp = jmp3SplitterFrame.getSelEndTime();
			//TimeData endTemp = new TimeData(temp.getMaxMillis(), temp.getMaxMillis());
			//int end = TimeData.shorterToMillis(endTemp.toStringShorter()); 
			timeData = new TimeData(temp.getCurrentMillis(), temp.getMaxMillis());		
		}

		if (timeData == null)
			timeData = new TimeData(0, jmp3SplitterFrame.getDurationTime()
					.getMaxMillis());
	}

	private void okButtonAction() {
		ok = true;
		TimeData temp = new TimeData(timeData.getMaxMillis(), timeData.getMaxMillis());
		if(timeData.toStringShorter().equals(temp.toStringShorter()))
			timeData = temp;
		
		if (whichTime == WhichTime.BEGIN)
			jmp3SplitterFrame.setSelBeginTime(timeData);
		else
			jmp3SplitterFrame.setSelEndTime(timeData);
		dispose();
	}
	//
	// public static void main(String[] args) {
	// // java.awt.EventQueue.invokeLater(new Runnable() {
	// // public void run() {
	// // JFrame frame = new JFrame();
	// // frame.setLocation(500, 500);
	// // TimeData begin = new TimeData(74449, 410044);
	// // TimeDialog td = new TimeDialog(frame, begin, "Pocz�tek: ");
	// // td.setVisible(true);
	// // System.exit(0);
	// // }
	// // });
	//
	// }

}
