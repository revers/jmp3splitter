package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javazoom.jl.decoder.JavaLayerException;
import mp3.MP3Info;
import mp3.MP3Player;
import mp3.PlaybackListener;
import utils.OptionValues;
import utils.ShortcutsEnum;
import utils.TimeData;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

import de.muntjak.tinylookandfeel.TinySliderUI;

public class EditDialog extends JDialog implements ComponentListener,
		MouseWheelListener {

	private static final String PAUSE_PREF = "p";

	TimeSpinner beginSpinner;
	TimeSpinner endSpinner;
	private JLabel beginJL;
	private JLabel endJL;

	private JLabel posFragJL;
	private JLabel posOrigJL;
	private JLabel fragmentJL;
	private JLabel sourceJL;
	private JLabel durFragJL;
	private JLabel durOrigJL;
	private JLabel slash1JL;
	private JLabel slash2JL;

	private JButton playPauseJB;
	private JButton beginJB;
	private JButton cancelJB;
	private JButton okJB;

	private JButton editBeginJB;
	private JButton editEndJB;
	private JButton endJB;
	private JCheckBox pauseAfterCB;

	private MySlider progressSlider;

	TimeData beginTime;
	TimeData endTime;
	private TimeData origCurrTime;
	private TimeData fragCurrTime;
	private TimeData durationTime;

	private TimeData origBeginTime;
	private TimeData origEndTime;

	MP3Info mp3Info;

	private MP3Player mp3Player;

	int beginFrame;
	int endFrame;
	private boolean canChangeValue = true;
	private boolean isPlaying = false;
	private PlayerStateListener playerStateListener;
	JMP3SplitterFrame jmp3SplitterFrame;

	private boolean jmpToEnd;
	int seconds;
	private ResourceBundle resourceBundle;

	public EditDialog(JMP3SplitterFrame parent, MP3Info mp3Info,
			TimeData beginTime, TimeData endTime, int beginFrame, int endFrame)
			throws IOException, JavaLayerException {
		super(parent, "Pause", true); //$NON-NLS-1$

		this.jmp3SplitterFrame = parent;

		playerStateListener = new PlayerStateListener();
		this.beginTime = new TimeData(beginTime.getCurrentMillis(), beginTime
				.getMaxMillis());
		this.endTime = new TimeData(endTime.getCurrentMillis(), endTime
				.getMaxMillis());
		this.durationTime = calcFragDuration();

		this.origBeginTime = beginTime;
		this.origEndTime = endTime;
		this.mp3Info = mp3Info;
		this.beginFrame = beginFrame;
		this.endFrame = endFrame;
		beginSpinner = new TimeSpinner(this.beginTime);
		endSpinner = new TimeSpinner(this.endTime);

		// loadPreferences();
		seconds = parent.options.getSeconds();
		jmpToEnd = parent.options.isJmpToEnd();

		if (parent.options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle("EditDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("EditDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}

		mp3Player = new MP3Player(mp3Info, playerStateListener, beginFrame,
				endFrame);
		origCurrTime = new TimeData(beginTime.getCurrentMillis(), beginTime
				.getMaxMillis());
		fragCurrTime = new TimeData(0, beginTime.getMaxMillis());

		Dimension d = new Dimension(432, 290);
		setSize(d);
		setMinimumSize(d);

		if (parent != null) {

			Point p = parent.getLocation();
			Dimension dim = parent.getSize();
			int x = p.x + (dim.width / 2) - (getWidth() / 2);
			int y = p.y + (dim.height / 2) - (getHeight() / 2);
			setLocation(x, y);
		}

		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");//UIManager.getSystemLookAndFeelClassName());// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, jmp3SplitterFrame.options)
					.setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);

		initComponents();
		progressSlider.setToolTipText(origCurrTime.toStringShorter());
		addComponentListener(this);
	}

	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public void setBeginFrame(int frame) {
		beginFrame = frame;
		try {
			mp3Player.setBeginFrame(beginFrame);
			mp3Player.stop();
		} catch (Exception e) {
			new ErrorDialog(this, e, jmp3SplitterFrame.options)
					.setVisible(true);
			e.printStackTrace();
		}
		beginTime.setCurrentMillis(mp3Player.getCurrMillis());
		beginSpinner.setValue(beginTime);
	}
	
	public void setEndFrame(int frame) {
		endFrame = frame;
		mp3Player.setEndFrame(endFrame);
//		try {
//			mp3Player.stop();
//		} catch (Exception e) {
//			new ErrorDialog(this, e, jmp3SplitterFrame.options)
//					.setVisible(true);
//			e.printStackTrace();
//		}
		int f = endFrame;
		if (mp3Info.hasAdditionalFrame && f > 0)
			f--;

		endTime.setCurrentMillis(Math.round((float) f
				* mp3Info.msPerFrame));
		endSpinner.setValue(endTime);
		//beginTimeJL.setText(endTime.toString());
		endButtonAction();
	}

	private void initComponents() {
		setTitle(getString("edit")); //$NON-NLS-1$
		beginJL = new JLabel();
		endJL = new JLabel();
		playPauseJB = new JButton(">"); //$NON-NLS-1$
		beginJB = new JButton("|<"); //$NON-NLS-1$
		endJB = new JButton(">...|"); //$NON-NLS-1$
		cancelJB = new JButton();
		okJB = new JButton("OK"); //$NON-NLS-1$
		progressSlider = new MySlider();

		fragmentJL = new JLabel(getString("fragment")); //$NON-NLS-1$
		sourceJL = new JLabel(getString("source")); //$NON-NLS-1$
		slash1JL = new JLabel(" / "); //$NON-NLS-1$
		slash2JL = new JLabel(" / "); //$NON-NLS-1$

		editBeginJB = new JButton(getString("editBegin")); //$NON-NLS-1$
		editEndJB = new JButton(getString("editEnd")); //$NON-NLS-1$
		pauseAfterCB = new JCheckBox(getString("pauseAfter")); //$NON-NLS-1$

		posOrigJL = new JLabel(origCurrTime.toStringShorter());
		durOrigJL = new JLabel(new TimeData(origEndTime.getMaxMillis(),
				origEndTime.getMaxMillis()).toStringShorter());
		durFragJL = new JLabel(calcFragDuration().toStringShorter());
		posFragJL = new JLabel(calcFragPosition().toStringShorter());

		cancelJB.setText(getString("cancel")); //$NON-NLS-1$
		endJL.setText(getString("endJL")); //$NON-NLS-1$
		beginJL.setText(getString("beginJL")); //$NON-NLS-1$

		Preferences prefs = Preferences.userNodeForPackage(getClass());

		String s = prefs.get(PAUSE_PREF, "t");
		if (s.equals("t"))
			pauseAfterCB.setSelected(true);

		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					mp3Player.stop();
					mp3Player.close();
				} catch (Exception ex) {
					new ErrorDialog(EditDialog.this, ex,
							jmp3SplitterFrame.options).setVisible(true);
					ex.printStackTrace();
				}
				dispose();
			}
		});
		setLayout(new BorderLayout());

		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new GridBagLayout());
		rootPanel.setBorder(new EmptyBorder(6, 10, 10, 10));

		JPanel timePanel = new JPanel();
		timePanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("timeTitle")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$
		timePanel.setLayout(new BoxLayout(timePanel, BoxLayout.X_AXIS));
		timePanel.add(fragmentJL);
		timePanel.add(posFragJL);
		timePanel.add(slash1JL);
		timePanel.add(durFragJL);
		timePanel.add(Box.createHorizontalGlue());
		timePanel.add(sourceJL);
		timePanel.add(posOrigJL);
		timePanel.add(slash2JL);
		timePanel.add(durOrigJL);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		rootPanel.add(timePanel, gbc);

		JPanel playerPanel = new JPanel();
		playerPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("player")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$
		playerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		// c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		playerPanel.add(beginJL, c);

		c.gridx = 1;
		playerPanel.add(beginSpinner, c);

		c.gridx = 2;
		c.weightx = 1.0;
		c.anchor = GridBagConstraints.EAST;
		playerPanel.add(endJL, c);

		c.gridx = 3;
		c.weightx = 0.0;
		playerPanel.add(endSpinner, c);

		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		c.gridwidth = 4;
		// c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 0, 0, 0);
		playerPanel.add(progressSlider, c);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		// buttonsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		buttonsPanel.add(playPauseJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(beginJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(endJB);
		buttonsPanel.add(Box.createHorizontalGlue());

		buttonsPanel.add(pauseAfterCB);

		c.gridy = 2;
		playerPanel.add(buttonsPanel, c);

		gbc.gridy = 1;
		rootPanel.add(playerPanel, gbc);

		JPanel precisionPanel = new JPanel();
		precisionPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("precisionTitle")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$
		precisionPanel.setLayout(new GridLayout(1, 2, 7, 0));
		precisionPanel.add(editBeginJB);
		precisionPanel.add(editEndJB);

		gbc.weighty = 1.0;
		gbc.gridy = 2;
		rootPanel.add(precisionPanel, gbc);

//		Dimension dim = cancelJB.getPreferredSize();
//		dim.width = 96;
//		okJB.setSize(dim);
//		okJB.setPreferredSize(dim);
//		okJB.setMinimumSize(dim);
//		
//		cancelJB.setSize(dim);
//		cancelJB.setPreferredSize(dim);
//		cancelJB.setMinimumSize(dim);
		okJB.setSize(cancelJB.getSize());
		okJB.setPreferredSize(cancelJB.getPreferredSize());
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
		bottomPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
		bottomPanel.add(Box.createHorizontalGlue());
		bottomPanel.add(okJB);
		bottomPanel.add(Box.createHorizontalStrut(5));
		bottomPanel.add(cancelJB);
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.PAGE_END;
		rootPanel.add(bottomPanel, gbc);

		add(rootPanel, BorderLayout.CENTER);

		// if (mp3Info.hasInfoFrame() || mp3Info.hasVBRFrame()) {
		// progressSlider.setValue(1);
		// progressSlider.setMinimum(1);
		// } else {
		// progressSlider.setValue(0);
		// progressSlider.setMinimum(0);
		// }

		progressSlider.setMinimum(beginFrame);
		progressSlider.setValue(beginFrame);
		progressSlider.setMaximum(endFrame);

		ProgressSliderListener psl = new ProgressSliderListener();

		// progressSlider.addMouseWheelListener(psl);
		addMouseWheelListener(this);
		progressSlider.addKeyListener(psl);
		progressSlider.addChangeListener(psl);
		progressSlider.addMouseListener(psl);

		cancelJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//System.out.println(EditDialog.this.getSize());
				try {
					mp3Player.stop();
					mp3Player.close();
				} catch (Exception ex) {
					new ErrorDialog(EditDialog.this, ex,
							jmp3SplitterFrame.options).setVisible(true);
					ex.printStackTrace();
				}

				EditDialog.this.dispose();

			}

		});

		okJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				origBeginTime.setCurrentMillis(beginTime.getCurrentMillis());
				origBeginTime.setMaxMillis(origBeginTime.getMaxMillis());

				TimeData temp = new TimeData(endTime.getMaxMillis(), endTime
						.getMaxMillis());
				if (endTime.toStringShorter().equals(temp.toStringShorter()))
					endTime = temp;

				origEndTime.setCurrentMillis(endTime.getCurrentMillis());
				origEndTime.setMaxMillis(origEndTime.getMaxMillis());

				try {
					mp3Player.stop();
					mp3Player.close();
				} catch (Exception ex) {
					new ErrorDialog(EditDialog.this, ex,
							jmp3SplitterFrame.options).setVisible(true);
					ex.printStackTrace();
				}

				jmp3SplitterFrame.setBeginFrame(mp3Player.getBeginFrame());
				jmp3SplitterFrame.setEndFrame(mp3Player.getEndFrame());

				if (jmpToEnd) {
					jmp3SplitterFrame.setPlayerPos(mp3Player.getEndFrame());
				}

				Preferences prefs = Preferences.userNodeForPackage(getClass());
				if (pauseAfterCB.isSelected())
					prefs.put(PAUSE_PREF, "t");
				else
					prefs.put(PAUSE_PREF, "f");
				EditDialog.this.dispose();
			}
		});

		endSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// if (!esMousePressed)
				endSpinnerStateChanged();
			}
		});

		beginSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				beginSpinnerStateChanged();
			}
		});

		playPauseJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				playPauseButtonAction();

			}

		});

		beginJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				beginButtonAction();
			}

		});

		endJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				endButtonAction();
			}

		});

		editBeginJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				editBeginAction();
			}

		});

		editEndJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				editEndAction();
			}

		});
	}

	private void editBeginAction() {
		mp3Player.pause();
		HighPrecisionBeginDialog high = new HighPrecisionBeginDialog(this);
		high.setVisible(true);
		
//		try {
//			mp3Player.stop();
//		} catch (Exception e) {
//			e.printStackTrace();
//			new ErrorDialog(this, e, jmp3SplitterFrame.options)
//					.setVisible(true);
//		}
		progressSlider.requestFocusInWindow();
	}

	private void editEndAction() {
		mp3Player.pause();
		HighPrecisionEndDialog high = new HighPrecisionEndDialog(this);
		high.setVisible(true);
		//endButtonAction();
//		try {
//			mp3Player.stop();
//		} catch (Exception e) {
//			e.printStackTrace();
//			new ErrorDialog(this, e, jmp3SplitterFrame.options)
//					.setVisible(true);
//		}
		progressSlider.requestFocusInWindow();
	}

	private TimeData calcFragDuration() {
		int beg = beginTime.getCurrentMillis();
		int end = endTime.getCurrentMillis();
		if (durationTime == null)
			durationTime = new TimeData(end - beg, endTime.getMaxMillis());
		else
			durationTime.setCurrentMillis(end - beg);

		return durationTime;
	}

	private TimeData calcFragPosition() {
		fragCurrTime.setCurrentMillis(origCurrTime.getCurrentMillis()
				- beginTime.getCurrentMillis());
		return fragCurrTime;
	}

	private void playPauseButtonAction() {
		if (mp3Player == null)
			return;
		if (!mp3Player.isPlaying()) {
			mp3Player.play();
		} else {
			mp3Player.pause();
		}
		progressSlider.requestFocusInWindow();
	}

	private void beginButtonAction() {

		if (isPlaying) {
			try {
				mp3Player.stop();
				mp3Player.play();
			} catch (Exception ex) {
				new ErrorDialog(EditDialog.this, ex, jmp3SplitterFrame.options)
						.setVisible(true);
				ex.printStackTrace();
			}
		} else {
			progressSlider.setValue(progressSlider.getMinimum());
			try {
				mp3Player.seek(progressSlider.getValue());
			} catch (Exception e) {
				new ErrorDialog(this, e, jmp3SplitterFrame.options)
						.setVisible(true);
				e.printStackTrace();
			}
			//setTitle(getString("stop") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
			//		+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$
		}
		progressSlider.requestFocusInWindow();
	}

	private void endButtonAction() {
		int frames = Math.round((float) (seconds * 1000) / mp3Info.msPerFrame);
		int seekFrame = endFrame - frames;
		if (seekFrame < 0)
			seekFrame = 0;

		try {
			mp3Player.seek(seekFrame);
		} catch (Exception e) {
			e.printStackTrace();
			new ErrorDialog(this, e, jmp3SplitterFrame.options)
					.setVisible(true);
		}
	}

	// private int lastMsTime = 1000;

	private void beginSpinnerStateChanged() {
		if (pauseAfterCB.isSelected() && mp3Player.isPlaying())
			mp3Player.pause();

		// if (lastMsTime <= 100) {
		// beginTime.setCurrentMillis(0);
		// beginSpinner.setTimeData(beginTime);
		// }
		int endCurr = endTime.getCurrentMillis();
		int beginCurr = beginTime.getCurrentMillis();
		// lastMsTime = beginCurr;

		if (beginCurr >= endCurr) {
			int newTime = endTime.getCurrentMillis() - 1000;
			if (newTime < 0)
				newTime = 0;
			beginTime.setCurrentMillis(newTime);
			beginSpinner.setTimeData(beginTime);
			// endTime = new TimeData(endTime.getMaxMillis(), endTime
			// .getMaxMillis());
			// endSpinner.setTimeData(endTime);
			// // progressSlider.setMaximum(mp3Info.getFrames());
			// mp3Player.setLastFrame(mp3Info.frames);
			// progressSlider.setMaximum(mp3Info.frames);
		}

		beginFrame = Math.round((float) beginTime.getCurrentMillis()
				/ mp3Info.msPerFrame);
		if (mp3Info.hasAdditionalFrame)
			beginFrame++;
		progressSlider.setMinimum(beginFrame);
		progressSlider.setValue(beginFrame);
		try {
			mp3Player.setBeginFrame(beginFrame);
		} catch (Exception e) {
			new ErrorDialog(this, e, jmp3SplitterFrame.options)
					.setVisible(true);
			e.printStackTrace();
		}
		durationTime = calcFragDuration();
		durFragJL.setText(durationTime.toStringShorter());

		origCurrTime.setCurrentMillis(mp3Player.getCurrMillis());
		posOrigJL.setText(origCurrTime.toStringShorter());
		posFragJL.setText(calcFragPosition().toStringShorter());
	}

	private void endSpinnerStateChanged() {
		//System.out.println("stateChanged");
		if (pauseAfterCB.isSelected() && mp3Player.isPlaying())
			mp3Player.pause();

		int endCurr = endTime.getCurrentMillis();
		int beginCurr = beginTime.getCurrentMillis();

		if (endCurr <= beginCurr) {
			int newTime = beginTime.getCurrentMillis() + 1000;
			if (newTime > endTime.getMaxMillis())
				newTime = endTime.getMaxMillis();
			endTime.setCurrentMillis(newTime);
			endSpinner.setTimeData(endTime);
		}

		endFrame = Math.round((float) endTime.getCurrentMillis()
				/ mp3Info.msPerFrame);
		if (mp3Info.hasAdditionalFrame)
			endFrame++;
		if (endFrame > mp3Info.frames)
			endFrame = mp3Info.frames;

		try {
			boolean play = isPlaying;
			if (play)
				mp3Player.pause();

			mp3Player.setEndFrame(endFrame);

			int frames = Math.round((float) (seconds * 1000)
					/ mp3Info.msPerFrame);
			int seekFrame = endFrame - frames;
			if (seekFrame < 0)
				seekFrame = 0;
			progressSlider.setMaximum(endFrame);
			progressSlider.setValue(seekFrame);
			// System.out.println("seekFrame = "+ seekFrame + "; endFrame = " +
			// mp3Player.getEndFrame());
			mp3Player.seek(seekFrame);
			if (play)
				mp3Player.play();
		} catch (Exception e) {
			new ErrorDialog(this, e, jmp3SplitterFrame.options)
					.setVisible(true);
			e.printStackTrace();
		}

		durationTime = calcFragDuration();
		durFragJL.setText(durationTime.toStringShorter());

		origCurrTime.setCurrentMillis(mp3Player.getCurrMillis());
		posOrigJL.setText(origCurrTime.toStringShorter());
		posFragJL.setText(calcFragPosition().toStringShorter());
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		progressSlider.requestFocusInWindow();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (mp3Player == null)
			return;

		try {
			if (e.getWheelRotation() == -1)
				mp3Player.seekAhead5s();
			else
				mp3Player.seekBack5s();
		} catch (Exception ex) {
			new ErrorDialog(EditDialog.this, ex, jmp3SplitterFrame.options)
					.setVisible(true);
			ex.printStackTrace();
		}
	}

	private class PlayerStateListener implements PlaybackListener {

		public void playbackStarted(MP3Player player) {
			playPauseJB.setText("||"); //$NON-NLS-1$
			isPlaying = true;
			// playPauseButton.setEnabled(false);
			// playPauseButton.setText("||");
		}

		public void playbackPaused(MP3Player player) {
			playPauseJB.setText(">"); //$NON-NLS-1$
			isPlaying = false;
			//setTitle(getString("pause") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
			//		+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$
			// playButton.setEnabled(true);
			// pauseButton.setEnabled(false);

			// stopButton.setEnabled(false);
		}

		public void playbackStoped(MP3Player player) {
			fragCurrTime.setCurrentMillis(beginTime.getCurrentMillis());
			progressSlider.setValue(beginFrame);
			isPlaying = false;
			//setTitle(getString("stop") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
			//		+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$

			// posFragJL.setText(calcFragPosition().toStringShorter());

			// pauseButton.setEnabled(false);
			// playButton.setEnabled(true);
			// stopButton.setEnabled(false);
		}

		public void playbackFinished(MP3Player player) {
			// System.out.println("PLAYBACK FINISHED");
			playPauseJB.setText(">"); //$NON-NLS-1$
			try {
				mp3Player.stop();
			} catch (Exception ex) {
				new ErrorDialog(EditDialog.this, ex, jmp3SplitterFrame.options)
						.setVisible(true);
				ex.printStackTrace();
			}
			isPlaying = false;
		}

		public void playbackTimeChanged(MP3Player player) {
			// if(!canChangeValue)

			// System.out.println("SDFDSD " + mp3Player.getFramePosition());
			// System.out.println("xxdfs = " + mp3Player.getFirstFrame());
			origCurrTime.setCurrentMillis(mp3Player.getCurrMillis());
			posOrigJL.setText(origCurrTime.toStringShorter());
			posFragJL.setText(calcFragPosition().toStringShorter());
			// if (isPlaying)
			//					setTitle(getString("playing") + origCurrTime.toStringShorter() //$NON-NLS-1$
			//							+ "> [" + calcFragDuration().toStringShorter() //$NON-NLS-1$
			//							+ "]"); //$NON-NLS-1$
			// else
			//					setTitle(getString("pause") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
			//							+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$
			if (canChangeValue) {
				// progressSlider.setMinimum(mp3Player.getBeginFrame());
				progressSlider.setValue(mp3Player.getCurrFrame());
			}
		}
	}

	private class ProgressSliderListener implements KeyListener, FocusListener,
			ChangeListener, MouseListener {

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			int mod = e.getModifiers();
			try {
				if (ShortcutsEnum.ONE_MIN_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead1min();
				} else if (ShortcutsEnum.ONE_MIN_BACK.isEqual(key, mod)) {
					mp3Player.seekBack1min();
				} else if (ShortcutsEnum.FIVE_S_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead5s();
				} else if (ShortcutsEnum.FIVE_S_BACK.isEqual(key, mod)) {
					mp3Player.seekBack5s();
				} else if (ShortcutsEnum.PLAY_PAUSE.isEqual(key, mod)) {
					if (!mp3Player.isPlaying()) {
						// if (progressSlider.getValue() == progressSlider
						// .getMaximum()) {
						// mp3Player.stop();
						// }
						mp3Player.play();
					} else {
						mp3Player.pause();
					}
				} else if (ShortcutsEnum.STOP.isEqual(key, mod)) {
					mp3Player.stop();
				}
			} catch (Exception ex) {
				new ErrorDialog(EditDialog.this, ex, jmp3SplitterFrame.options)
						.setVisible(true);
				ex.printStackTrace();
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
		}

		@Override
		public void focusGained(FocusEvent arg0) {
		}

		@Override
		public void focusLost(FocusEvent arg0) {
		}

		@Override
		public void stateChanged(ChangeEvent arg0) {
			if (!canChangeValue) {
				if (mp3Player == null)
					return;

				int val = progressSlider.getValue();
				if (mp3Player.info.hasAdditionalFrame)
					val -= 1;

				TimeData tempTime = new TimeData(Math
						.round(((float) val * mp3Player.info.msPerFrame)),
						origCurrTime.getMaxMillis());
				progressSlider.setToolTipText(tempTime.toStringShorter());
				// origCurrTime.setCurrentMillis(Math
				// .round(((float) val * mp3Player.info.msPerFrame)));
				//
				// progressSlider.setToolTipText(origCurrTime.toStringShorter());
				// // progressSlider.
				//				setTitle(getString("seeking") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
				//						+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$
			}
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			canChangeValue = false;
			int val = progressSlider.getValue();
			if (mp3Player.info.hasAdditionalFrame)
				val -= 1;

			TimeData tempTime = new TimeData(Math
					.round(((float) val * mp3Player.info.msPerFrame)),
					origCurrTime.getMaxMillis());
			progressSlider.setToolTipText(tempTime.toStringShorter());
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			if (mp3Player == null)
				return;

			try {
				// int pos =
				// if(pos)
				mp3Player.seek(progressSlider.getValue());
			} catch (Exception e) {
				new ErrorDialog(EditDialog.this, e, jmp3SplitterFrame.options)
						.setVisible(true);
				e.printStackTrace();
			}
			canChangeValue = true;

			//			setTitle(getString("pause") + origCurrTime.toStringShorter() + "> [" //$NON-NLS-1$ //$NON-NLS-2$
			//					+ calcFragDuration().toStringShorter() + "]"); //$NON-NLS-1$
		}
	}
}
