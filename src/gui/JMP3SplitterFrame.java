package gui;

import java.awt.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//import com.jgoodies.looks.plastic.
import com.sun.java.swing.plaf.windows.WindowsSliderUI; //import javax.swing.pl
import javazoom.jl.decoder.JavaLayerException;
import javax.swing.plaf.basic.*;
import javax.swing.plaf.metal.MetalSliderUI;

import de.muntjak.tinylookandfeel.TinySliderUI;

import java.awt.event.*;
import java.io.*;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import static java.awt.GridBagConstraints.*;

import utils.*;
import mp3.*;
import mp3.ProgressMonitor;

public class JMP3SplitterFrame extends JFrame implements DropTargetListener,
		MouseWheelListener {

	private static final String VERSION = "1.2.8"; //$NON-NLS-1$
	private static final String OPEN_PREF = "dir"; //$NON-NLS-1$
	private static final String EMPTY_TIME = "--:--.-"; //$NON-NLS-1$

	private JButton openButton;
	private JButton playButton;
	private JButton pauseButton;
	private JButton stopButton;
	private JButton markBeginButton;
	private JButton markEndButton;
	private JButton editMarkedButton;
	private JButton saveButton;
	private JButton startButton;
	private JButton endButton;
	private JButton aboutButton;
	private JButton optionsButton;
	private JButton exitButton;

	private MySlider progressSlider;

	private JLabel fileNameLabel;
	private JLabel posJL;
	private JLabel dur1JL;
	private JLabel dur2JL;
	private JLabel beginJL;
	private JLabel endJL;

	private JLabel posLabel;
	private JLabel durationLabel;
	private JLabel selDurationLabel;

	private File mp3File;
	private TimeData actualTime;
	private TimeData durationTime;
	private TimeData selBeginTime;
	private TimeData selEndTime;
	private TimeData selDurationTime;

	private MP3Info mp3Info;
	private MP3Player mp3Player;
	private int beginFrame;
	private int endFrame;
	private TagData tagData;

	private PlayerStateListener playerStateListener;
	private boolean canChangeSliderValue = true;
	private int minimalSliderValue;
	private ResourceBundle resourceBundle;

	private JPanel openPanel;
	private JPanel playerPanel;
	private JPanel selectionPanel;

	OptionValues options;

	public JMP3SplitterFrame() {
		try {
			UIManager
					.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, options).setVisible(true);
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(this);

		initOptions();
		initComponents();
		new DropTarget(this, this);

		Thread
				.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						new ErrorDialog(JMP3SplitterFrame.this,
								new Exception(e), options).setVisible(true);
					}
				});

	}

	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	private void initOptions() {

		options = OptionValues.getDefaults();
		Preferences prefs = Preferences.userNodeForPackage(options.getClass());

		String lang = prefs.get(OptionValues.LANG_PREF, options.getLang());
		options.setLang(lang);
		if (lang.equals("en")) {
			resourceBundle = ResourceBundle.getBundle("JMP3SplitterFrame",
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle("JMP3SplitterFrame",
					OptionValues.LOCALE_PL);
		}

		boolean jmpToEnd = Boolean.parseBoolean(prefs.get(
				OptionValues.JMP_PREF, Boolean.toString(options.isJmpToEnd())));
		options.setJmpToEnd(jmpToEnd);

		boolean autofill = Boolean.parseBoolean(prefs.get(
				OptionValues.AUTOFILL_PREF, Boolean.toString(options
						.isAutofillTag())));
		options.setAutofillTag(autofill);

		String sec = prefs.get(OptionValues.SECONDS_PREF, Integer
				.toString(options.getSeconds()));

		options.setSeconds(Integer.parseInt(sec));

		String val = prefs.get(OptionValues.FIVE_S_AHEAD_PREF, options
				.getFiveSAhead());
		options.setFiveSAhead(val);
		String[] vals = val.split(";"); //$NON-NLS-1$

		int key = Integer.parseInt(vals[0]);
		int mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.FIVE_S_AHEAD.setKeys(key, mod);

		val = prefs.get(OptionValues.FIVE_S_BACK_PREF, options.getFiveSBack());
		options.setFiveSBack(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.FIVE_S_BACK.setKeys(key, mod);

		val = prefs.get(OptionValues.ONE_MIN_AHEAD_PREF, options
				.getOneMinAhead());
		options.setOneMinAhead(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.ONE_MIN_AHEAD.setKeys(key, mod);

		val = prefs
				.get(OptionValues.ONE_MIN_BACK_PREF, options.getOneMinBack());
		options.setOneMinBack(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.ONE_MIN_BACK.setKeys(key, mod);

		val = prefs.get(OptionValues.MARK_BEGIN_PREF, options.getMarkBegin());
		options.setMarkBegin(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.MARK_BEGIN.setKeys(key, mod);

		val = prefs.get(OptionValues.MARK_END_PREF, options.getMarkEnd());
		options.setMarkEnd(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.MARK_END.setKeys(key, mod);

		val = prefs.get(OptionValues.PLAY_PAUSE_PREF, options.getPlayPause());
		options.setPlayPause(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.PLAY_PAUSE.setKeys(key, mod);

		val = prefs.get(OptionValues.STOP_PREF, options.getStop());
		options.setStop(val);
		vals = val.split(";"); //$NON-NLS-1$
		key = Integer.parseInt(vals[0]);
		mod = Integer.parseInt(vals[1]);
		ShortcutsEnum.STOP.setKeys(key, mod);

		String rollS = prefs.get(OptionValues.ROLL_PREF, options.getRoll());
		options.setRoll(rollS);
	}

	private void setLocalizedStrings() {
		openButton.setText(getString("openDots")); //$NON-NLS-1$
		saveButton.setText(getString("save")); //$NON-NLS-1$
		aboutButton.setText(getString("about")); //$NON-NLS-1$
		optionsButton.setText(getString("options")); //$NON-NLS-1$
		exitButton.setText(getString("exit")); //$NON-NLS-1$
		if (mp3Info == null)
			fileNameLabel.setText(getString("noFile")); //$NON-NLS-1$
		else
			fileNameLabel.setText(mp3Info.mp3File.getName());
		posJL.setText(getString("position") + " "); //$NON-NLS-1$
		dur1JL.setText(getString("duration") + " "); //$NON-NLS-1$
		dur2JL.setText(getString("duration")); //$NON-NLS-1$
		beginJL.setText(getString("begin")); //$NON-NLS-1$
		endJL.setText(getString("end")); //$NON-NLS-1$

		openPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("sourceFile")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$

		playerPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("player")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$

		selectionPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("fragment")), new EmptyBorder(0, 5, 5, 5))); //$NON-NLS-1$
	}

	private void initComponents() {
		playerStateListener = new PlayerStateListener();

		setTitle("JMP3Splitter " + VERSION); //$NON-NLS-1$
		Image icon = //new ImageIcon(ClassLoader.getSystemResource("nutka.png"))
	//			.getImage();
		Toolkit.getDefaultToolkit().getImage("./nutka.png"); //$NON-NLS-1$
		setIconImage(icon);

		openButton = new JButton();
		saveButton = new JButton();
		aboutButton = new JButton();
		optionsButton = new JButton();
		exitButton = new JButton();
		playButton = new JButton(">"); //$NON-NLS-1$
		pauseButton = new JButton("||"); //$NON-NLS-1$
		stopButton = new JButton("X"); //$NON-NLS-1$
		markBeginButton = new JButton("["); //$NON-NLS-1$
		markEndButton = new JButton("]"); //$NON-NLS-1$
		editMarkedButton = new JButton("[ > ]"); //$NON-NLS-1$
		startButton = new JButton(EMPTY_TIME);
		endButton = new JButton(EMPTY_TIME);
		
		// openButton.addKeyListener(this);

		progressSlider = new MySlider(0, 100, 0);
		// sliderToolTip = new SliderToolTip(progressSlider);

		fileNameLabel = new JLabel();
		posJL = new JLabel();
		dur1JL = new JLabel();
		dur2JL = new JLabel();
		beginJL = new JLabel();
		endJL = new JLabel();

		posLabel = new JLabel(EMPTY_TIME);
		durationLabel = new JLabel(EMPTY_TIME); // 00:00.0
		selDurationLabel = new JLabel(EMPTY_TIME);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		setSize(474, 298);
		setMinimumSize(getSize());
		// setMaximumSize(new Dimension(2000, 350));

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (dim.width / 2) - (getWidth() / 2);
		int y = (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);

		JPanel rootPanel = new JPanel();
		rootPanel.setBorder(new EmptyBorder(8, 12, 12, 12));
		rootPanel.setLayout(new GridBagLayout());

		add(rootPanel, BorderLayout.CENTER);

		openPanel = new JPanel();

		openPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = EAST;
		gbc.weightx = 1.0;
		gbc.fill = HORIZONTAL;
		openPanel.add(fileNameLabel, gbc);

		gbc.fill = NONE;
		gbc.weightx = 0.0;
		gbc.gridx = 1;
		gbc.insets = new Insets(0, 5, 0, 0);
		openPanel.add(openButton, gbc);

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		rootPanel.add(openPanel, c);

		playerPanel = new JPanel();

		playerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = NONE;
		c1.gridx = 0;
		c1.gridy = 0;
		// c1.gridwidth = 2;
		// c1.weightx = 1.0;
		c1.anchor = WEST;
		playerPanel.add(posJL, c1);

		c1.gridx = 1;
		c1.weightx = 1.0;
		playerPanel.add(posLabel, c1);

		c1.gridx = 2;
		c1.gridwidth = 1;
		c1.anchor = EAST;
		c1.weightx = 0.0;
		playerPanel.add(dur1JL, c1);

		c1.gridx = 3;
		// c1.insets = new Insets(0, 10, 0, 0);
		playerPanel.add(durationLabel, c1);

		c1.gridx = 0;
		c1.gridy = 1;
		c1.fill = HORIZONTAL;
		c1.insets = new Insets(10, 0, 10, 0);
		c1.weightx = 1.0;
		c1.gridwidth = 4;
		c1.anchor = CENTER;
		playerPanel.add(progressSlider, c1);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel
				.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
		buttonsPanel.add(playButton);
		buttonsPanel.add(pauseButton);
		buttonsPanel.add(stopButton);
		buttonsPanel.add(Box.createHorizontalStrut(20));

		buttonsPanel.add(markBeginButton);
		buttonsPanel.add(markEndButton);
		buttonsPanel.add(editMarkedButton);

		buttonsPanel.add(Box.createHorizontalGlue());
		buttonsPanel.add(saveButton);

		c1.gridy = 2;
		c1.insets = new Insets(0, 0, 0, 0);
		playerPanel.add(buttonsPanel, c1);
		playerPanel.setEnabled(false);

		c.gridy = 2;
		rootPanel.add(playerPanel, c);

		selectionPanel = new JPanel();

		selectionPanel.setLayout(new BoxLayout(selectionPanel,
				BoxLayout.LINE_AXIS));
		selectionPanel.add(dur2JL);
		selectionPanel.add(Box.createHorizontalStrut(5));
		selectionPanel.add(selDurationLabel);
		selectionPanel.add(Box.createHorizontalGlue());
		selectionPanel.add(beginJL);
		selectionPanel.add(Box.createHorizontalStrut(5));
		selectionPanel.add(startButton);
		selectionPanel.add(Box.createHorizontalStrut(10));
		selectionPanel.add(endJL);
		selectionPanel.add(Box.createHorizontalStrut(5));
		selectionPanel.add(endButton);

		c.gridy = 1;
		rootPanel.add(selectionPanel, c);

		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
		bottomPanel.add(aboutButton);
		bottomPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		bottomPanel.add(optionsButton);
		bottomPanel.add(Box.createHorizontalGlue());
		bottomPanel.add(exitButton);

		c.gridy = 3;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.PAGE_END;
		rootPanel.add(bottomPanel, c);

		setAllDisabled();

		openButton.addActionListener(new OpenButtonListener());
		ProgressSliderListener psl = new ProgressSliderListener();

		progressSlider.addKeyListener(psl);
		addMouseWheelListener(this);
		progressSlider.addChangeListener(psl);
		progressSlider.addMouseListener(psl);
		// progressSlider.addFocusListener(psl);
		// progressSlider.setSnapToTicks(true);

		editMarkedButton.addActionListener(new EditButtonListener());
		saveButton.addActionListener(new SaveButtonListener());
		markBeginButton.addActionListener(new MarkBeginButtonListener());
		markEndButton.addActionListener(new MarkEndButtonListener());

		setLocalizedStrings();

		exitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// System.out.println(JMP3SplitterFrame.this.getSize());
				System.exit(0);
			}

		});

		playButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					if (progressSlider.getValue() == progressSlider
							.getMaximum())
						try {
							mp3Player.stop();
						} catch (Exception e) {
							e.printStackTrace();
							new ErrorDialog(JMP3SplitterFrame.this, e, options)
									.setVisible(true);
						}
					mp3Player.play();
				}
				progressSlider.requestFocusInWindow();
			}
		});

		pauseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					mp3Player.pause();
				}
				progressSlider.requestFocusInWindow();
			}
		});

		stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					try {
						mp3Player.stop();
					} catch (Exception e) {
						new ErrorDialog(JMP3SplitterFrame.this, e, options)
								.setVisible(true);
						e.printStackTrace();
					}
					// playButton.setEnabled(true);
					// pauseButton.setEnabled(false);
					// stopButton.setEnabled(false);
				}
				progressSlider.requestFocusInWindow();
			}
		});

		optionsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (OptionsDialog.showOptionsDialog(JMP3SplitterFrame.this)) {
					if (options.getLang().equals("en")) {
						resourceBundle = ResourceBundle.getBundle(
								"JMP3SplitterFrame", Locale.ENGLISH);
					} else {
						resourceBundle = ResourceBundle.getBundle(
								"JMP3SplitterFrame", OptionValues.LOCALE_PL);
					}
					setLocalizedStrings();
				}
				progressSlider.requestFocusInWindow();
			}
		});

		aboutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AboutDialog(JMP3SplitterFrame.this, VERSION)
						.setVisible(true);
				progressSlider.requestFocusInWindow();
			}
		});

		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startButtonAction();
				progressSlider.requestFocusInWindow();
			}
		});

		endButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endButtonAction();
				progressSlider.requestFocusInWindow();
			}
		});
	}

	public int getBeginFrame() {
		return beginFrame;
	}

	public void setBeginFrame(int beginFrame) {
		this.beginFrame = beginFrame;
	}

	public int getEndFrame() {
		return endFrame;
	}

	public void setEndFrame(int endFrame) {
		this.endFrame = endFrame;
	}

	public void setPlayerPos(int frame) {
		if (mp3Player == null)
			return;
		// progressSlider.setValue(frame);
		// actualTime.setCurrentMillis(Math.round(((float) frame * mp3Player
		// .getMsPerFame())));
		// posLabel.setText(actualTime.toStringShorter());
		try {
			mp3Player.seek(frame);
		} catch (Exception e) {
			new ErrorDialog(this, e, options).setVisible(true);
			e.printStackTrace();
		}
	}

	public TimeData getSelBeginTime() {
		return selBeginTime;
	}

	public void setSelBeginTime(TimeData selBeginTime) {
		this.selBeginTime = selBeginTime;
	}

	public TimeData getSelEndTime() {
		return selEndTime;
	}

	public void setSelEndTime(TimeData selEndTime) {
		this.selEndTime = selEndTime;
	}

	public TimeData getDurationTime() {
		return durationTime;
	}

	private void startButtonAction() {

		boolean ok = TimeDialog.showTimeDialog(this,
				TimeDialog.WhichTime.BEGIN, getString("begin")); //$NON-NLS-1$

		if (ok) {
			if (selEndTime != null) {
				if (selBeginTime.getCurrentMillis() >= selEndTime
						.getCurrentMillis()) {
					selEndTime = null;
				}
			}
			updateSelectionLabels();

			beginFrame = Math.round((float) selBeginTime.getCurrentMillis()
					/ mp3Info.msPerFrame);
			if (mp3Info.hasAdditionalFrame)
				beginFrame++;

		}
	}

	private void endButtonAction() {
		boolean ok = TimeDialog.showTimeDialog(this, TimeDialog.WhichTime.END,
				getString("end")); //$NON-NLS-1$

		if (ok) {
			if (selBeginTime != null) {
				if (selEndTime.getCurrentMillis() <= selBeginTime
						.getCurrentMillis()) {
					selBeginTime = null;
				}
			}

			updateSelectionLabels();
			endFrame = Math.round((float) selEndTime.getCurrentMillis()
					/ mp3Info.msPerFrame);
			if (mp3Info.hasAdditionalFrame)
				endFrame++;
			if (endFrame > mp3Info.frames)
				endFrame = mp3Info.frames;
			// try {
			// endFrame = MP3Utils.findFramePos(mp3Info, selEndTime
			// .getCurrentMillis());
			// System.out.printf("JMP3SF: currMillis = %d; maxMillis = %d\n",
			// selEndTime.getCurrentMillis(), selEndTime
			// .getMaxMillis());
			// } catch (IOException e) {
			// new ErrorDialog(this, e).setVisible(true);
			// e.printStackTrace();
			// }
		}
	}

	// public void setVisible(boolean v) {
	// super.setVisible(v);
	// System.out.println(openButton.requestFocusInWindow());
	// }

	private void enableSelectionPanel() {

		startButton.setEnabled(true);
		endButton.setEnabled(true);

		selDurationLabel.setEnabled(true);
		// jLabel1.setEnabled(true);
		// jLabel2.setEnabled(false);
		dur2JL.setEnabled(true);
		beginJL.setEnabled(true);
		endJL.setEnabled(true);

	}

	private void setAllDisabled() {
		posLabel.setEnabled(false);
		durationLabel.setEnabled(false);
		// startLabel.setEnabled(false);
		// endLabel.setEnabled(false);
		selDurationLabel.setEnabled(false);
		posJL.setEnabled(false);
		dur1JL.setEnabled(false);
		dur2JL.setEnabled(false);
		beginJL.setEnabled(false);
		endJL.setEnabled(false);

		saveButton.setEnabled(false);
		editMarkedButton.setEnabled(false);
		markBeginButton.setEnabled(false);
		markEndButton.setEnabled(false);
		progressSlider.setEnabled(false);
		playButton.setEnabled(false);

		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);
		startButton.setEnabled(false);
		endButton.setEnabled(false);
	}

	private void enableComponentsAfterError() {
		if (mp3File != null) {
			posLabel.setEnabled(true);
			durationLabel.setEnabled(true);

			posJL.setEnabled(true);
			dur1JL.setEnabled(true);

			markBeginButton.setEnabled(true);
			markEndButton.setEnabled(true);
			progressSlider.setEnabled(true);

			playButton.setEnabled(true);
			selDurationLabel.setEnabled(true);
			// jLabel1.setEnabled(false);
			// jLabel2.setEnabled(false);
			dur2JL.setEnabled(true);
			beginJL.setEnabled(true);
			endJL.setEnabled(true);
			startButton.setEnabled(true);
			endButton.setEnabled(true);

			if (selDurationTime != null) {
				saveButton.setEnabled(true);
				editMarkedButton.setEnabled(true);
			}

			fileNameLabel.setText(mp3File.getName());

			openButton.setEnabled(true);
		} else {
			fileNameLabel.setText(getString("noFile"));
			openButton.setEnabled(true);
		}
	}

	private void preparingDone() {

		int dur = Math.round(mp3Player.info.totalMs);
		durationTime = new TimeData(dur, dur);
		durationLabel.setText(durationTime.toStringShorter());

		actualTime = new TimeData(0, dur);
		posLabel.setText(actualTime.toStringShorter());
		progressSlider.setToolTipText(actualTime.toStringShorter());

		posLabel.setEnabled(true);
		durationLabel.setEnabled(true);

		posJL.setEnabled(true);
		dur1JL.setEnabled(true);

		markBeginButton.setEnabled(true);
		markEndButton.setEnabled(true);
		progressSlider.setEnabled(true);

		playButton.setEnabled(true);

		// System.out.println("JMP3SplitterFrame: frames = "
		// + mp3Player.getFramesCount());
		// System.out.println("JMP3SplitterFrame: hasInfoFrame = "
		// + mp3Player.hasInfoFrame());
		// System.out.println("JMP3SplitterFrame: hasVBRFrame = "
		// + mp3Player.hasVBRFrame());

		if (mp3Player.info.hasAdditionalFrame) {
			minimalSliderValue = 1;
		} else {
			minimalSliderValue = 0;
		}
		progressSlider.setValue(minimalSliderValue);
		progressSlider.setMinimum(minimalSliderValue);

		progressSlider.setMaximum(mp3Player.info.frames);

		// startLabel.setEnabled(true);
		// endLabel.setEnabled(true);
		selDurationLabel.setEnabled(true);
		// jLabel1.setEnabled(false);
		// jLabel2.setEnabled(false);
		dur2JL.setEnabled(true);
		beginJL.setEnabled(true);
		endJL.setEnabled(true);
		startButton.setEnabled(true);
		endButton.setEnabled(true);

		saveButton.setEnabled(false);
		editMarkedButton.setEnabled(false);

		startButton.setText(EMPTY_TIME);
		selDurationLabel.setText(EMPTY_TIME);
		endButton.setText(EMPTY_TIME);

		selBeginTime = null;
		selEndTime = null;
		selDurationTime = null;
		// markBeginButton.setEnabled(false);
		// markEndButton.setEnabled(false);
		// progressSlider.setEnabled(false);

		// playButton.setEnabled(false);
		// pauseButton.setEnabled(false);
		// stopButton.setEnabled(false);

		// new Thread() {
		// public void run() {
		// System.out.println("b111111");
		fileNameLabel.setText(mp3File.getName());
		openButton.setEnabled(true);
		// }
		// }.start();

	}

	private void loadPlayer(final File file) {
		fileNameLabel.setText(getString("readingFile")); //$NON-NLS-1$
		JMP3SplitterFrame.this.openButton.setEnabled(false);

		new Thread() {
			public void run() {

				try {
					MP3Player mp3temp = null;
					if (mp3Player != null) {
						mp3temp = mp3Player;
						mp3Player.stop();
					}

					setAllDisabled();

					// MP3InfoScanner scanner = new MP3InfoScanner(file,
					// playerStateListener);
					MP3Info info = MP3Info.scan(file, playerStateListener);
					mp3Info = info;

					mp3File = file;
					mp3Player = new MP3Player(info, playerStateListener);

					if (mp3temp != null)
						mp3temp.close();

					tagData = null;

					preparingDone();
					stopButton.setEnabled(true);
				} catch (EarlyEndException e) {
					doYouWantToFix(file);
					enableComponentsAfterError();

				} catch (IOException e) {
					new ErrorDialog(JMP3SplitterFrame.this, e, options)
							.setVisible(true);
					e.printStackTrace();
				} catch (JavaLayerException e) {
					new ErrorDialog(JMP3SplitterFrame.this, e, options)
							.setVisible(true);
					e.printStackTrace();
				}

			} // end run()
		}.start(); // end Thread

	}

	private void doYouWantToFix(File file) {

		Object[] options = { getString("yes"), getString("no") }; //$NON-NLS-1$ //$NON-NLS-2$
		int n = JOptionPane.showOptionDialog(
				JMP3SplitterFrame.this,
				getString("fixFile"),//$NON-NLS-1$
				getString("errorFile"), //$NON-NLS-1$
				JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null,
				options, options[0]);
		if (n != 0) {
			return;
		}

		UIManager.put("FileChooser.saveDialogTitleText", getString("saveDots")); //$NON-NLS-1$ //$NON-NLS-2$
		UIManager.put("FileChooser.cancelButtonText", getString("cancel")); //$NON-NLS-1$ //$NON-NLS-2$
		UIManager.put("FileChooser.saveButtonText", getString("save"));

		final JFileChooser fileChooser = new JFileChooser(new File(file
				.getParent()));
		SwingUtilities.updateComponentTreeUI(fileChooser);
		fileChooser.removeChoosableFileFilter(fileChooser
				.getAcceptAllFileFilter());

		fileChooser.setSelectedFile(getDefaultFixFile(file));
		fileChooser
				.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

					public boolean accept(File f) {
						return f.isDirectory()
								|| f.getName().toLowerCase().endsWith(".mp3"); //$NON-NLS-1$
					}

					public String getDescription() {
						return "*.mp3"; //$NON-NLS-1$
					}
				});
		while (true) {
			int returnVal = fileChooser.showSaveDialog(JMP3SplitterFrame.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File selectedFile = fileChooser.getSelectedFile();
				if (selectedFile != null) {
					if (selectedFile.exists()) {

						n = JOptionPane.showOptionDialog(
								JMP3SplitterFrame.this,
								getString("overwrite") + "\n"//$NON-NLS-1$
										+ selectedFile.getName(), //$NON-NLS-1$
								getString("fileExcists"), //$NON-NLS-1$
								JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE, null, options,
								options[0]);

						if (n == 1)
							return;

						if (!selectedFile.delete()) {
							String msg = getString("cantOverwrite") //$NON-NLS-1$
									+ selectedFile.getName() + "\n" //$NON-NLS-1$
									+ getString("typeOtherName"); //$NON-NLS-1$
							JOptionPane.showMessageDialog(
									JMP3SplitterFrame.this, msg,
									getString("useOtherName"), //$NON-NLS-1$
									JOptionPane.ERROR_MESSAGE);
							continue;
						}
					}

					fixMP3File(file, selectedFile);
					break;
				}
			} else
				break;
		}
		stopButton.setEnabled(true);
	}

	private File getDefaultFixFile(File f) {
		String name = f.getAbsolutePath();
		name = name.substring(0, name.length() - 4) + " (FIXED).mp3";
		return new File(name);
	}

	private void fixMP3File(final File inFile, final File outFile) {

		final ProgressDialog progressDialog = new ProgressDialog(this, outFile
				.getName());

		new Thread() {
			@Override
			public void run() {
				MP3Fixer fixer = new MP3Fixer(inFile, outFile,
						new ProgressMonitor() {

							@Override
							public void progressMade(int totalPercent) {
								progressDialog
										.setProgressBarValue(totalPercent);
							}

						});
				try {
					// System.out.println("fixing... :P");
					fixer.fix();
					loadPlayer(outFile);
				} catch (IOException e) {
					progressDialog.setVisible(false);
					e.printStackTrace();
					new ErrorDialog(JMP3SplitterFrame.this, e, options)
							.setVisible(true);
				}
			}
		}.start();
		progressDialog.setVisible(true);

	}

	private void updateSelectionLabels() {
		if (selBeginTime != null && selEndTime != null) {
			int diff = selEndTime.getCurrentMillis()
					- selBeginTime.getCurrentMillis();
			selDurationTime = new TimeData(diff, durationTime
					.getCurrentMillis());
			selDurationLabel.setText(selDurationTime.toStringShorter());
			editMarkedButton.setEnabled(true);
			saveButton.setEnabled(true);
			startButton.setText(selBeginTime.toStringShorter());
			endButton.setText(selEndTime.toStringShorter());
		} else {
			selDurationLabel.setText(EMPTY_TIME);
			editMarkedButton.setEnabled(false);
			saveButton.setEnabled(false);
		}

		if (selBeginTime == null)
			startButton.setText(EMPTY_TIME);
		else
			startButton.setText(selBeginTime.toStringShorter());

		if (selEndTime == null)
			endButton.setText(EMPTY_TIME);
		else
			endButton.setText(selEndTime.toStringShorter());

		// System.out.println(selBeginTime.toStringShorter());
	}

	private void markBeginButtonAction() {
		enableSelectionPanel();
		int current = Math
				.round(((float) progressSlider.getValue() * mp3Player.info.msPerFrame));
		if (selEndTime != null) {
			if (current >= selEndTime.getCurrentMillis()) {
				selEndTime = null;
			}
		}
		selBeginTime = new TimeData(current, durationTime.getMaxMillis());

		// } else {
		// selBeginTime = new TimeData(current, durationTime
		// .getMaxMillis());
		// }
		beginFrame = progressSlider.getValue();
		updateSelectionLabels();
		progressSlider.requestFocusInWindow();
	}

	private void markEndButtonAction() {
		enableSelectionPanel();
		int current = Math
				.round(((float) progressSlider.getValue() * mp3Player.info.msPerFrame));
		if (current == 0)
			return;
		if (selBeginTime != null) {
			if (current <= selBeginTime.getCurrentMillis()) {
				selBeginTime = null;
			}
		}
		selEndTime = new TimeData(current, durationTime.getMaxMillis());

		endFrame = progressSlider.getValue();
		// } else {
		// selEndTime = new TimeData(current, durationTime.getMaxMillis());
		// }

		updateSelectionLabels();
		progressSlider.requestFocusInWindow();
	}

	@Override
	public void dragEnter(DropTargetDragEvent arg0) {

	}

	@Override
	public void dragExit(DropTargetEvent arg0) {

	}

	@Override
	public void dragOver(DropTargetDragEvent arg0) {

	}

	@Override
	public void drop(DropTargetDropEvent e) {
		try {
			Transferable transferable = e.getTransferable();

			if (transferable
					.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
				e.acceptDrop(DnDConstants.ACTION_MOVE);
				java.util.List<File> fileList = (java.util.List<File>) transferable
						.getTransferData(DataFlavor.javaFileListFlavor);

				if (fileList.size() == 1) {
					File file = fileList.get(0);
					if (file.getName().toLowerCase().endsWith(".mp3")) { //$NON-NLS-1$
						loadPlayer(file);
					}
				}

				e.getDropTargetContext().dropComplete(true);
			} else {
				e.rejectDrop();
			}

		} catch (IOException ex) {
			e.rejectDrop();
			new ErrorDialog(JMP3SplitterFrame.this, ex, options)
					.setVisible(true);
			ex.printStackTrace();
		} catch (UnsupportedFlavorException ex) {
			e.rejectDrop();
			new ErrorDialog(JMP3SplitterFrame.this, ex, options)
					.setVisible(true);
			ex.printStackTrace();
		}

	}

	@Override
	public void dropActionChanged(DropTargetDragEvent arg0) {

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		if (mp3Player == null)
			return;
		try {
			if (e.getWheelRotation() == -1)
				if (options.getRoll().equals("s"))
					mp3Player.seekAhead5s();
				else
					mp3Player.seekAhead1min();
			else {
				if (options.getRoll().equals("s"))
					mp3Player.seekBack5s();
				else
					mp3Player.seekBack1min();
			}

		} catch (Exception ex) {
			new ErrorDialog(JMP3SplitterFrame.this, ex, options)
					.setVisible(true);
			ex.printStackTrace();
		}
	}

	private class OpenButtonListener implements ActionListener {

		// OpenButtonListener() {
		//
		// }

		@Override
		public void actionPerformed(ActionEvent arg0) {
			final Preferences prefs = Preferences
					.userNodeForPackage(getClass());

			String defaultValue = File.listRoots()[0].getPath();
			String propertyValue = prefs.get(OPEN_PREF, defaultValue); // "a string"
			// System.out.println(propertyValue);

			// TODO: CHANGE File.listRoots()[2]
			UIManager.put(
					"FileChooser.openDialogTitleText", getString("openDots")); //$NON-NLS-1$ //$NON-NLS-2$
			UIManager.put("FileChooser.cancelButtonText", getString("cancel")); //$NON-NLS-1$ //$NON-NLS-2$
			UIManager.put("FileChooser.openButtonText", getString("open")); //$NON-NLS-1$ //$NON-NLS-2$

			final JFileChooser fileChooser = new JFileChooser(new File(
					propertyValue));
			SwingUtilities.updateComponentTreeUI(fileChooser);
			fileChooser.removeChoosableFileFilter(fileChooser
					.getAcceptAllFileFilter());

			fileChooser
					.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

						public boolean accept(File f) {
							return f.isDirectory()
									|| f.getName().toLowerCase().endsWith(
											".mp3"); //$NON-NLS-1$
						}

						public String getDescription() {
							return "*.mp3"; //$NON-NLS-1$
						}
					});
			// fileChooser.set
			int returnVal = fileChooser.showOpenDialog(JMP3SplitterFrame.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File file = fileChooser.getSelectedFile();
				if (file != null) {
					loadPlayer(file);
					prefs.put(OPEN_PREF, file.getParent());
				}
			}
			progressSlider.requestFocusInWindow();
		}
	}

	private class ProgressSliderListener implements ChangeListener,
			MouseListener, KeyListener {

		@Override
		public void stateChanged(ChangeEvent arg0) {

			if (!canChangeSliderValue) {
				if (mp3Player == null)
					return;

				int val = progressSlider.getValue();
				if (mp3Player.info.hasAdditionalFrame)
					val -= 1;
				TimeData tempTime = new TimeData(Math
						.round(((float) val * mp3Player.info.msPerFrame)),
						actualTime.getMaxMillis());
				progressSlider.setToolTipText(tempTime.toStringShorter());
				// actualTime.setCurrentMillis();
				// posLabel.setText(actualTime.toStringShorter());
			}
			// System.out.println("stateChanged = " +
			// progressSlider.getValue());
			// if (mouseReleased) {
			// System.out.println("STATE CHANGED");
			// System.out.println("stateChanged = "
			// + progressSlider.getValue());
			// mp3Player.seek(progressSlider.getValue());
			// ///mouseReleased = false; // Niepotrzebne?
			// }
			// else
			// flag = true;

		}

		@Override
		public void keyPressed(KeyEvent e) {
			// System.out.println("Key Pressed");

			if (mp3Player == null)
				return;

			// System.out.println(KeyEvent.getModifiersExText(e.getModifiersEx()));

			int key = e.getKeyCode();
			int mod = e.getModifiers();
			try {
				if (ShortcutsEnum.ONE_MIN_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead1min();
				} else if (ShortcutsEnum.ONE_MIN_BACK.isEqual(key, mod)) {
					mp3Player.seekBack1min();
				} else if (ShortcutsEnum.FIVE_S_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead5s();
				} else if (ShortcutsEnum.FIVE_S_BACK.isEqual(key, mod)) {
					mp3Player.seekBack5s();
				} else if (ShortcutsEnum.MARK_BEGIN.isEqual(key, mod)) {
					markBeginButtonAction();
				} else if (ShortcutsEnum.MARK_END.isEqual(key, mod)) {
					markEndButtonAction();
				} else if (ShortcutsEnum.PLAY_PAUSE.isEqual(key, mod)) {
					if (!mp3Player.isPlaying()) {
						if (progressSlider.getValue() == progressSlider
								.getMaximum()) {
							mp3Player.stop();
						}
						mp3Player.play();
					} else {
						mp3Player.pause();
					}
				} else if (ShortcutsEnum.STOP.isEqual(key, mod)) {
					mp3Player.stop();
				}
			} catch (Exception ex) {
				new ErrorDialog(JMP3SplitterFrame.this, ex, options)
						.setVisible(true);
				ex.printStackTrace();
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			canChangeSliderValue = false;
			int val = progressSlider.getValue();
			if (mp3Player.info.hasAdditionalFrame)
				val -= 1;
			TimeData tempTime = new TimeData(Math
					.round(((float) val * mp3Player.info.msPerFrame)),
					actualTime.getMaxMillis());
			progressSlider.setToolTipText(tempTime.toStringShorter());
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			// System.out.println("MOUSE RELEASED");
			// System.out.println("getValue = " + progressSlider.getValue());
			try {
				mp3Player.seek(progressSlider.getValue());
			} catch (Exception e) {
				new ErrorDialog(JMP3SplitterFrame.this, e, options)
						.setVisible(true);
				e.printStackTrace();
			}
			// ////actualTime.setCurrentMillis(mp3Player.getNextTime());
			// System.out.println("ac = " + actualTime.getCurrentMillis());
			// ///posLabel.setText(actualTime.toStringShorter());
			canChangeSliderValue = true;

			// mp3Player.seek(progressSlider.getValue());

		}
	}

	private class EditButtonListener implements ActionListener {

		// private JMP3SplitterFrame parent;

		// EditButtonListener(JMP3SplitterFrame parent) {
		// this.parent = parent;
		// }

		@Override
		public void actionPerformed(ActionEvent e) {
			if (mp3Player != null)
				mp3Player.pause();
			try {
				// System.out.printf("begTime = (%d, %d)\nendTime = (%d, %d)\n"
				// + "begFrame = %d\nendFrame = %d\n", selBeginTime
				// .getCurrentMillis(), selBeginTime.getMaxMillis(),
				// selEndTime.getCurrentMillis(), selEndTime
				// .getMaxMillis(), beginFrame, endFrame);

				new EditDialog(JMP3SplitterFrame.this, mp3Info, selBeginTime,
						selEndTime, beginFrame, endFrame).setVisible(true);

				startButton.setText(selBeginTime.toStringShorter());
				endButton.setText(selEndTime.toStringShorter());

				int diff = selEndTime.getCurrentMillis()
						- selBeginTime.getCurrentMillis();
				selDurationTime = new TimeData(diff, durationTime
						.getCurrentMillis());

				selDurationLabel.setText(selDurationTime.toStringShorter());

				progressSlider.requestFocusInWindow();
			} catch (Exception exc) {
				new ErrorDialog(JMP3SplitterFrame.this, exc, options)
						.setVisible(true);
				exc.printStackTrace();
			}
			progressSlider.requestFocusInWindow();
		}
	}

	private class SaveButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (mp3Player != null)
					mp3Player.pause();
				SaveDialog sd = new SaveDialog(JMP3SplitterFrame.this, mp3Info,
						beginFrame, endFrame, tagData);
				sd.setTagData(tagData);
				sd.setVisible(true);
				tagData = sd.getTagData();
				progressSlider.requestFocusInWindow();
			} catch (Exception ex) {
				new ErrorDialog(JMP3SplitterFrame.this, ex, options)
						.setVisible(true);
				ex.printStackTrace();
			}
			progressSlider.requestFocusInWindow();
		}

	}

	private class MarkBeginButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			markBeginButtonAction();
		}

	}

	private class MarkEndButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			markEndButtonAction();
		}
	}

	private class PlayerStateListener implements PlaybackListener,
			mp3.ProgressMonitor {

		public void playbackStarted(MP3Player player) {
			// System.out.println("playback started :P");
			playButton.setEnabled(false);
			pauseButton.setEnabled(true);
			// stopButton.setEnabled(true);
		}

		public void playbackPaused(MP3Player player) {
			// System.out.println("Playback paused :P");
			playButton.setEnabled(true);
			pauseButton.setEnabled(false);

			// stopButton.setEnabled(false);
		}

		public void playbackStoped(MP3Player player) {
			// System.out.println("playback stoped :P");
			actualTime.setCurrentMillis(0);
			progressSlider.setValue(minimalSliderValue);
			posLabel.setText(actualTime.toStringShorter());
			pauseButton.setEnabled(false);
			playButton.setEnabled(true);
			stopButton.setEnabled(false);
		}

		public void playbackFinished(MP3Player player) {
			mp3Player.pause();
		}

		public void playbackTimeChanged(MP3Player player) {
			// if(!canChangeValue)

			actualTime.setCurrentMillis(mp3Player.getCurrMillis());// TODO:
			// zamienic
			// getCurrentTime z metody na pole?
			posLabel.setText(actualTime.toStringShorter());
			if (canChangeSliderValue) {
				progressSlider.setValue(mp3Player.getCurrFrame());
			}
		}

		@Override
		public void progressMade(int percent) {
			fileNameLabel.setText(getString("reading") + percent + "%"); //$NON-NLS-1$ //$NON-NLS-2$

		}
	}

	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new JMP3SplitterFrame().setVisible(true);

			}
		});
	}
}
