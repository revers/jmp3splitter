package gui;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * 
 * Action class to provide an action to Cut from a component.
 * 
 * @author steve.webb
 */
class CutAction extends AbstractAction {
	/**
	 * Icon to displayed against this action.
	 */
	// static final private ImageIcon icon =
	// new
	// ImageIcon(ClassLoader.getSystemResource("toolbarButtonGraphics/general/Cut16.gif"));
	/**
	 * The component the action is associated with.
	 */
	JTextComponent comp;

	/**
	 * Default constructor.
	 * 
	 * @param comp
	 *            The component the action is associated with.
	 */
	public CutAction(JTextComponent comp, String txt) {
		super(txt /* ,icon */);
		this.comp = comp;
	}

	/**
	 * Action has been performed on the component.
	 * 
	 * @param e
	 *            ignored
	 */
	public void actionPerformed(ActionEvent e) {
		comp.cut();
	}

	/**
	 * Checks if the action can be performed.
	 * 
	 * @return True if the action is allowed
	 */
	public boolean isEnabled() {
		return comp.isEditable() && comp.isEnabled()
				&& comp.getSelectedText() != null;
	}
}