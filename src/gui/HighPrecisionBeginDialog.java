package gui;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.CENTER;
import static java.awt.GridBagConstraints.EAST;
import static java.awt.GridBagConstraints.HORIZONTAL;
import static java.awt.GridBagConstraints.NONE;
import static java.awt.GridBagConstraints.WEST;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javazoom.jl.decoder.JavaLayerException;
import mp3.MP3Player;
import mp3.PlaybackListener;
import mp3.SoundGraph;
import utils.OptionValues;
import utils.ShortcutsEnum;
import utils.SwingUtils;
import utils.TimeData;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

public class HighPrecisionBeginDialog extends JDialog implements
		ComponentListener, MouseWheelListener {

	private static final String EMPTY_TIME = "00:00.0"; //$NON-NLS-1$
	private static final int TIME_SPINNER_DEFAULT_VALUE = 300;

	private JButton okJB;
	private JButton cancelJB;
	private JButton playJB;
	private JButton pauseJB;
	private JButton beginJB;
	private JButton markBeginJB;
	private JButton restoreBeginJB;
	private JButton findJB;

	private TimeSpinner distSpinner;
	private JComboBox rangeCB;

	private JLabel startJL;
	private JLabel endJL;
	private JLabel posJL;
	private JLabel beginTimeJL;
	private JLabel amplitudeJL;
	private JLabel rangeTextJL;
	private JLabel distTextJL;
	private JLabel posTextJL;
	private JLabel beginTextJL;
	private JLabel amplitudeTextJL;

	private MySlider progressSlider;
	private GraphPanel graphPanel;
	private MP3Player mp3Player;
	private EditDialog editDialog;

	private int minimalSliderValue;
	private boolean canChangeSliderValue = true;
	private TimeData actualTime;
	// private TimeData durTime;
	private TimeData beginTime;

	private int restoreBeginFrame;
	private int beginFrame;
	private int endFrame;
	private int frames;
	private int markBeginFrame;
	private ResourceBundle resourceBundle;

	public HighPrecisionBeginDialog(EditDialog editDialog) {
		super(editDialog, true);
		this.editDialog = editDialog;
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = new Dimension(screenDim.width - 100, 332);
		setSize(size);
		int x = (screenDim.width - size.width) / 2;
		int y = (screenDim.height - size.height) / 2;
		setLocation(x, y);
		try {
			UIManager
					.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
			e.printStackTrace();
			new ErrorDialog(this, e, editDialog.jmp3SplitterFrame.options)
					.setVisible(true);
		}
		SwingUtilities.updateComponentTreeUI(this);

		if (editDialog.jmp3SplitterFrame.options.getLang().equals("en")) {//$NON-NLS-1$  
			resourceBundle = ResourceBundle.getBundle(
					"HighPrecisionBeginDialog", //$NON-NLS-1$
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle.getBundle(
					"HighPrecisionBeginDialog", //$NON-NLS-1$
					OptionValues.LOCALE_PL);
		}

		initComponents();
		addMouseWheelListener(this);
	}

	private void initComponents() {
		progressSlider = new MySlider(0, 100, 0);

		playJB = new JButton(">"); //$NON-NLS-1$
		pauseJB = new JButton("||"); //$NON-NLS-1$
		beginJB = new JButton("|<"); //$NON-NLS-1$
		markBeginJB = new JButton(getString("mark")); //$NON-NLS-1$
		restoreBeginJB = new JButton(getString("restore")); //$NON-NLS-1$

		startJL = new JLabel(EMPTY_TIME);
		endJL = new JLabel(EMPTY_TIME);
		posJL = new JLabel(EMPTY_TIME);
		beginTimeJL = new JLabel(EMPTY_TIME);
		amplitudeJL = new JLabel("0");
		distTextJL = new JLabel(getString("distance")); //$NON-NLS-1$
		rangeTextJL = new JLabel(getString("range")); //$NON-NLS-1$
		posTextJL = new JLabel(getString("position")); //$NON-NLS-1$
		beginTextJL = new JLabel(getString("begin")); //$NON-NLS-1$
		amplitudeTextJL = new JLabel(getString("amplitude")); //$NON-NLS-1$

		okJB = new JButton("OK"); //$NON-NLS-1$
		cancelJB = new JButton(getString("cancel")); //$NON-NLS-1$

		findJB = new JButton(getString("find")); //$NON-NLS-1$
		TimeData td = new TimeData(0, 10000);
		distSpinner = new TimeSpinner(td);
		rangeCB = new JComboBox();

		graphPanel = new GraphPanel(getString("loading")); //$NON-NLS-1$
		graphPanel.addComponentListener(this);

		rangeCB.setModel(new DefaultComboBoxModel(new String[] {
				getString("radius"), //$NON-NLS-1$
				getString("before"), getString("after") })); //$NON-NLS-1$ //$NON-NLS-2$

		setAllEnabled(false);
		pauseJB.setEnabled(false);

		setTitle(getString("title")); //$NON-NLS-1$
		setResizable(false);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLayout(new BorderLayout());

		JPanel rootPanel = new JPanel();
		rootPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		rootPanel.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();

		// --------------------------------------------------
		JPanel playerPanel = new JPanel();
		playerPanel.setBorder(new CompoundBorder(new TitledBorder(""), //$NON-NLS-1$
				new EmptyBorder(5, 5, 5, 5)));
		playerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c3 = new GridBagConstraints();
		c3.fill = BOTH;
		c3.weightx = 1.0;
		c3.weighty = 1.0;
		c3.gridwidth = 2;
		c3.insets = new Insets(0, 0, 7, 0);
		playerPanel.add(graphPanel, c3);

		c3.gridwidth = 1;
		c3.gridy = 1;
		c3.fill = NONE;

		c3.weighty = 0.0;
		c3.anchor = WEST;
		playerPanel.add(startJL, c3);

		c3.gridx = 1;
		c3.anchor = EAST;
		playerPanel.add(endJL, c3);

		c3.fill = HORIZONTAL;
		c3.gridy = 2;
		c3.gridx = 0;
		c3.gridwidth = 2;
		playerPanel.add(progressSlider, c3);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		buttonsPanel.add(playJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(pauseJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(beginJB);
		buttonsPanel.add(Box.createHorizontalStrut(20));
		buttonsPanel.add(markBeginJB);
		buttonsPanel.add(Box.createHorizontalGlue());
		buttonsPanel.add(restoreBeginJB);

		c3.insets = new Insets(0, 0, 0, 0);
		c3.gridy = 3;
		playerPanel.add(buttonsPanel, c3);

		// --------------------------------------------------
		JPanel findPanel = new JPanel();
		findPanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("findAuto")), new EmptyBorder(5, 5, 5, 5))); //$NON-NLS-1$
		findPanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = NONE;
		c1.anchor = WEST;
		c1.insets = new Insets(0, 0, 7, 7);
		findPanel.add(distTextJL, c1);
		c1.gridy = 1;
		findPanel.add(rangeTextJL, c1);

		c1.gridx = 1;
		c1.insets = new Insets(0, 0, 7, 0);
		c1.anchor = CENTER;
		c1.fill = HORIZONTAL;
		findPanel.add(rangeCB, c1);
		c1.gridy = 0;
		findPanel.add(distSpinner, c1);

		c1.gridy = 2;
		c1.gridx = 0;
		c1.gridwidth = 2;
		c1.insets = new Insets(0, 0, 0, 0);
		findPanel.add(findJB, c1);
		// --------------------------------------------------
		JPanel timePanel = new JPanel();
		timePanel.setBorder(new CompoundBorder(new TitledBorder(
				getString("time")), //$NON-NLS-1$
				new EmptyBorder(5, 5, 5, 5)));
		timePanel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();

		c2.anchor = WEST;
		c2.fill = NONE;
		c2.weightx = 1.0;
		c2.insets = new Insets(0, 0, 5, 5);

		timePanel.add(posTextJL, c2);

		c2.gridy = 1;
		timePanel.add(beginTextJL, c2);

		c2.gridy = 2;
		c2.insets = new Insets(0, 0, 0, 5);
		timePanel.add(amplitudeTextJL, c2);

		c2.gridx = 1;
		c2.anchor = EAST;
		c2.insets = new Insets(0, 0, 0, 0);
		timePanel.add(amplitudeJL, c2);

		c2.gridy = 1;
		c2.insets = new Insets(0, 0, 5, 0);
		timePanel.add(beginTimeJL, c2);

		c2.gridy = 0;
		timePanel.add(posJL, c2);
		// --------------------------------------------------
		gbc.fill = BOTH;
		gbc.gridheight = 4;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		rootPanel.add(playerPanel, gbc);

		gbc.gridheight = 1;
		gbc.insets = new Insets(0, 7, 7, 0);
		gbc.gridx = 1;

		gbc.weightx = 0.0;
		rootPanel.add(findPanel, gbc);

		gbc.gridy = 1;
		rootPanel.add(timePanel, gbc);

		gbc.gridy = 2;
		gbc.weighty = 0.0;
		rootPanel.add(okJB, gbc);

		gbc.insets = new Insets(0, 7, 0, 0);
		gbc.gridy = 3;
		rootPanel.add(cancelJB, gbc);
		add(rootPanel, BorderLayout.CENTER);

		cancelJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				cancelAction();
			}

		});

		playJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					if (progressSlider.getValue() == progressSlider
							.getMaximum())
						try {
							mp3Player.stop();
						} catch (Exception e) {
							e.printStackTrace();
						}
					mp3Player.play();
				}
				progressSlider.requestFocusInWindow();
			}

		});

		pauseJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					mp3Player.pause();
				}
				progressSlider.requestFocusInWindow();

			}

		});

		beginJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				beginAction();
			}
		});

		markBeginJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				markBeginAction();
			}
		});

		okJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				okAction();
			}
		});

		restoreBeginJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				restoreBeginAction();
			}
		});

		findJB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				findAction();
			}
		});
	}

	private void findAction() {
		boolean before = false;
		boolean after = false;

		if (rangeCB.getSelectedIndex() == 0) {
			before = true;
			after = true;
		} else if (rangeCB.getSelectedIndex() == 1)
			before = true;
		else
			after = true;

		int startIndex = markBeginFrame - beginFrame;
		int searchFrames = Math.round((float) distSpinner.getTimeData()
				.getCurrentMillis()
				/ editDialog.mp3Info.msPerFrame);
		float[] vals = graphPanel.getValues();

		int beforeMinIndex = Integer.MAX_VALUE;
		if (before) {

			int stopIndex = startIndex - searchFrames;
			if (stopIndex < 0)
				stopIndex = 0;
			beforeMinIndex = startIndex;
			for (int i = startIndex - 1; i >= stopIndex; i--) {
				if (vals[i] < vals[beforeMinIndex])
					beforeMinIndex = i;
			}
		}

		int afterMinIndex = Integer.MAX_VALUE;
		if (after) {
			int stopIndex = startIndex + searchFrames;
			if (stopIndex >= vals.length)
				stopIndex = vals.length - 1;

			afterMinIndex = startIndex;
			for (int i = startIndex + 1; i < stopIndex; i++) {
				if (vals[i] < vals[afterMinIndex])
					afterMinIndex = i;
			}
		}

		if (before && after) {
			if (vals[afterMinIndex] < vals[beforeMinIndex])
				setBeginFrame(afterMinIndex + beginFrame);
			else
				setBeginFrame(beforeMinIndex + beginFrame);
		} else if (before) {
			setBeginFrame(beforeMinIndex + beginFrame);
		} else
			setBeginFrame(afterMinIndex + beginFrame);
	}

	private void beginAction() {
		try {
			mp3Player.seek(markBeginFrame);
		} catch (Exception e) {
			e.printStackTrace();
			new ErrorDialog(HighPrecisionBeginDialog.this, e,
					editDialog.jmp3SplitterFrame.options).setVisible(true);
		}
	}

	private void restoreBeginAction() {
		setBeginFrame(restoreBeginFrame);
	}

	private void markBeginAction() {
		setBeginFrame(mp3Player.getCurrFrame());
	}

	private void okAction() {
		editDialog.setBeginFrame(markBeginFrame);
		cancelAction();
	}

	private void cancelAction() {
		// System.out.println(HighPrecisionBeginDialog.this.getSize());
		if (mp3Player != null) {
			try {
				mp3Player.stop();
				mp3Player.close();
			} catch (Exception ex) {
				ex.printStackTrace();
				new ErrorDialog(HighPrecisionBeginDialog.this, ex,
						editDialog.jmp3SplitterFrame.options).setVisible(true);
			}
		}
		dispose();
	}

	private void setBeginFrame(int frame) {
		markBeginFrame = frame;
		graphPanel.setMarkPos(markBeginFrame - beginFrame);
		int f = markBeginFrame;
		if (editDialog.mp3Info.hasAdditionalFrame && f > 0)
			f--;

		beginTime.setCurrentMillis(Math.round((float) f
				* editDialog.mp3Info.msPerFrame));
		beginTimeJL.setText(beginTime.toString());
	}

	private void setAllEnabled(boolean enabled) {
		okJB.setEnabled(enabled);
		cancelJB.setEnabled(enabled);
		progressSlider.setEnabled(enabled);
		playJB.setEnabled(enabled);
		beginJB.setEnabled(enabled);
		markBeginJB.setEnabled(enabled);
		restoreBeginJB.setEnabled(enabled);
		distSpinner.setEnabled(enabled);
		rangeCB.setEnabled(enabled);
		startJL.setEnabled(enabled);
		endJL.setEnabled(enabled);
		posJL.setEnabled(enabled);
		beginTimeJL.setEnabled(enabled);
		amplitudeJL.setEnabled(enabled);
		rangeTextJL.setEnabled(enabled);
		distTextJL.setEnabled(enabled);
		posTextJL.setEnabled(enabled);
		beginTextJL.setEnabled(enabled);
		amplitudeTextJL.setEnabled(enabled);
		findJB.setEnabled(enabled);

	}

	private void calcBeginFrame(int maxMillis, int begFrame) {
		if (begFrame < 0) {
			if (editDialog.mp3Info.hasAdditionalFrame)
				beginFrame = 1;
			else
				beginFrame = 0;
			startJL.setText(new TimeData(0, maxMillis).toString());
		} else if (begFrame == 0 && editDialog.mp3Info.hasAdditionalFrame) {
			beginFrame = 1;
			startJL.setText(new TimeData(0, maxMillis).toString());
		} else {
			beginFrame = begFrame;
			int millis = 0;
			if (editDialog.mp3Info.hasAdditionalFrame)
				millis = Math.round(editDialog.mp3Info.msPerFrame
						* (float) (beginFrame - 1));
			else
				millis = Math.round(editDialog.mp3Info.msPerFrame
						* (float) (beginFrame));
			startJL.setText(new TimeData(millis, maxMillis).toString());
		}
	}

	private void calcFrames(int maxMillis, Dimension graphDim) {

		boolean canExtendBegin = true;
		int halfFrames = (graphDim.width - 1) / 2;
		int begFrame = markBeginFrame - halfFrames;

		calcBeginFrame(maxMillis, begFrame);
		if (beginFrame != begFrame) {
			canExtendBegin = false;
			halfFrames -= begFrame;
		}

		endFrame = markBeginFrame + halfFrames;
		if (endFrame > (editDialog.mp3Info.frames - 1)) {
			if (canExtendBegin) {
				begFrame = beginFrame
						- (endFrame - (editDialog.mp3Info.frames - 1));
				calcBeginFrame(maxMillis, begFrame);
			}
			endFrame = (editDialog.mp3Info.frames - 1);

		}
		if (editDialog.mp3Info.hasAdditionalFrame) {
			int millis = Math.round((float) (endFrame - 1)
					* editDialog.mp3Info.msPerFrame);
			endJL.setText(new TimeData(millis, maxMillis).toString());
		} else {
			int millis = Math.round((float) endFrame
					* editDialog.mp3Info.msPerFrame);
			endJL.setText(new TimeData(millis, maxMillis).toString());
		}

		frames = endFrame - beginFrame;
	}

	private void setUpFindTimeSpinner() {
		int b = markBeginFrame - beginFrame;
		int e = endFrame - markBeginFrame;
		int greater = b;
		if (e > b)
			greater = e;

		int millis = Math
				.round(editDialog.mp3Info.msPerFrame * (float) greater);
		int initVal = TIME_SPINNER_DEFAULT_VALUE;
		if (millis < initVal)
			initVal = millis;
		TimeData td = new TimeData(initVal, millis);
		distSpinner.setTimeData(td);
	}

	private void initGraphAndPlayer() {
		markBeginFrame = editDialog.beginFrame;
		restoreBeginFrame = editDialog.beginFrame;
		int maxMillis = editDialog.beginTime.getMaxMillis();
		Dimension graphDim = graphPanel.getSize();

		calcFrames(maxMillis, graphDim);
		//System.out.printf("beginFrame = %d\n", beginFrame);
		setUpFindTimeSpinner();

		float[] values = null;
		float greatestValue = -1.0f;
		try {
			mp3Player = new MP3Player(editDialog.mp3Info, null, beginFrame,
					endFrame);

			SoundGraph graph = new SoundGraph(editDialog.mp3Info, beginFrame,
					frames + 1);
			values = graph.getValues();
			greatestValue = graph.getGreatestValue();
			mp3Player.seek(markBeginFrame);
		} catch (Exception e) {
			e.printStackTrace();
			new ErrorDialog(HighPrecisionBeginDialog.this, e,
					editDialog.jmp3SplitterFrame.options).setVisible(true);
			return;
		}
		progressSlider.setMinimum(beginFrame);
		progressSlider.setMaximum(endFrame);
		progressSlider.setValue(mp3Player.getCurrFrame());
		graphPanel.setValues(values, greatestValue);
		graphPanel.setCursorPos(mp3Player.getCurrFrame() - beginFrame);
		graphPanel.setMarkPos(mp3Player.getCurrFrame() - beginFrame);

		// int dur = Math.round(frames * editDialog.mp3Info.msPerFrame);
		// durTime = new TimeData(dur, maxMillis);
		amplitudeJL.setText(String.format(Locale.ENGLISH, "%.2f", graphPanel //$NON-NLS-1$
				.getAmplitude(mp3Player.getCurrFrame() - beginFrame)));
		actualTime = new TimeData(mp3Player.getCurrMillis(), maxMillis);
		progressSlider.setToolTipText(actualTime.toString());
		posJL.setText(actualTime.toString());
		beginTime = new TimeData(editDialog.beginTime.getCurrentMillis(),
				maxMillis);
		beginTimeJL.setText(beginTime.toString());

		mp3Player.setHighPrecisionTimer(true);
		ProgressSliderListener progList = new ProgressSliderListener();
		progressSlider.addMouseListener(progList);
		progressSlider.addKeyListener(progList);
		progressSlider.addChangeListener(progList);
		PlayerStateListener psl = new PlayerStateListener();
		mp3Player.setListener(psl);

		// graphPanel.removeLoadingLabel();
		graphPanel.setDrawGraph(true);

		setAllEnabled(true);
		// pauseJB.setEnabled(false);

		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				cancelAction();
			}
		});
	}

	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		new Thread() {
			@Override
			public void run() {
				initGraphAndPlayer();
				progressSlider.requestFocusInWindow();
			}
		}.start();
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		progressSlider.requestFocusInWindow();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		if (mp3Player == null)
			return;
		try {
			if (e.getWheelRotation() == -1)
				mp3Player.seekAhead5s();
			else {
				mp3Player.seekBack5s();
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	private class PlayerStateListener implements PlaybackListener {

		public void playbackStarted(MP3Player player) {
			// System.out.println("playback started :P");
			playJB.setEnabled(false);
			pauseJB.setEnabled(true);
			// stopJB.setEnabled(true);
		}

		public void playbackPaused(MP3Player player) {
			// System.out.println("Playback paused :P");
			playJB.setEnabled(true);
			pauseJB.setEnabled(false);

			// stopButton.setEnabled(false);
		}

		public void playbackStoped(MP3Player player) {
			// System.out.println("playback stoped :P");
			actualTime.setCurrentMillis(0);
			progressSlider.setValue(minimalSliderValue);
			posJL.setText(actualTime.toString());
			pauseJB.setEnabled(false);
			playJB.setEnabled(true);
			// stopJB.setEnabled(false);
		}

		public void playbackFinished(MP3Player player) {
			mp3Player.pause();
			beginAction();
		}

		public void playbackTimeChanged(MP3Player player) {
			// if(!canChangeValue)

			actualTime.setCurrentMillis(mp3Player.getCurrMillis());// TODO:
			// zamienic
			// getCurrentTime z metody na pole?
			posJL.setText(actualTime.toString());

			if (canChangeSliderValue) {
				progressSlider.setValue(mp3Player.getCurrFrame());
				//graphPanel.setCursorPos(mp3Player.getCurrFrame() - beginFrame);
				graphPanel.setCursorPos(mp3Player.getCurrFrame() - beginFrame, (int)(mp3Player.info.msPerFrame));
				
				amplitudeJL.setText(String.format(Locale.ENGLISH,
						"%.2f", graphPanel //$NON-NLS-1$
								.getAmplitude(mp3Player.getCurrFrame()
										- beginFrame)));

			}
		}
	}

	private class ProgressSliderListener implements ChangeListener,
			MouseListener, KeyListener {

		@Override
		public void stateChanged(ChangeEvent arg0) {

			if (!canChangeSliderValue) {
				if (mp3Player == null)
					return;

				int val = progressSlider.getValue();
				
//				System.out.printf(
//						"beginFrame = %d; val = %d; minimumVal = %d\n",
//						beginFrame, val, progressSlider.getMinimum());
				amplitudeJL.setText(String.format(Locale.ENGLISH,
						"%.2f", graphPanel //$NON-NLS-1$
								.getAmplitude(val - beginFrame)));

				graphPanel.setCursorPos(val - beginFrame);
				
				if (mp3Player.info.hasAdditionalFrame)
					val -= 1;
				TimeData tempTime = new TimeData(Math
						.round(((float) val * mp3Player.info.msPerFrame)),
						actualTime.getMaxMillis());
				progressSlider.setToolTipText(tempTime.toString());
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// System.out.println("Key Pressed");

			if (mp3Player == null)
				return;

			// System.out.println(KeyEvent.getModifiersExText(e.getModifiersEx()));

			int key = e.getKeyCode();
			int mod = e.getModifiers();
			try {

				if (ShortcutsEnum.FIVE_S_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead5s();
				} else if (ShortcutsEnum.FIVE_S_BACK.isEqual(key, mod)) {
					mp3Player.seekBack5s();
				} else if (ShortcutsEnum.MARK_BEGIN.isEqual(key, mod)) {
					markBeginAction();
				} else if (ShortcutsEnum.PLAY_PAUSE.isEqual(key, mod)) {
					if (!mp3Player.isPlaying()) {
						if (progressSlider.getValue() == progressSlider
								.getMaximum()) {
							mp3Player.stop();
						}
						mp3Player.play();
					} else {
						mp3Player.pause();
					}
				} else if (ShortcutsEnum.STOP.isEqual(key, mod)) {
					mp3Player.pause();
					beginAction();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			canChangeSliderValue = false;
			int val = progressSlider.getValue();
			
			amplitudeJL.setText(String.format(Locale.ENGLISH,
					"%.2f", graphPanel //$NON-NLS-1$
							.getAmplitude(val - beginFrame)));

			graphPanel.setCursorPos(val - beginFrame);
			
			if (mp3Player.info.hasAdditionalFrame)
				val -= 1;
			TimeData tempTime = new TimeData(Math
					.round(((float) val * mp3Player.info.msPerFrame)),
					actualTime.getMaxMillis());
			progressSlider.setToolTipText(tempTime.toString());
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			// System.out.println("MOUSE RELEASED");
			// System.out.println("getValue = " + progressSlider.getValue());
			try {
				mp3Player.seek(progressSlider.getValue());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// ////actualTime.setCurrentMillis(mp3Player.getNextTime());
			// System.out.println("ac = " + actualTime.getCurrentMillis());
			// ///posLabel.setText(actualTime.toString());
			canChangeSliderValue = true;

			// mp3Player.seek(progressSlider.getValue());

		}
	}

	// public static void main(String[] args) {
	// SwingUtilities.invokeLater(new Runnable() {
	//
	// @Override
	// public void run() {
	// HighPrecisionBeginDialog hpbd = new HighPrecisionBeginDialog();
	// hpbd.setVisible(true);
	//
	// }
	//
	// });
	// }
}
