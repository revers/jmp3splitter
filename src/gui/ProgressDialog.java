package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import utils.OptionValues;

import java.awt.*;
import java.awt.event.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static java.awt.GridBagConstraints.*;

public class ProgressDialog extends JDialog implements ComponentListener {
	private JLabel label1;
	private JLabel fileJL;
	private JProgressBar progressBar;
	private Dimension baseDim;
	private JButton okButton;
	private ResourceBundle resourceBundle;
	private JMP3SplitterFrame owner;

	public ProgressDialog(JMP3SplitterFrame owner, String fileName) {
		super(owner, true); //$NON-NLS-1$
		
		this.owner = owner;
		
		if (owner.options.getLang().equals("en")) { //$NON-NLS-1$ 
			resourceBundle = ResourceBundle.getBundle("ProgressDialog",
					Locale.ENGLISH);
		} else {
			resourceBundle = ResourceBundle
					.getBundle("ProgressDialog", OptionValues.LOCALE_PL);
		}
		
		setTitle(getString("saving"));
		initComponents();
		fileJL.setText(fileName);
		addComponentListener(this);
		
		

		try {
			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
		} catch (Exception e) {
			new ErrorDialog(this, e, owner.options).setVisible(true);
			e.printStackTrace();
			// System.err.println("Nie moge zmienic LookAndFeel");
		}
		SwingUtilities.updateComponentTreeUI(this);
	}
	
	private String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private void setLocationOnCenter() {
		Point p = owner.getLocation();
		Dimension dim = owner.getSize();
		int x = p.x + (dim.width / 2) - (getWidth() / 2);
		int y = p.y + (dim.height / 2) - (getHeight() / 2);
		setLocation(x, y);
	}
	
	public void setProgressBarValue(int val) {
		//System.out.println(val);
		progressBar.setValue(val);
		if(val == 100) {
			setTitle(getString("savingOK")); //$NON-NLS-1$
			okButton.setEnabled(true);
			okButton.requestFocusInWindow();
		}
	}
	
	private void initComponents() {
		label1 = new JLabel(getString("savingFile")); //$NON-NLS-1$
		fileJL = new JLabel();
		progressBar = new JProgressBar(0, 100);
		baseDim = new Dimension(200, 144);
		okButton = new JButton("OK"); //$NON-NLS-1$
		
		
		okButton.setEnabled(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		//setResizable(false);
		progressBar.setStringPainted(true);
		setLayout(new BorderLayout());
		setSize(baseDim);
		setLocationOnCenter();
		JPanel mainPanel = new JPanel();
		add(mainPanel, BorderLayout.CENTER);
		mainPanel.setBorder(new EmptyBorder(7, 7, 7, 7));
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = WEST;
		c.weightx = 1.0;
		c.fill = HORIZONTAL;
		c.insets = new Insets(0, 0, 5, 0);
		mainPanel.add(label1, c);
		
		c.gridy = 1;
		mainPanel.add(fileJL, c);
		
		c.gridy = 2;
		mainPanel.add(progressBar, c);
		
		c.anchor = CENTER;
		c.gridy = 3;
		c.fill = NONE;
		c.insets = new Insets(3, 0, 0, 0);
		Dimension dim = new Dimension(100, 23);
		okButton.setPreferredSize(dim);
		okButton.setSize(dim);
		okButton.setMinimumSize(dim);
		mainPanel.add(okButton, c);
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
			//	System.out.println(getSize());
				dispose();
			}
		});
		
	}
//	public static void main(String[] args) {
//		javax.swing.SwingUtilities.invokeLater(new Runnable() {
//
//			@Override
//			public void run() {
//				JFrame frame = new JFrame();
//				frame.setBounds(200, 200, 500, 500);
//				new ProgressDialog(frame, "[Cuted from Pink Floyd - Comfortably Numb [DZ Vs Rastatronics RMX] 4clubbers.Pl[by Ov]] - Track #2.mp3").setVisible(true);
//			}
//			
//		});
//
//	}


	@Override
	public void componentHidden(ComponentEvent arg0) {
		
	}


	@Override
	public void componentMoved(ComponentEvent arg0) {
		
	}


	@Override
	public void componentResized(ComponentEvent arg0) {
		
	}


	@Override
	public void componentShown(ComponentEvent arg0) {

			int width = baseDim.width;
			int height = baseDim.height;

			Graphics2D g = (Graphics2D) fileJL.getGraphics();
			FontRenderContext frc = g.getFontRenderContext();
			TextLayout tl = new TextLayout(fileJL.getText(), fileJL.getFont(), frc);
			int textWidth = (int) Math.round(tl.getBounds().getWidth());
			if (textWidth > baseDim.width) {
				width += textWidth - 170;
			}

			setSize(width, height);
			setLocationOnCenter();

	}

}
