package gui;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;

import javax.swing.JTextArea;

public class ExtendedTextArea extends JTextArea {

	private String copyStr = "Copy";
	private String pasteStr = "Paste";
	private String cutStr = "Cut";
	private String selectAllStr = "Select all";
	private String deleteStr = "Delete";

	public ExtendedTextArea() {
		super();
		initialize();
	}

	/**
	 * Constructs a new ExtendedTextArea that uses the given text storage model
	 * and the given number of columns. This is the constructor through which
	 * the other constructors feed. If the document is null, a default model is
	 * created.
	 * 
	 * @param doc
	 *            the text storage to use; if this is null, a default will be
	 *            provided by calling the createDefaultModel method
	 * @param text
	 *            the initial string to display, or null
	 * @param columns
	 *            the number of columns to use to calculate the preferred width
	 *            >= 0; if columns is set to zero, the preferred width will be
	 *            whatever naturally results from the component implementation.
	 */
	public ExtendedTextArea(Document doc, String text, int rows, int columns) {
		super(doc, text, rows, columns);
		initialize();
	}

	/**
	 * Constructs a new empty ExtendedTextArea with the specified number of
	 * columns. A default model is created and the initial string is set to
	 * null.
	 * 
	 * @param columns
	 *            the number of columns to use to calculate the preferred width;
	 *            if columns is set to zero, the preferred width will be
	 *            whatever naturally results from the component implementation.
	 */
	public ExtendedTextArea(int rows, int columns) {
		super(rows, columns);
		initialize();
	}

	/**
	 * Constructs a new ExtendedTextArea initialized with the specified text. A
	 * default model is created and the number of columns is 0.
	 * 
	 * @param text
	 *            the text to be displayed, or null
	 */
	public ExtendedTextArea(String text) {
		super(text);
		initialize();
	}

	/**
	 * Constructs a new ExtendedTextArea initialized with the specified text and
	 * columns. A default model is created.
	 * 
	 * @param text
	 *            the text to be displayed, or null
	 * @param columns
	 *            the number of columns to use to calculate the preferred width;
	 *            if columns is set to zero, the preferred width will be
	 *            whatever naturally results from the component implementation
	 */
	public ExtendedTextArea(String text, int rows, int columns) {
		super(text, rows, columns);
		initialize();
	}

	/**
	 * Initialize's specific behaviour for this class.
	 */
	protected void initialize() {
		// Setup the popup menu

		// Add the mouse listener
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				requestFocus();
				dealWithMousePress(evt);
			}
		});
	}

	/**
	 * The mouse has been pressed over this text field. Popup the cut/paste
	 * menu.
	 * 
	 * @param evt
	 *            mouse event
	 */
	protected void dealWithMousePress(MouseEvent evt) {
		// Only interested in the right button
		if (SwingUtilities.isRightMouseButton(evt)) {
			// if(MenuSelectionManager.defaultManager().getSelectedPath().length>0)
			// return;

			JPopupMenu menu = new JPopupMenu();
			menu.add(new CutAction(this, cutStr));
			menu.add(new PasteAction(this, pasteStr));
			menu.add(new CopyAction(this, copyStr));
			menu.add(new DeleteAction(this, deleteStr));
			menu.addSeparator();
			menu.add(new SelectAllAction(this, selectAllStr));

			// Display the menu
			Point pt = SwingUtilities.convertPoint(evt.getComponent(), evt
					.getPoint(), this);
			menu.show(this, pt.x, pt.y);
		}
	}
	
	public String getCopyStr() {
		return copyStr;
	}

	public void setCopyStr(String copyStr) {
		this.copyStr = copyStr;
	}

	public String getPasteStr() {
		return pasteStr;
	}

	public void setPasteStr(String pasteStr) {
		this.pasteStr = pasteStr;
	}

	public String getCutStr() {
		return cutStr;
	}

	public void setCutStr(String cutStr) {
		this.cutStr = cutStr;
	}

	public String getSelectAllStr() {
		return selectAllStr;
	}

	public void setSelectAllStr(String selectAllStr) {
		this.selectAllStr = selectAllStr;
	}

	public String getDeleteStr() {
		return deleteStr;
	}

	public void setDeleteStr(String deleteStr) {
		this.deleteStr = deleteStr;
	}

}
