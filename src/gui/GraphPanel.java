package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import static java.awt.GridBagConstraints.*;

public class GraphPanel extends JPanel implements ComponentListener {

	private float[] values;
	private boolean drawGraph;
	// private JProgressBar progressBar;
	private JLabel loadingJL;
	private int cursorPos;
	private int markPos;
	private Dimension dim;
	private int y;
	private float greatestValue;

	public GraphPanel(String loadingText) {
		// progressBar = new JProgressBar(0, 100);
		// progressBar.setStringPainted(true);
		// setBackground(Color.white);
		loadingJL = new JLabel(loadingText);
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = NONE;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = CENTER;
		add(loadingJL, c);
		addComponentListener(this);
	}

	public float[] getValues() {
		return values;
	}

	public void setValues(float[] values, float greatestValue) {
		this.values = values;
		this.greatestValue = greatestValue;
	}

	public boolean isDrawGraph() {
		return drawGraph;
	}

	public void setDrawGraph(boolean drawGraph) {
		this.drawGraph = drawGraph;
		if (drawGraph == true)
			remove(loadingJL);
		repaint();
	}

	private void drawDashedLine(Graphics2D g2, int x, int y, int height) {
		int pos = y;
		int step = 3;
		while (y + height >= pos) {
			g2.drawLine(x, pos, x, pos += step);
			pos += step;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (!drawGraph)
			return;

		float scaleFactor = (float) (dim.height - 20) / greatestValue;

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.RED);
		// g2.drawLine(markPos, 0, markPos, dim.height);
		drawDashedLine(g2, markPos, 0, dim.height);
		// System.out.println("markPos = " + markPos + "; cursorPos = " +
		// cursorPos );

		g2.setColor(Color.BLUE);
		for (int i = 0; i < values.length; i++) {
			int mid = Math.round(values[i] * scaleFactor) / 2;
			g2.drawLine(i, y - mid, i, y + mid);
		}

		g2.setColor(Color.ORANGE);
		g2.drawLine(0, y, dim.width, y);

		g2.setColor(Color.green);
		g2.drawLine(cursorPos, 10, cursorPos, dim.height - 10);
	}

	public void removeLoadingLabel() {
		remove(loadingJL);
	}
	
	public void setCursorPos(int val) {
		cursorPos = val;
		repaint();
	}

	public void setCursorPos(final int val, final int delay) {
		new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep((int)delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				cursorPos = val;
				repaint();
			}

		}.start();
	}

	public int getMarkPos() {
		return markPos;
	}

	public void setMarkPos(int markPos) {
		this.markPos = markPos;
		repaint();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		dim = getSize();
		y = dim.height / 2;
		// System.out.println("GraphPanel.width = " + dim.width);
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
	}

	public float getAmplitude(int index) {
		return values[index];
	}
}
