import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import gui.*;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

import utils.*;

public class SliderTest extends JFrame {

	private MySlider slider;
		
	
	class MySlider extends JSlider implements MouseMotionListener {
		MySlider(int a, int b, int c) {
			super(a, b, c);
			addMouseMotionListener(this);
		}

		@Override
		public void mouseDragged(MouseEvent arg0) {
			int value = slider.getValue();

			if (slider.getOrientation() == JSlider.HORIZONTAL) {
				value = ((WindowsSliderUI)getUI()).valueForXPosition(slider.getMousePosition().x);
			} else if (slider.getOrientation() == JSlider.VERTICAL) {
				value = ((WindowsSliderUI)getUI()).valueForYPosition(slider.getMousePosition().y);
			}
			slider.setValue(value);
			
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			
			
		}
	}

	public SliderTest() {
		initComponents();
	}

	private void initComponents() {
		setTitle("SliderTest");
		setSize(400, 200);
		SwingUtils.setLocationOnCenter(this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(this);

		slider = new MySlider(0, 100, 0);
		final SliderToolTip stt = new SliderToolTip(slider);
		stt.setToolTipText("0");
		slider.setUI(new WindowsSliderUI(slider) {
			@Override
			protected void scrollDueToClickInTrack(int direction) {
				// this is the default behaviour, let's comment that out
				// scrollByBlock(direction);

				int value = slider.getValue();

				if (slider.getOrientation() == JSlider.HORIZONTAL) {
					value = this.valueForXPosition(slider.getMousePosition().x);
				} else if (slider.getOrientation() == JSlider.VERTICAL) {
					value = this.valueForYPosition(slider.getMousePosition().y);
				}
				slider.setValue(value);
			}

			@Override
			protected void installKeyboardActions(JSlider slider) {

			}
		});

		add(slider);

		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				// System.out.println(slider.getValue());
				stt.setToolTipText(Integer.toString(slider.getValue()));
			}

		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new SliderTest().setVisible(true);
			}
		});
	}

}
