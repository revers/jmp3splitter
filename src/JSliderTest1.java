import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
* JSliderTest1
* @author John B. Matthews
*/
public class JSliderTest1 extends JPanel {

   public static void main(String args[]) {
       EventQueue.invokeLater(new Runnable() {
           public void run() {
           JFrame frame = new JFrame("Sliders!");
           frame.add(new JSliderTest1());
           frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           frame.setSize(550, 400);
           frame.setVisible(true);
           }
       });
   }

   public JSliderTest1() {
       this.setLayout(new BorderLayout());
       this.add(genVerticalSliderPanel(), BorderLayout.WEST);
       this.add(genHorizontalSliderPanel(1), BorderLayout.NORTH);
       this.add(genHorizontalSliderPanel(3), BorderLayout.CENTER);
       this.add(genHorizontalSliderPanel(1), BorderLayout.SOUTH);
       this.add(genVerticalSliderPanel(), BorderLayout.EAST);
   }

   private JPanel genHorizontalSliderPanel(int count) {
       JPanel panel = new JPanel();
       panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
       for (int i = 0; i < count; i++) {
           JSlider slider = new JSlider(JSlider.HORIZONTAL);
           decorate(slider, 400,  Short.MAX_VALUE);
           panel.add(Box.createVerticalGlue());
           panel.add(slider);
       }
       panel.add(Box.createVerticalGlue());
       return panel;
   }

   private JPanel genVerticalSliderPanel() {
       JPanel panel = new JPanel();
       panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
       JSlider slider = new JSlider(JSlider.VERTICAL);
       decorate(slider, Short.MAX_VALUE, 300);
       panel.add(slider);
       return panel;
   }
   
   private void decorate(JSlider slider, int w, int h) {
       slider.setMaximumSize(new Dimension(w, h));
       slider.setSnapToTicks(true);
       slider.setMajorTickSpacing(20);
       slider.setPaintTicks(true);
       slider.setPaintLabels(true);
   }
}