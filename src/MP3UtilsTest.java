import java.io.*;

import mp3.*;

public class MP3UtilsTest {
	private static final int SAME_FRAMES = 5;
	private static final int BUFFER_SIZE = (433 * 4 + 8) * SAME_FRAMES;

	private static int getFirstFramePos(File inFile) throws IOException {
		BufferedInputStream source = new BufferedInputStream(
				new FileInputStream(inFile));
		PushbackInputStream in = new PushbackInputStream(source, BUFFER_SIZE);

		byte[] buff = new byte[BUFFER_SIZE];
		int index = 0;

		int resultPos = 0;
		int counter = 0;
		int i = 0;
		MP3Header oldMP3Header = null;
		int headerCompleteness = 0;
		int header = 0;
		int oldHeader = 0;
		boolean restoreOld = false;
		boolean flag = true;
		int pos = 0;
		while ((i = in.read()) != -1) {

			pos++;

			if (oldMP3Header != null)
				buff[index++] = (byte) i;
			header <<= 8;
			header |= i;
			headerCompleteness++;
			if (flag) {
				// printBinaryByte((byte)i);
				resultPos++;
			}
			if (headerCompleteness >= 4) {
				try {
					MP3Header mp3Header = new MP3Header(header);
					//System.out.println("POS = " + (pos - 4));
					flag = false;
					if (oldMP3Header == null) {
						oldMP3Header = mp3Header;
						// resultPos = pos - 4;
						oldHeader = header;
						restoreOld = true;
						// System.out.println("resPos = " + resultPos);
					} else {
						if (!mp3Header.equals(oldMP3Header))
							throw new IncorrectMP3HeaderException("");
					}

					int toRead = mp3Header.getFrameLength() - 4;
					pos += toRead;
					while (toRead > 0) {
						int bytesRead = in.read(buff, index, toRead);
						index += bytesRead;
						toRead -= bytesRead;
					}

					// header = 0;
					counter++;
					headerCompleteness = 0;

					// tempHeader = mp3Header;
				} catch (IncorrectMP3HeaderException e) {
					// System.out.println(index);
					// if(index > 0)
					// index -= 1;
					in.unread(buff, 0, index);
					if (restoreOld)
						header = oldHeader;
					restoreOld = false;
					counter = 0;

					pos -= index;
					index = 0;
					// headerCompleteness = 4;
					oldMP3Header = null;

					flag = true;

				}
			}

			if (counter > SAME_FRAMES - 1) {

				// oldMP3Header.printHeaderLayout(oldMP3Header.getHeaderBits());
				// oldMP3Header.printValues();
				break;
			}

		}

		in.close();
		return (resultPos - 4);
	}
	
	private static void testHeader(File f, int pos) throws IOException {
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				f));

		while (pos > 0)
			pos -= in.skip(pos);

		byte[] buff = new byte[4];
		in.read(buff);
		in.close();
		
		int header = (((buff[0] & 0xff) << 24) | ((buff[1] & 0xff) << 16)
				| ((buff[2] & 0xff) << 8) | (buff[3] & 0xff));
		
		try {
			MP3Header mp3Header = new MP3Header(header);
			System.out.println("frame length = " + mp3Header.getFrameLength());
			System.out.println("GOOD SHIT HEADER ;)");
			MP3Utils.printHeaderLayout(header);
		} catch(IncorrectMP3HeaderException e) {
			System.out.println("ZLY NAGLOWEK: ");
			MP3Utils.printHeaderLayout(header);
		}
	}
	
	private static void testFindFirstFrame() throws Exception {
		File dir = new File("C:\\Users\\Revers\\Desktop\\[MUZYKA]\\techno");
		File[] files = dir.listFiles();
		for(File f : files) {
			int a = getFirstFramePos(f);
			int b = MP3Utils.findFirstFramePos(f);
			System.out.printf("a = %d\tb = %d\n", a, b);
			if(a != b) 
				throw new IOException("Dupa dla pliku: " + f);
		}
	}
	
	private static void testMP3Info() throws Exception {
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("D:\\a.txt")));
		File dir = new File("C:\\Users\\Revers\\Desktop\\[MUZYKA]\\techno");
		File[] files = dir.listFiles();
		for(File f : files) {
			System.out.println(f.getName());
			MP3Info m = MP3Info.scan(f);
			out.println(m.msPerFrame);
			System.out.println(m.msPerFrame);
			System.out.println();
		}
		out.close();
	}
		
	
	public static void main(String[] args) throws Exception {
		System.out.println("START");
		testMP3Info();
		//testFindFirstFrame();
		//File f = new File("C:\\Users\\Revers\\Desktop\\[MUZYKA]\\techno\\D-Unity_&_Richard_Dinsdale_-_Winterdayze_(www4clubbers.com.pl).mp3");
		//testHeader(f, 296);
		System.out.println("END");
	}

}
