import java.io.*;
import java.util.ArrayList;
import java.util.TreeSet;

public class PropertiesFixer {

	private static void fix() throws IOException {
		File dir = new File(
				"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter\\src\\gui");
		File[] files = dir.listFiles();
		for (File f : files) {
			if (!f.getName().endsWith(".java"))
				continue;
			BufferedReader in = new BufferedReader(new FileReader(f));
			String s1 = "", s2 = "";
			String fName = f.getName();
			fName = fName.substring(0, fName.length() - 5) + "_pl.properties";
			File outFile = new File(fName);
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(outFile)));
			boolean firstAppearance = true;
			ArrayList<String> keys = new ArrayList<String>();
			outer: while ((s1 = in.readLine()) != null) {
				if (!s1.contains("getString(\""))
					continue;
				String key = s1.split("\"")[1];
				for (String s : keys) {
					if (s.equals(key))
						continue outer;
				}
				keys.add(key);

				if (firstAppearance) {
					System.out.println(fName);
					firstAppearance = false;
				}
				s1 = s1.trim();
				if (s1.startsWith(".") || s1.startsWith("+"))
					out.println("# " + s2);
				out.println("# " + s1);
				s2 = s1;
				out.println(key + "=");

			}

			in.close();
			out.close();
			if (outFile.length() == 0)
				outFile.delete();
		}

	}

	private static void makeEng() throws IOException {
		File dir = new File(
				"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter");
		File[] files = dir.listFiles();
		for (File f : files) {
			if (!f.getName().endsWith(".properties"))
				continue;
			String fName = f.getName().replace("_pl", "_en");
			System.out.println(fName);
			BufferedReader in = new BufferedReader(new FileReader(f));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fName)));
			String s;
			while ((s = in.readLine()) != null) {
				if (s.startsWith("#") || s.equals(""))
					continue;
				out.println(s);
			}
			in.close();
			out.close();
		}
	}
	
	private static final String[] unicode = { "\\u0105", "\\u0119", "\\u00F3", "\\u015B", "\\u0142", "\\u017C", "\\u017A", "\\u0107", "\\u0144", "\\u0104", "\\u0118", "\\u00D3", "\\u015A", "\\u0141", "\\u017B", "\\u0179", "\\u0106", "\\u0143", "\\!"};
	private static final String[] plChars = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "!" };

	public static void genFinalProperties() throws IOException {
		File dir = new File(
				"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter");
		File[] files = dir.listFiles();
		for (File f : files) {
			if (!f.getName().endsWith(".properties"))
				continue;
			BufferedReader in = new BufferedReader(new FileReader(f));
			
			String s;
			TreeSet<String> set = new TreeSet<String>();
			while ((s = in.readLine()) != null) {
				if (s.startsWith("#") || s.equals(""))
					continue;
				for(int i = 0; i < plChars.length; i++) {
					s = s.replace(plChars[i], unicode[i]);
				}
				
				set.add(s);
			}
			in.close();
			
			PrintWriter out = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(
							"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter\\bin\\"
									+ f.getName()), "ASCII"));
			for(String str : set) {
				out.println(str);
			}
			out.close();
		}
	}
	
	private static String makeUnicode(String s) {
		for(int i = 0; i < plChars.length; i++) {
			s = s.replace(plChars[i], unicode[i]);
		}
		return s;
	}
	

	public static void main(String[] args) throws IOException {
		System.out.println("START");
		//fix();
		//makeEng();
		//genFinalProperties();
		System.out.println(makeUnicode("Podany plik zawiera b��dy. Czy chcesz je naprawi�?"));
		System.out.println("END");
	}

}
