import java.io.*;

import mp3.IncorrectMP3HeaderException;
import mp3.MP3Header;


public class SkipBytesTest {
	
	private static final int BUFFER_SIZE = (433 * 4 + 8) * 5;

	public static int getFirstFramePos(File inFile) throws IOException {
		BufferedInputStream source = new BufferedInputStream(
				new FileInputStream(inFile));
		PushbackInputStream in = new PushbackInputStream(source, BUFFER_SIZE);

		byte[] buff = new byte[BUFFER_SIZE];
		int index = 0;

		int resultPos = 0;
		int counter = 0;
		int i = 0;
		MP3Header oldMP3Header = null;
		int headerCompleteness = 0;
		int header = 0;
		int oldHeader = 0;
		boolean restoreOld = false;
		boolean flag = true;
		while ((i = in.read()) != -1) {
			// pos++;
			if (oldMP3Header != null)
				buff[index++] = (byte) i;
			header <<= 8;
			header |= i;
			headerCompleteness++;
			if (flag) {
				// printBinaryByte((byte)i);
				resultPos++;
			}
			if (headerCompleteness >= 4) {
				try {
					MP3Header mp3Header = new MP3Header(header);
					flag = false;
					if (oldMP3Header == null) {
						oldMP3Header = mp3Header;
						// resultPos = pos - 4;
						oldHeader = header;
						restoreOld = true;
						// System.out.println("resPos = " + resultPos);
					} else {
						if (!mp3Header.equals(oldMP3Header))
							throw new IncorrectMP3HeaderException("");
					}

					int toRead = mp3Header.getFrameLength() - 4;
					// pos += toRead;
					while (toRead > 0) {
						int bytesRead = in.read(buff, index, toRead);
						index += bytesRead;
						toRead -= bytesRead;
					}

					// header = 0;
					counter++;
					headerCompleteness = 0;
					// tempHeader = mp3Header;
				} catch (IncorrectMP3HeaderException e) {
					// System.out.println(index);
					// if(index > 0)
					// index -= 1;
					in.unread(buff, 0, index);
					if (restoreOld)
						header = oldHeader;
					restoreOld = false;
					counter = 0;

					// pos -= index;
					index = 0;
					// headerCompleteness = 4;
					oldMP3Header = null;

					flag = true;

				}
			}
			if (counter > 4) {
				// oldMP3Header.printHeaderLayout(oldMP3Header.getHeaderBits());
				// oldMP3Header.printValues();
				break;
			}

		}

		in.close();
		return (resultPos - 4);
	}

	String[] unicode = { "\\u0105", "\\u0119", "\\u00F3", "\\u015B", "\\u0142", "\\u017C", "\\u017A", "\\u0107", "\\u0144", "\\u0104", "\\u0118", "\\u00D3", "\\u015A", "\\u0141", "\\u017B", "\\u0179", "\\u0106", "\\u0143" };
	String[] pl = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�" };
	
	public static void main(String[] args) {
		
		char[] carr = { '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�' };
		for(char c : carr) {
			String str = Integer.toHexString((int)c);
			if(str.length() == 3)
				str = "0" + str;
			else if(str.length() == 2)
				str = "00" + str;
			System.out.print("\"\\\\u" + str.toUpperCase() + "\", ");
		}

	}

}
