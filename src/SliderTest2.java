import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicSliderUI;

import gui.*;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;

import de.muntjak.tinylookandfeel.TinySliderUI;

import utils.*;

public class SliderTest2 extends JFrame {

	private class MySliderUI extends WindowsSliderUI implements
			MouseMotionListener, MouseListener {
		final JPopupMenu pop = new JPopupMenu();
		JMenuItem item = new JMenuItem();

		public MySliderUI(JSlider slider) {
			super(slider);
			//this.slider = slider;
			slider.addMouseMotionListener(this);
			slider.addMouseListener(this);
			pop.add(item);

			pop.setDoubleBuffered(true);
		}

		public void showToolTip(MouseEvent me) {
			item.setText(mySlider.getValue() + " fps");

			// limit the tooltip location relative to the slider
//			Rectangle b = me.getComponent().getBounds();
//			int x = me.getX();
//			x = (x > (b.x + b.width / 2) ? (b.x + b.width / 2)
//					: (x < (b.x - b.width / 2) ? (b.x - b.width / 2) : x));
			Point toolTipLocation = getToolTipLocation(me);

			//pop.show(me.getComponent(), x - 5, me.getY() -30);
			pop.show(me.getComponent(), toolTipLocation.x, toolTipLocation.y);
			// pop.setFocusable( false );
			// pop.setEnabled( false );

			item.setArmed(false);
			// item.setSelected(false);

			// item.setEnabled( false );
		}
		
		public Point getToolTipLocation(MouseEvent event) {
			Point p = event.getLocationOnScreen();
			Rectangle sliderBounds = getBounds();
			Point sliderLoc = getLocationOnScreen();
			int y = sliderLoc.y + (sliderBounds.height / 2) + 12;
			int x = p.x;

			Dimension toolTipDim = pop.getSize();
			int maxXLocation = sliderLoc.x + sliderBounds.width - toolTipDim.width;
			int minXLocation = sliderLoc.x;

			if (x > maxXLocation)
				x = maxXLocation;
			else if (x < minXLocation)
				x = minXLocation;

			return new Point(x, y);
		}

		public void mouseDragged(MouseEvent me) {
			showToolTip(me);
		}

		public void mouseMoved(MouseEvent me) {
		}

		public void mousePressed(MouseEvent me) {
			showToolTip(me);
		}

		public void mouseClicked(MouseEvent me) {
		}

		public void mouseReleased(MouseEvent me) {
			pop.setVisible(false);
		}

		public void mouseEntered(MouseEvent me) {
		}

		public void mouseExited(MouseEvent me) {
		}
	}

	private JSlider mySlider;

	public SliderTest2() {
		initComponents();
	}

	private void initComponents() {
		setTitle("SliderTest2");
		setSize(400, 200);
		SwingUtils.setLocationOnCenter(this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(this);

		mySlider = new JSlider(0, 100, 0);
		//final SliderToolTip stt = new SliderToolTip(mySlider);
		//stt.setToolTipText("0");
		// slider.setUI(new WindowsSliderUI(slider) {
		// @Override
		// protected void scrollDueToClickInTrack(int direction) {
		// // this is the default behaviour, let's comment that out
		// // scrollByBlock(direction);
		//
		// int value = slider.getValue();
		//
		// if (slider.getOrientation() == JSlider.HORIZONTAL) {
		// value = this.valueForXPosition(slider.getMousePosition().x);
		// } else if (slider.getOrientation() == JSlider.VERTICAL) {
		// value = this.valueForYPosition(slider.getMousePosition().y);
		// }
		// slider.setValue(value);
		// }
		//
		// @Override
		// protected void installKeyboardActions(JSlider slider) {
		//
		// }
		// });
		mySlider.setUI(new MySliderUI(mySlider));

		add(mySlider);

//		mySlider.addChangeListener(new ChangeListener() {
//
//			@Override
//			public void stateChanged(ChangeEvent arg0) {
//				// System.out.println(slider.getValue());
//				stt.setToolTipText(Integer.toString(mySlider.getValue()));
//			}
//
//		});

		mySlider.setUI(new WindowsSliderUI(mySlider) {
			protected void scrollDueToClickInTrack(int direction) {
				// this is the default behaviour, let's comment that out
				// scrollByBlock(direction);

				int value = slider.getValue();

				if (slider.getOrientation() == JSlider.HORIZONTAL) {
					value = this.valueForXPosition(slider.getMousePosition().x);
				} else if (slider.getOrientation() == JSlider.VERTICAL) {
					value = this.valueForYPosition(slider.getMousePosition().y);
				}
				slider.setValue(value);
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new SliderTest2().setVisible(true);
			}
		});
	}

}
