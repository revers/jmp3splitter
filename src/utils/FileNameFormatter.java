package utils;
import java.util.regex.*;
public class FileNameFormatter {
	
	// Dodaje spacje za i przed myslnikiem, pomiajac myslniki 
	// miedzy dwoma cyframi (np. w datach).
	private static String spaceDashes(String s) {
		if (!s.contains("-"))
			return s;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != '-')
				continue;

			if ((i - 1) >= 0 && (i + 1) < s.length()
					&& Character.isDigit(s.charAt(i - 1))
					&& Character.isDigit(s.charAt(i + 1)))
				continue;

			return s.substring(0, i) + " - "
					+ spaceDashes(s.substring(i + 1, s.length()));
		}
		return s;
	}

	public static String format(String s) {
		s = s.replace("%20", " ");
		s = s.replace('_', ' ');
		s = spaceDashes(s);
		//s = s.replace("-", " - ");
		s = s.replace("(", " (");
		s = s.replaceAll("\\s{2,}", " ");
		s = s.trim();
		s = capitalizeFirstLetters(s);
		
		
		
		return s;
		
	}
	
	private static String capitalizeFirstLetters(String s) {
		Pattern p = Pattern.compile("\\b[\\w&&[^\\d]]+[\\S]*");
		Matcher m = p.matcher(s);
		while(m.find()) {
			
			String gr = m.group().trim();
			
			char first = gr.charAt(0);
			if(Character.isUpperCase(first))
				continue;
			
			gr = gr.substring(0, 1).toUpperCase() + gr.substring(1).toLowerCase();
			s = s.substring(0, m.start()) + gr + s.substring(m.end());
		}
		return s;
	}
	
	public static void main(String[] args) {
		
		String s = format("06-burial-southern_comfort.mp3");
		if(s.toLowerCase().endsWith(".mp3"))
			s = s.substring(0, s.length() - 4);
		
		System.out.println(s);

	}

}
