package utils;
import javax.swing.*;

public class KeyAction {
	private KeyStroke keyStroke;
	private AbstractAction action;
	private String name;
	
	public KeyAction(String name, KeyStroke keyStroke, AbstractAction action) {
		this.keyStroke = keyStroke;
		this.action = action;
		this.name = name;
	}
	
	public KeyStroke getKeyStroke() {
		return keyStroke;
	}

	public void setKeyStroke(KeyStroke keyStroke, InputMap inputMap) {
		inputMap.remove(this.keyStroke);
		inputMap.put(keyStroke, action);
		this.keyStroke = keyStroke;
		
	}

	public AbstractAction getAction() {
		return action;
	}

	public String getName() {
		return name;
	}

}
