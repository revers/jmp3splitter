package utils;

import java.util.Locale;

public class OptionValues {
	public static final String JMP_PREF = "jmp";
	public static final String AUTOFILL_PREF = "auto";
	public static final String SECONDS_PREF = "s";
	
	public static final String FIVE_S_AHEAD_PREF = "5sa";
	public static final String FIVE_S_BACK_PREF = "5sb";
	public static final String ONE_MIN_AHEAD_PREF = "1mina";
	public static final String ONE_MIN_BACK_PREF = "1minb";
	public static final String PLAY_PAUSE_PREF = "pp";
	public static final String MARK_BEGIN_PREF = "mbeg";
	public static final String MARK_END_PREF = "mend";
	public static final String STOP_PREF = "stop";
	public static final String ROLL_PREF = "roll";
	public static final String LANG_PREF = "lang";
	
	public static final Locale LOCALE_PL = new Locale("pl");

	private boolean jmpToEnd = true;
	private boolean autofillTag = true;
	private int seconds = 3;
	
	private String fiveSAhead = "39;0";
	private String fiveSBack = "37;0";
	private String oneMinAhead = "38;0"; 
	private String oneMinBack = "40;0";
	private String markBegin = "65;0";
	private String markEnd = "70;0";
	private String playPause = "32;0";
	private String stop = "10;0";
	private String roll = "s";
	private String lang = "en";
	
	private static final OptionValues defaultValues = new OptionValues();
//	static {
//		defaultValues.setJmpToEnd(true);
//		defaultValues.setAutofillTag(true);
//		defaultValues.setSeconds(4);
//		defaultValues.setFiveSAhead("39;0");
//		defaultValues.setFiveSBack("37;0");
//		defaultValues.setOneMinAhead("38;0");
//		defaultValues.setOneMinBack("40;0");
//		defaultValues.setMarkBegin("65;0");
//		defaultValues.setMarkEnd("70;0");
//		defaultValues.setPlayPause("32;0");
//		defaultValues.setStop("10;0");
//		defaultValues.setRoll("s");
//	}
	
//	private OptionValues(boolean jmpToEnd, boolean autofillTag, int seconds) {
//		this.jmpToEnd = jmpToEnd;
//		this.autofillTag = autofillTag;
//		this.seconds = seconds;
//	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isJmpToEnd() {
		return jmpToEnd;
	}

	public void setJmpToEnd(boolean jmpToEnd) {
		this.jmpToEnd = jmpToEnd;
	}

	public boolean isAutofillTag() {
		return autofillTag;
	}

	public void setAutofillTag(boolean autofillTag) {
		this.autofillTag = autofillTag;
	}

	public int getSeconds() {
		return seconds;
	}

	public String getRoll() {
		return roll;
	}

	public void setRoll(String roll) {
		this.roll = roll;
	}

	public String getFiveSAhead() {
		return fiveSAhead;
	}

	public void setFiveSAhead(String fiveSAhead) {
		this.fiveSAhead = fiveSAhead;
	}

	public String getFiveSBack() {
		return fiveSBack;
	}

	public void setFiveSBack(String fiveSBack) {
		this.fiveSBack = fiveSBack;
	}

	public String getOneMinAhead() {
		return oneMinAhead;
	}

	public void setOneMinAhead(String oneMinAhead) {
		this.oneMinAhead = oneMinAhead;
	}

	public String getOneMinBack() {
		return oneMinBack;
	}

	public void setOneMinBack(String oneMinBack) {
		this.oneMinBack = oneMinBack;
	}

	public String getMarkBegin() {
		return markBegin;
	}

	public void setMarkBegin(String markBegin) {
		this.markBegin = markBegin;
	}

	public String getMarkEnd() {
		return markEnd;
	}

	public void setMarkEnd(String markEnd) {
		this.markEnd = markEnd;
	}

	public String getPlayPause() {
		return playPause;
	}

	public void setPlayPause(String playPause) {
		this.playPause = playPause;
	}

	public String getStop() {
		return stop;
	}

	public void setStop(String stop) {
		this.stop = stop;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public static OptionValues getDefaults() {
		return defaultValues;
	}
}
