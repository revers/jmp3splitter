package utils;

import javax.swing.KeyStroke;

public enum ShortcutsEnum {
	FIVE_S_AHEAD(39, 0), FIVE_S_BACK(37, 0), ONE_MIN_AHEAD(38, 0), ONE_MIN_BACK(
			40, 0), MARK_BEGIN(65, 0), MARK_END(70, 0), PLAY_PAUSE(32, 0), STOP(
			10, 0);

	private int key;
	private int mod;

	private ShortcutsEnum(int key, int mod) {
		this.key = key;
		this.mod = mod;
	}

	public int getKey() {
		return key;
	}

	public int getMod() {
		return mod;
	}

	public void setKeys(int key, int mod) {
		this.key = key;
		this.mod = mod;

	}
	
	public boolean isEqual(int key, int mod) {
		if(this.key == key && this.mod == mod)
			return true;
		return false;
	}
	
	public KeyStroke getKeyStroke() {
		return KeyStroke.getKeyStroke(key, mod);
	}
}
