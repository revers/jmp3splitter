package utils;

public class TimeData {

	public static final int HOUR = 1000 * 60 * 60;
	public static final int MINUTE = 1000 * 60;
	public static final int SECOND = 1000;
	public static final int HUNDRED_MILLIS = 1;

	private int maxMillis = -1;
	private int currentMillis = 0;
	private int oldCurrentMillis = -1;
	private boolean gotHours = false;
	private boolean gotMinutes = false;

	public TimeData() {
	}

	public TimeData(int currentMillis) {
		this.currentMillis = currentMillis;
	}

	public TimeData(int currentMillis, int maxMillis) {
		this.currentMillis = currentMillis;
		setMaxMillis(maxMillis);
	}

	public int getMaxMillis() {
		return maxMillis;
	}

	public void setMaxMillis(int maxMillis) {
		this.maxMillis = maxMillis;

		if (maxMillis / HOUR > 0) {
			gotHours = true;
			gotMinutes = true;

		} else if (maxMillis / MINUTE > 0) {
			gotMinutes = true;
		} else {
			gotHours = false;
			gotMinutes = false;
		}
	}

	public int getCurrentMillis() {
		return currentMillis;
	}

	public void setCurrentMillis(int currentMillis) {
		this.currentMillis = currentMillis;
	}

	public boolean addMillis(int millis) {
		if (maxMillis == -1) {
			currentMillis += millis;
			return true;
		}

		if (currentMillis + millis > maxMillis)
			currentMillis = maxMillis;
		else
			currentMillis += millis;

		if (currentMillis == oldCurrentMillis)
			return false;

		oldCurrentMillis = currentMillis;
		return true;
	}

	public boolean subMillis(int millis) {
		if (currentMillis - millis < 0)
			currentMillis = 0;
		else
			currentMillis -= millis;

		if (currentMillis == oldCurrentMillis)
			return false;

		oldCurrentMillis = currentMillis;
		return true;
	}

	public boolean increaseHours() {
		return addMillis(HOUR);
	}

	public boolean decreaseHours() {
		return subMillis(HOUR);
	}

	public boolean increaseMinutes() {
		return addMillis(MINUTE);
	}

	public boolean decreaseMinutes() {
		return subMillis(MINUTE);
	}

	public boolean increaseSeconds() {
		return addMillis(SECOND);
	}

	public boolean decreaseSeconds() {
		return subMillis(SECOND);
	}

	public boolean addHudredMillis() {
		return addMillis(100);
	}

	public boolean subHudredMillis() {
		return subMillis(100);
	}

	public String toString() {

		int millis = currentMillis;
		int hours = millis / HOUR;
		millis -= hours * HOUR;
		int mins = millis / MINUTE;
		millis -= mins * MINUTE;
		int secs = millis / SECOND;
		millis -= secs * SECOND;

		StringBuilder s = new StringBuilder();
		if (gotHours || hours > 0) {
			if (hours < 10)
				s.append("0");
			s.append(hours).append(":");
		}
		if (gotMinutes || mins > 0) {
			if (mins < 10)
				s.append("0");
			s.append(mins).append(":");
		}

		if (secs < 10)
			s.append("0");
		
		s.append(secs).append(".");
		String m = Integer.toString(millis);
		s.append(m);
		switch (m.length()) {
		case 1:
			s.append("00");
			break;
		case 2:
			s.append("0");
			break;
		}
		//s.append(secs).append(".").append(m);

		return s.toString();
	}

	public String toStringWithoutMillis() {
		int millis = currentMillis;
		int hours = millis / HOUR;
		millis -= hours * HOUR;
		int mins = millis / MINUTE;
		millis -= mins * MINUTE;
		int secs = millis / SECOND;
		millis -= secs * SECOND;

		StringBuilder s = new StringBuilder();
		if (gotHours || hours > 0) {
			if (hours < 10)
				s.append("0");
			s.append(hours).append(":");
		}
		if (gotMinutes || mins > 0) {
			if (mins < 10)
				s.append("0");
			s.append(mins).append(":");
		}

		// TODO: Gdzie indziej tez zaokraglac? //sprawdz poniezej (**BLE**)
		if (millis > 500)
			secs += 1;

		if (secs < 10)
			s.append("0");
		s.append(secs);// .append(".").append(mills);

		return s.toString();
	}

	public String toStringShorter() {
		// int millis = currentMillis;
		// millis += 50; // Zaokroglenie. TODO: SPRAWDZIC CZY DOBRE!! (**BLE**)
		int millis = Math.round((float) (currentMillis) / 100.0f) * 100;
		int hours = millis / HOUR;

		millis -= hours * HOUR;
		int mins = millis / MINUTE;
		millis -= mins * MINUTE;
		int secs = millis / SECOND;
		millis -= secs * SECOND;

		StringBuilder s = new StringBuilder();
		if (gotHours || hours > 0) {
			if (hours < 10)
				s.append("0");
			s.append(hours).append(":");
		}
		if (gotMinutes || mins > 0) {
			if (mins < 10)
				s.append("0");
			s.append(mins).append(":");
		}

		if (secs < 10)
			s.append("0");
		// String mil = Integer.toString(millis);

		millis /= 100;

		s.append(secs).append(".").append(millis);

		return s.toString();

	}

	public static int shorterToMillis(String time) {
		String[] tokens = time.split("[:|.]");
		int factor = 100;
		int result = Integer.parseInt(tokens[tokens.length - 1]) * factor;
		factor *= 10;
		for (int i = tokens.length - 2; i >= 0; i--) {
			result += Integer.parseInt(tokens[i]) * factor;
			factor *= 60;
		}
		return result;
	}

	public static void main(String[] args) {
		// System.out.println(Integer.MAX_VALUE / (1000 * 60 * 60));
		TimeData td = new TimeData(1000 * 60 * 60 + 1000 * 60 * 25 + 1005 * 13,
				1000 * 60 * 60 * 2);
		System.out.println(td.toStringShorter());
		System.out.println("a = " + td.getCurrentMillis());
		System.out.println("b = " + shorterToMillis(td.toStringShorter()));
		// td.increaseHours();
		// System.out.println(td.toStringShorter());

	}

}
