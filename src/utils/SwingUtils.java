package utils;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.*;

public class SwingUtils {
	public static void setLocationOnCenter(JFrame frame) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (dim.width / 2) - (frame.getWidth() / 2);
		int y = (dim.height / 2) - (frame.getHeight() / 2);
		frame.setLocation(x, y);
	}

	public static void setLocationOnCenter(JFrame parent, JDialog dialog) {
		Point p = parent.getLocation();
		Dimension dim = parent.getSize();
		int x = p.x + (dim.width / 2) - (dialog.getWidth() / 2);
		int y = p.y + (dim.height / 2) - (dialog.getHeight() / 2);
		dialog.setLocation(x, y);
	}

	public static void setSizeAndCenter(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		setLocationOnCenter(frame);
	}

	public static void setSizeAndCenter(JFrame parent, JDialog dialog,
			int width, int height) {
		dialog.setSize(width, height);
		setLocationOnCenter(parent, dialog);
	}

	public static void setWindowsLAF(JFrame frame) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(frame);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setWindowsLAF(JDialog dialog) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());// "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(dialog);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
