package utils;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//
//import cn.cooper.id3.ID3v1Tag;
//import cn.cooper.id3.ID3v2Tag;
//import cn.cooper.id3.MP3File;

public class TagData {
	public String fileName;
	public boolean rememberTag;
	public boolean autoFileName;
	public String artist = "";
	public String title = "";
	public String album = "";
	public String comment = "";
	public String year = "";
	public String genre = "";

	public TagData(String fileName) {
		this.fileName = fileName;
	}

//	public static boolean hasBadEncoding(String str) {
//		return str.startsWith("��");
//	}

	public static String fixBadEncoding(String str) {
		if (!str.startsWith("��"))
			return str;
		//str = str.substring(1);
		StringBuffer buff = new StringBuffer();
		for (int i = 2; i < str.length(); i += 2)
			buff.append(str.charAt(i));

		return buff.toString();
	}
}
