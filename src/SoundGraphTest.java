import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javazoom.jl.decoder.*;

import com.sun.java.swing.plaf.windows.WindowsSliderUI;
import java.io.*;

import utils.*;
import gui.*;
import mp3.*;

public class SoundGraphTest extends JFrame implements MouseWheelListener {
	private JButton playJB;
	private JButton pauseJB;
	private JButton stopJB;
	private JButton exitJB;
	private MySlider progressSlider;
	private JLabel posLabel;
	private JLabel durLabel;
	private MP3Info info;
	private TimeData actualTime;
	private TimeData durTime;
	private MP3Player mp3Player;
	private int minimalSliderValue;
	private boolean canChangeSliderValue = true;
	private GraphPanel graphPanel;
	
	private class GraphPanel extends JPanel {
		private int[] graph;
		private int cursorPos;
		
		GraphPanel(MP3Info info, int seconds) {
			System.out.println("Making sound graph...");
			SoundGraph sg = new SoundGraph(info);
			try {
				graph = sg.getValues(MILLIS, 300);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}
			System.out.println("Done graph.");
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Dimension d = getSize();
			int y = d.height / 2;
			Graphics2D g2 = (Graphics2D)g;
			g2.setColor(Color.BLUE);
			for(int i = 0; i < graph.length; i++) {
				int mid = graph[i]/2;
				g2.drawLine(i, y - mid, i, y + mid);
			}
			
			g2.setColor(Color.red);
			g2.drawLine(0, y, d.width, y);
			
			g2.setColor(Color.green);
			g2.drawLine(cursorPos, 0, cursorPos, d.height);
		}
		
		public void setCursorPos(int pos) {
			cursorPos = pos;
			repaint();
		}
	}
	private static final int MILLIS = 10000;

	public SoundGraphTest(MP3Info info) {
		this.info = info;
		SwingUtils.setWindowsLAF(this);

		int frames = Math.round((float)MILLIS / info.msPerFrame);
		int beginFrame = 0;
		PlayerStateListener psl = new PlayerStateListener();
		progressSlider = new MySlider();
		ProgressSliderListener progList = new ProgressSliderListener();
		progressSlider.addMouseListener(progList);
		progressSlider.addKeyListener(progList);
		progressSlider.addChangeListener(progList);

		try {
			if (info.hasAdditionalFrame) {
				beginFrame = 1;
				minimalSliderValue = 1;
				mp3Player = new MP3Player(info, psl, beginFrame, frames + 1);
				progressSlider.setMaximum(frames + 1);
			} else {
				mp3Player = new MP3Player(info, psl, beginFrame, frames);
				progressSlider.setMaximum(frames);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Stream Pos = " + mp3Player.getStreamPosition());

		progressSlider.setMinimum(minimalSliderValue);
		progressSlider.setValue(minimalSliderValue);
		mp3Player.setHighPrecisionTimer(true);

		posLabel = new JLabel();
		durLabel = new JLabel();
		int dur = Math.round(frames * info.msPerFrame);
		durTime = new TimeData(dur, dur);
		durLabel.setText(durTime.toString());

		actualTime = new TimeData(0, dur);
		posLabel.setText(actualTime.toString());
		
		graphPanel = new GraphPanel(info, MILLIS);

		initComponents();
		addMouseWheelListener(this);

		progressSlider.setUI(new WindowsSliderUI(progressSlider) {
			protected void scrollDueToClickInTrack(int direction) {
				// this is the default behaviour, let's comment that out
				// scrollByBlock(direction);

				int value = slider.getValue();

				if (slider.getOrientation() == JSlider.HORIZONTAL) {
					value = this.valueForXPosition(slider.getMousePosition().x);
				} else if (slider.getOrientation() == JSlider.VERTICAL) {
					value = this.valueForYPosition(slider.getMousePosition().y);
				}
				slider.setValue(value);
			}
		});
	}

	private void initComponents() {

		playJB = new JButton(">");
		pauseJB = new JButton("||");
		stopJB = new JButton("X");
		exitJB = new JButton("Exit");

		setTitle("SoundGraphTest");
		SwingUtils.setSizeAndCenter(this, 600, 450);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		mainPanel.setLayout(new BorderLayout());

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new GridLayout(3, 1, 5, 5));

		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.X_AXIS));
		labelPanel.add(new JLabel("Position: "));
		labelPanel.add(posLabel);
		labelPanel.add(Box.createHorizontalGlue());
		labelPanel.add(new JLabel("Length: "));
		labelPanel.add(durLabel);

		southPanel.add(labelPanel);
		southPanel.add(progressSlider);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		buttonsPanel.add(playJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(pauseJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(stopJB);
		buttonsPanel.add(Box.createHorizontalStrut(5));
		buttonsPanel.add(Box.createHorizontalGlue());
		buttonsPanel.add(exitJB);

		southPanel.add(buttonsPanel);

		mainPanel.add(graphPanel, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		add(mainPanel);

		exitJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		playJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					if (progressSlider.getValue() == progressSlider
							.getMaximum())
						try {
							mp3Player.stop();
						} catch (Exception e) {
							e.printStackTrace();
						}
					mp3Player.play();
				}
				progressSlider.requestFocusInWindow();
			}
		});

		pauseJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					mp3Player.pause();
				}
				progressSlider.requestFocusInWindow();
			}
		});

		stopJB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mp3Player != null) { // TODO: niepotrzebne sprawdzanie?
					try {
						mp3Player.stop();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				progressSlider.requestFocusInWindow();
			}
		});
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		if (mp3Player == null)
			return;
		try {
			if (e.getWheelRotation() == -1)
				mp3Player.seekAhead5s();
			else {
				mp3Player.seekBack5s();
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	private class PlayerStateListener implements PlaybackListener {

		public void playbackStarted(MP3Player player) {
			// System.out.println("playback started :P");
			playJB.setEnabled(false);
			pauseJB.setEnabled(true);
			stopJB.setEnabled(true);
		}

		public void playbackPaused(MP3Player player) {
			// System.out.println("Playback paused :P");
			playJB.setEnabled(true);
			pauseJB.setEnabled(false);

			// stopButton.setEnabled(false);
		}

		public void playbackStoped(MP3Player player) {
			// System.out.println("playback stoped :P");
			actualTime.setCurrentMillis(0);
			progressSlider.setValue(minimalSliderValue);
			posLabel.setText(actualTime.toString());
			pauseJB.setEnabled(false);
			playJB.setEnabled(true);
			stopJB.setEnabled(false);
		}

		public void playbackFinished(MP3Player player) {
			// mp3Player.pause();
			try {
				mp3Player.stop();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}
		}

		public void playbackTimeChanged(MP3Player player) {
			// if(!canChangeValue)

			actualTime.setCurrentMillis(mp3Player.getCurrMillis());// TODO:
			// zamienic
			// getCurrentTime z metody na pole?
			posLabel.setText(actualTime.toString());
			
			if (canChangeSliderValue) {
				progressSlider.setValue(mp3Player.getCurrFrame());
				graphPanel.setCursorPos(mp3Player.getCurrFrame());
			}
		}
	}

	private class ProgressSliderListener implements ChangeListener,
			MouseListener, KeyListener {

		@Override
		public void stateChanged(ChangeEvent arg0) {

			if (!canChangeSliderValue) {
				if (mp3Player == null)
					return;

				int val = progressSlider.getValue();
				if (mp3Player.info.hasAdditionalFrame)
					val -= 1;
				TimeData tempTime = new TimeData(Math
						.round(((float) val * mp3Player.info.msPerFrame)),
						actualTime.getMaxMillis());
				progressSlider.setToolTipText(tempTime.toString());
				graphPanel.setCursorPos(val);
				// actualTime.setCurrentMillis();
				// posLabel.setText(actualTime.toString());
			}
			// System.out.println("stateChanged = " +
			// progressSlider.getValue());
			// if (mouseReleased) {
			// System.out.println("STATE CHANGED");
			// System.out.println("stateChanged = "
			// + progressSlider.getValue());
			// mp3Player.seek(progressSlider.getValue());
			// ///mouseReleased = false; // Niepotrzebne?
			// }
			// else
			// flag = true;

		}

		@Override
		public void keyPressed(KeyEvent e) {
			// System.out.println("Key Pressed");

			if (mp3Player == null)
				return;

			// System.out.println(KeyEvent.getModifiersExText(e.getModifiersEx()));

			int key = e.getKeyCode();
			int mod = e.getModifiers();
			try {
				if (ShortcutsEnum.ONE_MIN_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead1min();
				} else if (ShortcutsEnum.ONE_MIN_BACK.isEqual(key, mod)) {
					mp3Player.seekBack1min();
				} else if (ShortcutsEnum.FIVE_S_AHEAD.isEqual(key, mod)) {
					mp3Player.seekAhead5s();
				} else if (ShortcutsEnum.FIVE_S_BACK.isEqual(key, mod)) {
					mp3Player.seekBack5s();
				} else if (ShortcutsEnum.PLAY_PAUSE.isEqual(key, mod)) {
					if (!mp3Player.isPlaying()) {
						if (progressSlider.getValue() == progressSlider
								.getMaximum()) {
							mp3Player.stop();
						}
						mp3Player.play();
					} else {
						mp3Player.pause();
					}
				} else if (ShortcutsEnum.STOP.isEqual(key, mod)) {
					mp3Player.stop();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			canChangeSliderValue = false;
			int val = progressSlider.getValue();
			if (mp3Player.info.hasAdditionalFrame)
				val -= 1;
			TimeData tempTime = new TimeData(Math
					.round(((float) val * mp3Player.info.msPerFrame)),
					actualTime.getMaxMillis());
			progressSlider.setToolTipText(tempTime.toString());
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			if (mp3Player == null)
				return;
			// System.out.println("MOUSE RELEASED");
			// System.out.println("getValue = " + progressSlider.getValue());
			try {
				mp3Player.seek(progressSlider.getValue());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// ////actualTime.setCurrentMillis(mp3Player.getNextTime());
			// System.out.println("ac = " + actualTime.getCurrentMillis());
			// ///posLabel.setText(actualTime.toString());
			canChangeSliderValue = true;

			// mp3Player.seek(progressSlider.getValue());

		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				System.out.println("Getting Info...");
				File f = new File("D:\\Habakuk - Rasta Trans Remix 2004.mp3");
				MP3Info info = null;
				try {
					info = MP3Info.scan(f);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
				System.out.println("Done.");
				System.out.printf("firstFramePos = %d\n", info.firstFramePos);
				SoundGraphTest sgt = new SoundGraphTest(info);
				sgt.setVisible(true);
			}
		});
	}
}
