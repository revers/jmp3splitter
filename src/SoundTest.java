import javazoom.jl.decoder.*;
import java.io.*;

import mp3.*;

public class SoundTest {
	private static int averageAplitude(short[] arr) {
		int avg = 0;
		for(short sh : arr) {
			if(sh < 0)
				avg += -sh;
			else
				avg += sh;
		}
		return Math.round((float)avg/(float)arr.length);
	}
	
	private static void test(MP3Info info) throws IOException,
			JavaLayerException {
		PosFileInputStream stream = new PosFileInputStream(info.mp3File);
		int toSkip = info.firstFramePos;
		while (toSkip != 0)
			toSkip -= stream.skip(toSkip);

		Bitstream bitstream = new Bitstream(stream);
		Decoder decoder = new Decoder();

		for (int i = 0; i < 400; i++) {
			Header h = bitstream.readFrame();
			//System.out.println("ms/frame = " + h.ms_per_frame());
			if (h == null) {
				throw new IOException("Nie ma ramki O_o ;)");
			}

			// sample buffer set when decoder constructed
			SampleBuffer output = (SampleBuffer) decoder.decodeFrame(h,
					bitstream);

			// out.write(output.getBuffer(), 0, output.getBufferLength());
			//System.out.println("" + (output.getBufferLength()) + "; r = "
				//	+ (output.getBufferLength() % 2));

			bitstream.closeFrame();
			System.out.println("avg = " + averageAplitude(output.getBuffer()));
		}

		bitstream.close();
		stream.close();
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("Getting Info...");
		File f = new File("D:\\Habakuk - Rasta Trans Remix 2004.mp3");
		MP3Info info = MP3Info.scan(f);
		System.out.println("Done.");
		System.out.printf("firstFramePos = %d\n", info.firstFramePos);
		test(info);
		
		System.out.println("END");
	}
}
