

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JSlider;
import javax.swing.JToolTip;
import javax.swing.Popup;
import javax.swing.PopupFactory;

public class SliderToolTip implements MouseListener, MouseMotionListener {
	private JSlider slider;
	private Popup popup;
	private JToolTip toolTip;
	private String toolTipText = "0";

	public SliderToolTip(JSlider slider) {
		this.slider = slider;

		slider.addMouseListener(this);
		slider.addMouseMotionListener(this);

		toolTip = slider.createToolTip();
	}

	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String text) {
		this.toolTipText = text;
		toolTip.setTipText(toolTipText);
	}

	protected void showToolTip(MouseEvent event) {
		// Determine tooltip text and location

		Point toolTipLocation = getToolTipLocation(event);

		// Trick is to hide a previous popup before showing a new one

		if (popup != null)
			popup.hide();

		PopupFactory factory = PopupFactory.getSharedInstance();
		popup = factory.getPopup(slider, toolTip, toolTipLocation.x,
				toolTipLocation.y);
		popup.show();
	}

	protected Point getToolTipLocation(MouseEvent event) {
		Point p = event.getLocationOnScreen();
		Rectangle sliderBounds = slider.getBounds();
		Point sliderLoc = slider.getLocationOnScreen();
		int y = sliderLoc.y + (sliderBounds.height / 2) + 12;
		int x = p.x;

		Dimension toolTipDim = toolTip.getSize();
		int maxXLocation = sliderLoc.x + sliderBounds.width - toolTipDim.width;
		int minXLocation = sliderLoc.x;

		if (x > maxXLocation)
			x = maxXLocation;
		else if (x < minXLocation)
			x = minXLocation;

		return new Point(x, y);
	}

	//
	// Implement MouseListener Interface
	//
	@Override
	public void mousePressed(MouseEvent e) {
		if (slider.isEnabled())
			showToolTip(e);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (slider.isEnabled())
			popup.hide();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	//
	// Implement MouseMotionListener Interface
	//
	/**
	 * Update the tooltip text and/or position as the mouse is dragged
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if (slider.isEnabled())
			showToolTip(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}
}
