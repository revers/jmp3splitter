import java.io.*;
import mp3.*;

public class FramePosTest {

	private static void test() throws Exception {
		MP3Info info = MP3Info.scan(new File("D:\\Habakuk - Rasta Trans Remix 2004.mp3"));
		int frame = 7526;
		int pos = MP3Utils.getFramePos(info.mp3File, frame, info.firstFramePos);
		System.out.println("pos of frame " + frame + " = " + pos);
	}
	public static void main(String[] args) throws Exception {
		test();
	}

}
