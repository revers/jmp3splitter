import java.io.*;

public class LineCounter {

	private static int getLineCount(File f) throws IOException {
		int counter = 0;
		BufferedReader in = new BufferedReader(new FileReader(f));
		while (in.readLine() != null)
			counter++;
		in.close();
		return counter;
	}

	private static int getLinesInDir(File dir) throws IOException {
		int counter = 0;
		File[] files = dir.listFiles();
		for (File f : files) {
			if (!f.getName().toLowerCase().endsWith(".java"))
				continue;
			counter += getLineCount(f);
		}
		return counter;
	}

	private static final File[] DIRS = { new File(
			"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter\\src\\gui"), new File(
			"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter\\src\\mp3"), new File(
			"D:\\Revers\\[ECLIPSE]\\eclipse\\workspace\\JMP3Splitter\\src\\utils") };

	public static void main(String[] args) throws IOException {
		int total = 0;
		for(File f : DIRS) {
			total += getLinesInDir(f);
		}
		System.out.println("Liczba linijek: " + total);
	}

}
